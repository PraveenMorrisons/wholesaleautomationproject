package TestScripts_OrderShipment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
//import org.testng.SkipException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import SandPiper_All_Functions.SandPiper_GetCall;
import SandPiper_All_Functions.SandPiper_PostCall;
import SandPiperUtilities.MyException;
import SandPiperUtilities.ProjectConfigurations;
import SandPiperUtilities.Reporting_Utilities;
import SandPiperUtilities.RowGenerator;
import SandPiperUtilities.excelCellValueWrite;
import SandPiperUtilities.getCurrentDate;
import SandPiperUtilities.utilityFileWriteOP;

public class CreateShipment {
	
	//String URL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	String TestDataPath;
	String TemporaryFilePath;
	String Screenshotpath;
	String ResultPath="";

	public WebDriver driver = null;
	public WebDriverWait wait = null;
	ExtentReports extent;
	ExtentTest logger;


@Before
public void setUp() throws Exception {

	DriverPath=ProjectConfigurations.LoadProperties("Sandpiper_DriverPath");
	DriverName=ProjectConfigurations.LoadProperties("Sandpiper_DriverName");
	DriverType=ProjectConfigurations.LoadProperties("SandPiper_DriverType");
	TestDataPath=ProjectConfigurations.LoadProperties("Sandpiper_TestDataPath");
	SheetName=ProjectConfigurations.LoadProperties("Sandpiper_SheetName");
	ItemDetailsSheetName=ProjectConfigurations.LoadProperties("Sandpiper_ItemDetailsSheetName");
	TemporaryFilePath=ProjectConfigurations.LoadProperties("SandPiper_TemporaryFilePath");
	Screenshotpath = ProjectConfigurations.LoadProperties("Sandpiper_Screenshotpath");
//	ResultPath="/Results/Result_"+getCurrentDate.getISTTimeHTMLforfolder();
	//*********************Setting up Result Path*****************************
	
	ResultPath=System.getProperty("resultpath");
	if(ResultPath==null){
		ResultPath=Reporting_Utilities.createResultPath();
	}
	System.out.println("Result path is "+ResultPath);
	//************************************************************************
	System.setProperty(DriverName, DriverPath);
	utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
	utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
	//*********************************Initialising Extent Report Variables**********************************
	extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"SOPExtentReport.html", false);
	extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	//*******************************************************************************************************
}

@After
public void tearDown() throws Exception {
	extent.endTest(logger);
	extent.flush();
	extent.close();
	utilityFileWriteOP.writeToLog("*********************************END**********************************",ResultPath);	 					 
	utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
	}


	@Test
	public void test() throws IOException {
		//*************************Initialise Variable***************************
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Order_Ship_Sales_Stocks";
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String ColumnNames = null;
		String ItemIDs = null;
		String AllQuantitiestoShip = null;
		String AllOrderHeaders = null;
		String AllMessageHeaders = null;
		String AllShipDetailsHeaders = null;
		String AllBilltoDetailsHeaders = null;
		String AllBilltoTaxDetailsHeaders = null;
		HashMap<String, String> Payload_map = null;
		ArrayList<String> ItemDetailsList = new ArrayList<String>();
		//-----------------------Extent Report-----------
		logger = extent.startTest("CreateShipmentOrder");
		//-----------------------------------------------	
		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        
        String consolidatedScreenshotpath="";
        //-------------------------------------------------
        
		int r = 0;
		try{
		int rows = 0;
		int occurances = 0;
		int ItemDetailsoccurances = 0;
		System.out.println(TestDataPath + "/"+SheetName);
		Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
		Sheet sheet1 = wrk1.getSheet(SheetName);
		Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
		rows = sheet1.getRows();
		int itemdetailsrows = sheet2.getRows();
		System.out.println(itemdetailsrows);
		int item_row=0;
		for(int i=1; i<itemdetailsrows; i++) {
			
			Keyword = sheet2.getCell(0, i).getContents().trim();
			
			
			System.out.println(sheet2.getCell(0, i).getContents().trim());
			if(ItemDetailsoccurances>0){
		          
		    	if(!(TestKeyword.contentEquals(Keyword))){
		    		System.out.println("breaking with keyword "+ Keyword);
			    	break;
			    	
		    	}

			 }
			ItemIDs = sheet2.getCell(1, i).getContents().trim();
			
			item_row=i;
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
				  
				ItemDetailsoccurances=ItemDetailsoccurances+1;
				 
				
				ItemIDs.split(",");
			
			}
		}
		
		for(r=1; r<rows; r++) {
			//***************************Initialise Variables**************************************
			TestCaseNo = sheet1.getCell(0, r).getContents().trim();
			TestCaseName = sheet1.getCell(1, r).getContents().trim();
			Keyword = sheet1.getCell(2, r).getContents().trim();
			ProxyHostName = sheet1.getCell(5, r).getContents().trim();
			ProxyPort = sheet1.getCell(6, r).getContents().trim();
			SYSUserName = sheet1.getCell(7, r).getContents().trim();
			SYSPassWord = sheet1.getCell(8, r).getContents().trim();
			TargetHostName = sheet1.getCell(9, r).getContents().trim();
			TargetPort = sheet1.getCell(10, r).getContents().trim();
			TargetHeader = sheet1.getCell(11, r).getContents().trim();
			UrlTail = sheet1.getCell(12, r).getContents().trim();
			ApiKey = sheet1.getCell(13, r).getContents().trim();
			AuthorizationKey = sheet1.getCell(14, r).getContents().trim();
			AuthorizationValue =sheet1.getCell(15, r).getContents().trim();
			AllOrderIDs = sheet1.getCell(16, r).getContents().trim();
			AllMessageTypes = sheet1.getCell(17, r).getContents().trim();
			AllShipToLocationIds = sheet1.getCell(18, r).getContents().trim();
			ShipToDeliverAt = sheet1.getCell(19, r).getContents().trim();
			AllOrderAttributes = sheet1.getCell(21, r).getContents().trim();
			AllMessageAttributes = sheet1.getCell(22, r).getContents().trim();
			AllBillToAddressAttributes = sheet1.getCell(24, r).getContents().trim();
			AllBillToTaxAddressAttributes = sheet1.getCell(25, r).getContents().trim();
			ColumnNames = sheet1.getCell(34, r).getContents().trim();
			AllQuantitiestoShip = sheet1.getCell(33, r).getContents().trim();
			AllOrderHeaders = sheet1.getCell(21, 0).getContents().trim();
			AllMessageHeaders = sheet1.getCell(22, 0).getContents().trim();
			AllShipDetailsHeaders = sheet1.getCell(23, 0).getContents().trim();
			AllBilltoDetailsHeaders = sheet1.getCell(24, 0).getContents().trim();
			AllBilltoTaxDetailsHeaders = sheet1.getCell(25, 0).getContents().trim();
			String shiipedvalue=sheet1.getCell(33, r).getContents().trim();
			String Quantityordered=sheet1.getCell(56, r).getContents().trim();
			if(occurances>0){
		        
		    	if(!(TestKeyword.contentEquals(Keyword))){

			    	break;
			    	
		    	}

			 }
			
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
					
				occurances=occurances+1;
				Payload_map = new HashMap<String, String>();
				String OrderID[] = AllOrderIDs.split(",");
				String ShipToLocationId[] = AllShipToLocationIds.split(",");
				String MessageType[] = AllMessageTypes.split(",");
				ColumnNames.split(",");
				System.out.println(ItemIDs);
				System.out.println("AllOrderIDs"+AllOrderIDs);
				String OrderHeaders[] = AllOrderHeaders.split(",");
				String MessageHeaders[] = AllMessageHeaders.split(",");
				String ShipDetailsHeaders[] = AllShipDetailsHeaders.split(",");
				String BilltoDetailsHeaders[] = AllBilltoDetailsHeaders.split(",");
				String BilltoTaxDetailsHeaders[] = AllBilltoTaxDetailsHeaders.split(",");
				shiipedvalue.split(",");
				
				for(int i=0; i<OrderHeaders.length; i++) {
					Payload_map.put(OrderHeaders[i], AllOrderAttributes.split(",")[i]); 
				}	
				 
				for(int i=0; i<MessageHeaders.length; i++) {
					Payload_map.put(MessageHeaders[i], AllMessageAttributes.split(",")[i]); 
				}
				 
				for(int i=0; i<ShipDetailsHeaders.length; i++) {
					Payload_map.put(ShipDetailsHeaders[i], AllShipDetailsHeaders.split(",")[i]); 
				}
				 
				for(int i=0; i<BilltoDetailsHeaders.length; i++) {
					Payload_map.put(BilltoDetailsHeaders[i], AllBillToAddressAttributes.split(",")[i]); 
				}
				 
				for(int i=0; i<BilltoTaxDetailsHeaders.length; i++) {
					Payload_map.put(BilltoTaxDetailsHeaders[i], AllBillToTaxAddressAttributes.split(",")[i]); 
				}
				String[] AllItemsInItemDetails = ItemIDs.split(",");
				Quantityordered.split(",");
				 
				for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
					String ItemDetailsJson = sheet2.getCell(c, item_row).getContents().trim();
					System.out.println("Adding " + ItemDetailsJson);
					ItemDetailsList.add(ItemDetailsJson);					
				}
				boolean post = true;
				boolean rdsValidation = true;
				boolean get = true;
				boolean login = true;
				boolean taskrun = true;
				int order = 0;
				File screenshotpath = new File(ResultPath+"Screenshot/"+TestCaseNo+"/");
				screenshotpath.mkdirs();
				consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
				 
				//-------------------HTML Header--------------------
					 
				Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
				Integer stepnum=0;
				try{
					stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
				}catch(Exception e){
					System.setProperty("stepnumber", "1");
					stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
				}
									
				//----------------------------------------------------
				Boolean RestShip = SandPiper_PostCall.OrderShipmentSandpiper(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[0], MessageType[0], ShipToLocationId[0], ShipToDeliverAt, ItemIDs, AllQuantitiestoShip, TestCaseNo , ResultPath, xwpfRun);
				if(RestShip==true) {
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Ship Order Service Call", "Order Shipped Successfully.", "PASS", ResultPath);
					logger.log(LogStatus.PASS, "Order Shipped Successfully.");
				}else{
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Ship Order Service Call", "Error Occured during Order Shipped.", "FAIL", ResultPath);
					logger.log(LogStatus.FAIL, "Error Occured during Order Shipped.");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}	
				post = post && RestShip;
				stepnum++;
				Boolean RestGetShipped =    SandPiper_GetCall.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "shipped", TestCaseNo,ResultPath,xwpfRun);		 
				if(RestGetShipped==true) {
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Order Service Call After Order Shipped", "Shipped Order Validated Successfully.", "PASS", ResultPath);
					logger.log(LogStatus.PASS, "Shipped Order Validated Successfully.");
				}
				else{
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Order Service Call After Order Shipped", "Error Occured during Shipped Order Validation.", "FAIL", ResultPath);
					logger.log(LogStatus.FAIL, "Shipped Order Validated Successfully.");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
				get = get && RestGetShipped;
				stepnum++;
				Thread.sleep(5000);
				System.setProperty("stepnumber", stepnum.toString());
				Payload_map.put("locationId", "9"+ShipToLocationId[0]); 
				if( (post) && (get) && (rdsValidation) && (login) && (taskrun)) {
					Final_Result = "PASS";
					excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
					Assert.assertTrue(TestCaseName, true);
				 }else {
					 Final_Result = "FAIL";
					 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
					 Assert.assertTrue(TestCaseName, false);
				}
			}
		}
	}catch(Exception e) {
		e.printStackTrace();
		Final_Result = "FAIL";
		excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
		Assert.assertTrue(TestCaseName, false);
	}finally
	{   
		FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"1.docx");
		doc.write(out1);
		out1.close();
		doc.close();
		RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
	}
			
}
}
