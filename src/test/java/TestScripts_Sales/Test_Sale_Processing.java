package TestScripts_Sales;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
//


@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestScripts_OrderCreation.CreateWholesaleOrder.class,
	TestScripts_OrderShipment.CreateShipment.class,
	salesProcessing.Run_ECS_task_and_validate_in_RDS.class,
	salesProcessing.Run_SalesFileGeneratorLambda_SIT.class,
	salesProcessing.Extract_RTLOGname_RDS_and_download_RTLOG_AWS.class,
	salesProcessing.Validate_Item_Details_in_RTLOG.class,
	
})



public class Test_Sale_Processing {
	
	
	

 
} 