package AWS;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import SandPiperUtilities.TakeScreenShot;
import SandPiperUtilities.getCurrentDate;
import SandPiperUtilities.utilityFileWriteOP;

public class AWS_Utilities {  
	
	
	
	@SuppressWarnings("finally")
	//******************************************************
	//Method Name: Lamda_job_rigger
	//Method Description: Method to trigger Lamda job in the AWS
	//Created By: Morrissons TCS Automation Team
	//*******************************************************
		public static boolean Lamda_job_trigger(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String jobname , String resultpath , String Screenshotpath,XWPFRun xwpfRun, ExtentTest logger)
		{
			boolean res=false;
			try{
				resultpath=System.getProperty("user.dir")+"/"+resultpath;
				//******************************Navigate to the page to trigger Lamda job*************************************************
				String AfterAWSLogin_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AfterAWSLogin_WP1, driver);
				String oldTab = driver.getWindowHandle();
				driver.findElement(By.xpath("//a[@class='particles-button particles-button--regular particles-button--secondary particles-button particles-button--regular particles-button--secondary console-signin-button aws-020551178989' and contains(text(),'AWS Console')]")).click();
				utilityFileWriteOP.writeToLog(TC_no, "NonProd-m-ordering AWS Console", "Clicked!!!",resultpath,xwpfRun);
				logger.log(LogStatus.PASS, "NonProd-m-ordering AWS Console is Clicked!!!");
				logger.log(LogStatus.PASS, logger.addScreenCapture(AfterAWSLogin_WP1));
				//Window or switched Tabs Handling
				ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				//System.out.println(newTab.size());
				newTab.remove(oldTab);
				// change focus to new tab
				driver.switchTo().window(newTab.get(0));        
				//Waiting for AWS Services Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
				boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
				if(imgres1)
				{
					utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",resultpath);
					// Take Screenshot
					String AWSServicePage_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					//Enter Lambda keyword in the search window
					driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys(searchkey);
					utilityFileWriteOP.writeToLog(TC_no, "Lambda Keyword", "Entered!!!",resultpath,xwpfRun);
					logger.log(LogStatus.PASS, "Lambda Keyword is Entered!!!");
					logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
					driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'Lambda')]")).click();
					//driver.findElement(By.xpath("//input[@id='search-box-input' and @data-reactid='.0.0.0.1.0']")).sendKeys(Keys.RETURN);
					//Waiting for Lambda Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='text' and @placeholder='Filter by tags and attributes or search by keyword']")));
					utilityFileWriteOP.writeToLog(TC_no, "Lambda Search Page", "Found!!!",resultpath);
					// Take Screenshot
					String LambdaFnSearch_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					//Enter Lambda Function search string for daily load in the search window
					driver.findElement(By.xpath("//input[@type='text' and @placeholder='Filter by tags and attributes or search by keyword']")).sendKeys(jobname);
					utilityFileWriteOP.writeToLog(TC_no, "Daily Load Function name", "Entered!!!",resultpath,xwpfRun);
					logger.log(LogStatus.PASS, "Daily Load Function name is Entered!!!");
					logger.log(LogStatus.PASS, logger.addScreenCapture(LambdaFnSearch_WP1));
					driver.findElement(By.xpath("//input[@type='text' and @placeholder='Filter by tags and attributes or search by keyword']")).sendKeys(Keys.RETURN);
					Thread.sleep(3000);
					//Take Screenshot
					String LambdaFnName=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnName, driver);	
					driver.findElement(By.xpath("//a[@href='#/functions/"+jobname+"' and contains(text(),'"+jobname+"')]")).click();		 	
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'"+jobname+"')]")));
					utilityFileWriteOP.writeToLog(TC_no, "Location Found for Daily Load Lambda function", "Passed!!!",resultpath,xwpfRun);
					logger.log(LogStatus.PASS, "Location Found for Daily Load Lambda function");
					logger.log(LogStatus.PASS, logger.addScreenCapture(LambdaFnName));
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Actions')]")));
					Thread.sleep(5000);
					try
					{
						driver.findElement(By.xpath("//span[contains(text(),'Test')]")).click();
					}
					catch (Exception e)
					{
						e.printStackTrace();
						System.out.println("The error is "+e);
						utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "as test button not clickable, hence trying again",resultpath);
						logger.log(LogStatus.FAIL, "Problem ocuurred for "+e, "as test button not clickable, hence trying again");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Test')]")));
						driver.findElement(By.xpath("//span[contains(text(),'Test')]")).click();
					}
					//Take Screenshot
					String LambdaTriggered=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaTriggered, driver);
					utilityFileWriteOP.writeToLog(TC_no, "Test Button clicked", "Passed!!!",resultpath,xwpfRun);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Execution result: succeeded')]")));
					String LambdaPassedSuccessfully=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaPassedSuccessfully, driver);
					//System.out.println("Daily Load Triggred Successfully");
					Thread.sleep(5000);
					driver.close();
					utilityFileWriteOP.writeToLog(TC_no, "Daily Load triggered succesfully, moving to previous tab", "Passed!!!",resultpath,xwpfRun);
					logger.log(LogStatus.PASS, "Daily Load triggered succesfully, moving to previous tab");
					logger.log(LogStatus.PASS, logger.addScreenCapture(LambdaPassedSuccessfully));
					res=true;	 		
					return res;

				}else
				{
					res =false;
					return res;
				}

			}catch (Exception e) 
			{	
				String error=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(error, driver);
				e.printStackTrace();
				res=false;
				System.out.println("The error is "+e);
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",resultpath);
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred While ", "Triggering Daily Load",resultpath,xwpfRun,"");
				logger.log(LogStatus.FAIL, "Problem ocuurred for "+e, "as test button not clickable, hence trying again");
				return res;
			}

		}
	
	
	
	
	
	//******************************************************
	//Method Name: aws_login
	//Method Description: Method to login to AWS
	//Created By: Morrissons TCS Automation Team
	//*******************************************************
	public static boolean aws_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw , String Screenshotpath , String Resultpath,XWPFRun xwpfrun, ExtentTest logger)
	{
		boolean res=false;
		try
		{
			Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@class='rs-input-large' and @id='username']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Login Page of AWS Rackspace", "Displayed!!!",Resultpath);
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			driver.findElement(By.xpath("//input[@class='rs-input-large' and @id='username']")).sendKeys(un);
			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath);
			driver.findElement(By.xpath("//input[@class='rs-input-large' and @id='password']")).sendKeys(pw);
			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath);
			String awsDynamoDB_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			driver.findElement(By.xpath("//button[@type='submit' and text()='Log In']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Login Button", "Clicked!!!",Resultpath);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='card-title particles-card__header--truncate' and text()='nonprod-m-ordering']")));
			String awsDynamoDB_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			if(driver.findElement(By.xpath("//div[@class='card-title particles-card__header--truncate' and text()='nonprod-m-ordering']")).isDisplayed())
			{
				res=true;
				logger.log(LogStatus.PASS, "AWS Home Page is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc2));
				SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Home Page", "Displayed!!!",Resultpath);
			}else{
				logger.log(LogStatus.FAIL, "AWS Home Page is NOT displayed");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc2));
				SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Home Page", "Not Displayed!!!",Resultpath);
			
			}
				
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while logging in AWS Rackspace", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}
		
	}
	
	
	
	
	public static boolean AWSLogOut (String UserName,WebDriver driver, WebDriverWait wait, String TC_no , String Resultpath ,  String Screenshotpath,XWPFRun xwpfrun) 
	{
		boolean res = false;
		try
		{
			String MainPg_title = "Fanatical Support for AWS";
			String currentWindow = driver.getWindowHandle();  //will keep current window to switch back
			System.out.println("Current Title: "+driver.getTitle());
			for(String winHandle : driver.getWindowHandles())
			{
				if (driver.switchTo().window(winHandle).getTitle().equals(MainPg_title))  //Search for the desired Window
				{

					
					System.out.println("Entered to this block working correctly");
					break;

				} 
				else 
				{
					
					driver.switchTo().window(currentWindow);
				} 
			}


			
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='rs-dropdown-title' and contains(text(),' "+ UserName + "' )]")));
			driver.findElement(By.xpath("//div[@class='phx-dropdown-title' and contains(text(),'"+UserName.toUpperCase()+"')]")).click();

			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")));
			driver.findElement(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")).click();
			Thread.sleep(3000);
			//Take Screenshot
			String ClickonLogout=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";   
			TakeScreenShot.saveScreenShot(ClickonLogout, driver);
			utilityFileWriteOP.writeToLog(TC_no, "Logout Button Clicked", "Done!!!",Resultpath);
			Thread.sleep(5000);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));

			String AfterLogout_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";

			TakeScreenShot.saveScreenShot(AfterLogout_WP1, driver);

			if(driver.findElement(By.xpath("//input[@id='username']")).isDisplayed())
			{
				res=true;
			}


			if(res)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Logout", "Successfull!!!",Resultpath,xwpfrun);
				System.out.println("Logout-Pass");
			}
			if(!res)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Cannot Logout", "Failed to Logout!!!",Resultpath,xwpfrun);
				System.out.println("Logout-Fail");
			}


			return res;


		}

		catch (Exception e)
		{
			res=false;
			System.out.println("The error is "+e);
			utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for"+e, "while logging out from AWS",Resultpath);
			return res;
		}

		finally
		{
			return res;
		}
	}
	
	
	
	
	
	
	
	public static boolean statusvalidateordersInRDS(String[] columnname, String[] orderid, String[] columnvalue, String tcid,String DriverSheetPath) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=true;
		 
		 
	    
	    String QResult="";

	       try{      
	             
	             JSch jsch = new JSch();
	             
	             java.util.Properties config = new java.util.Properties(); 
	             
	          //   System.out.println("0");

	             //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	             String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user master";
	             
	             
	           session = jsch.getSession("mwadmin","10.244.39.83",22);
	             
	            config.put("StrictHostKeyChecking", "no");
	             
	             session.setConfig(config);
	             
	        //    System.out.println("1");
	             
	         session.setPassword("2Bchanged");
	         
	         session.connect();
	             
	          //   System.out.println("Connection success");

	             channel = session.openChannel("exec");
	             ((ChannelExec)channel).setCommand(command);

	             channel.setInputStream(null);
	             ((ChannelExec)channel).setErrStream(System.err);
	             InputStream in = channel.getInputStream();
	           ((ChannelExec)channel).setPty(false);
	             
	             OutputStream out=channel.getOutputStream();
	             channel.connect();
	             
	             Thread.sleep(1000);
	             
	       out.write(("FS4T3fdQCYCym48h"+"\n").getBytes());
	       out.flush();
	       
	       
	       for(int item=0;item<orderid.length;item++)
	       {
	    	   for(int attribute=0; attribute<columnname.length; attribute++){
	       
	       Thread.sleep(1000);
	       QResult="";
	       out.flush();
	       
	    //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	    out.write(("select distinct "+columnname[attribute]+" from storepick_sales_service.SALES_DETAILS where order_id='"+orderid[item]+"';\n").getBytes());
	    
	       out.flush();
	       
	       Thread.sleep(1000);
	       
	       
	       
	       Thread.sleep(5000);

	    // channel.setOutputStream(System.out);

	     //  System.out.println("2");
	       
	       byte[] tmp = new byte[2048];
	          
	       int i=0;  
	             
	       
	         while (in.available() > 0) {
	               	
	               	{
	                 
	               i = in.read(tmp, 0, 1024);
	                  
	                   System.out.println("i "+i);
	                 //  System.out.println(i);
	                   
	                   if (i < 0) {
	                       break;

	                   }
	                 
	          //   System.out.print(new String(tmp, 0, i));
	                   
	               }  
	               	
	               	
	              
	              }
	         
	       QResult=new String(tmp, 0, i); 
	       String[] rest= QResult.split(columnname[attribute]);
			 String justfinalres=rest[rest.length-1].replaceAll("[+|]", "");
			 System.out.println("QResult is "+QResult+" is this!!!");
			 System.out.println("justfinalres "+justfinalres+" is this!");
	   	 String[] rest2=justfinalres.split("[\\r\\n]+");
	   	 String finalres="";
	   	 for(int restcount=0;restcount<rest2.length;restcount++)
	   	 {
	   	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	   	 if(rest2[restcount].contains("("))
	   	 {
	   		 if(!rest2[restcount-1].equals(columnname))
	   		 {
	   		 finalres=rest2[restcount-1].trim();
	   		 
	   		 break;
	   		 }
	   	 }
	   	 
	   	 }
	   	 finalres = rest2[2].trim();
	   	 System.out.println("finalres "+finalres);
	   	 //System.out.println(columnname[col]);
	   	 
	   	 //System.out.println(itemid[item]);
	   	//System.out.println(itemlist.get(item).toString());
	   	//System.out.println(columnname[col]);
	   	 System.out.println(columnvalue[attribute]);
	   	 if(finalres.equalsIgnoreCase(columnvalue[attribute]))
	   	 {
	   		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[attribute]+" from POSTMAN "+columnvalue[attribute], "Matched with the value found in RDS DB "+finalres,DriverSheetPath);
	   		 
	   		 
	   	 }
	   	 else
	   	 {		
	   		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	   		 {
	   			 
	   		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[attribute]+" for messageid "+orderid[item], "Did not match because NO value was found in RDS DB ",DriverSheetPath);
		    		 
	   		 }
	   		 else
	   		 {
	   		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[attribute]+" for messageid "+orderid[item], "Did not match with the value found in RDS DB "+finalres,DriverSheetPath);
	   		 }
	   		 res=false;
	   	 }
	   	 
	   	 
	    	   }    
	       }
	       
	       out.flush();
	       out.write(("\\q"+"\n").getBytes());
	       
	       out.flush();
	       
	       Thread.sleep(5000);
	     //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	  for(int j=0;j<res.length;j++){

	  if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	   	System.out.println(res[j].trim());
	   	adj_val_rds=res[j].trim();
	   	
	    }  
	  

	  }*/
	  
	  
	// System.out.println( session.isConnected());

	 channel.disconnect();

	 session.disconnect();
	 
	 Thread.sleep(5000);
	 
	 out.close();
	 
	 in.close();
	 
	//System.out.println(adj_val_rds);
	 


	}
	             

	       
	catch(Exception e){
	e.printStackTrace();
	System.out.println(e);

	       } 


	finally{        
	       
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	     return res;
	}
	}


	
	
	
	
	public static ArrayList<String> ExtractRTLOGnamefromRDS(String[] columnname, String[] orderid, String[] columnvalue, String tcid,String DriverSheetPath) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 ArrayList<String> RTLOGS = new ArrayList<String>();
		 
		 boolean res=true;
		 
		 
	    
	    String QResult="";

	       try{      
	             
	             JSch jsch = new JSch();
	             
	             java.util.Properties config = new java.util.Properties(); 
	             
	          //   System.out.println("0");

	             //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	             String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user master";
	             
	             
	           session = jsch.getSession("mwadmin","10.244.39.83",22);
	             
	            config.put("StrictHostKeyChecking", "no");
	             
	             session.setConfig(config);
	             
	        //    System.out.println("1");
	             
	         session.setPassword("2Bchanged");
	         
	         session.connect();
	             
	          //   System.out.println("Connection success");

	             channel = session.openChannel("exec");
	             ((ChannelExec)channel).setCommand(command);

	             channel.setInputStream(null);
	             ((ChannelExec)channel).setErrStream(System.err);
	             InputStream in = channel.getInputStream();
	           ((ChannelExec)channel).setPty(false);
	             
	             OutputStream out=channel.getOutputStream();
	             channel.connect();
	             
	             Thread.sleep(1000);
	             
	       out.write(("FS4T3fdQCYCym48h"+"\n").getBytes());
	       out.flush();
	       
	       
	       for(int item=0;item<orderid.length;item++)
	       {
	    	  
	       
	       Thread.sleep(1000);
	       QResult="";
	       out.flush();
	       
	    //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	    out.write(("select distinct rtlog_file_name from storepick_sales_service.SALES_DETAILS where order_id='"+orderid[item]+"';\n").getBytes());
	    
	       out.flush();
	       
	       Thread.sleep(1000);
	       
	       
	       
	       Thread.sleep(5000);

	    // channel.setOutputStream(System.out);

	     //  System.out.println("2");
	       
	       byte[] tmp = new byte[2048];
	          
	       int i=0;  
	             
	       
	         while (in.available() > 0) {
	               	
	               	{
	                 
	               i = in.read(tmp, 0, 1024);
	                  
	                   System.out.println("i "+i);
	                 //  System.out.println(i);
	                   
	                   if (i < 0) {
	                       break;

	                   }
	                 
	          //   System.out.print(new String(tmp, 0, i));
	                   
	               }  
	               	
	               	
	              
	              }
	         
	       QResult=new String(tmp, 0, i); 
	       String[] rest= QResult.split(orderid[item]);
			 String justfinalres=rest[rest.length-1].replaceAll("[+|]", "");
			 System.out.println("QResult is "+QResult+" is this!!!");
			 System.out.println("justfinalres "+justfinalres+" is this!");
	   	 String[] rest2=justfinalres.split("[\\r\\n]+");
	   	 String finalres="";
	   	 for(int restcount=0;restcount<rest2.length;restcount++)
	   	 {
	   	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	   	 if(rest2[restcount].contains("("))
	   	 {
	   		 if(!rest2[restcount-1].equals(columnname))
	   		 {
	   		 finalres=rest2[restcount-1].trim();
	   		 
	   		 break;
	   		 }
	   	 }
	   	 
	   	 }
	   	 finalres = rest2[7].trim();
	   	 System.out.println("finalres "+finalres);
	   	 //System.out.println(columnname[col]);
	   	 
	   	 //System.out.println(itemid[item]);
	   	//System.out.println(itemlist.get(item).toString());
	   	//System.out.println(columnname[col]);
	   	 
	   			
	   		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	   		 {
	   			 
	   		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of RTLOG for orderid "+orderid[item], "Did not found because NO value was found in RDS DB ",DriverSheetPath);
		    		 
	   		 }
	   		 else
	   		 {
	   		 
	   			RTLOGS.add(finalres) ;
	   		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of RTLOG for messageid "+orderid[item], "is "+finalres,DriverSheetPath);
	   		 }
	   		 res=false;
	   		 
	   	 }
	   	 
	   	 
	    	      
	       
	       
	       out.flush();
	       out.write(("\\q"+"\n").getBytes());
	       
	       out.flush();
	       
	       Thread.sleep(5000);
	     //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	  for(int j=0;j<res.length;j++){

	  if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	   	System.out.println(res[j].trim());
	   	adj_val_rds=res[j].trim();
	   	
	    }  
	  

	  }*/
	  
	  
	// System.out.println( session.isConnected());

	 channel.disconnect();

	 session.disconnect();
	 
	 Thread.sleep(5000);
	 
	 out.close();
	 
	 in.close();
	 
	//System.out.println(adj_val_rds);
	 


	}
	             

	       
	catch(Exception e){
	e.printStackTrace();
	System.out.println(e);

	       } 


	finally{        
	       
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	     return RTLOGS;
	}
	}


	
	public static boolean ValidateandDownloadRTLOGfromPending(WebDriver driver, WebDriverWait wait, String searchKey,  String TC_no , ArrayList<String> RTLOGnames, String Resultpath , String Screenshotpath)
	{
		boolean res=false;
		
		try
		{
			
			String AfterAWSLogin_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
			TakeScreenShot.saveScreenShot(AfterAWSLogin_WP1, driver);

			String oldTab = driver.getWindowHandle();
			driver.findElement(By.xpath("//a[@class='particles-button particles-button--regular particles-button--secondary particles-button particles-button--regular particles-button--secondary console-signin-button aws-020551178989' and contains(text(),'AWS Console')]")).click();
			utilityFileWriteOP.writeToLog(TC_no, "NonProd-m-ordering AWS Console", "Clicked!!!",Resultpath);

			//Window or switched Tabs Handling
			ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(newTab.size());
			newTab.remove(oldTab);
			// change focus to new tab
			driver.switchTo().window(newTab.get(0));        

			//Waiting for AWS Services Page
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input' and @data-reactid='.0.0.0.1.0']")));


				utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath);
				// Take Screenshot

				String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);

				//Enter Lambda keyword in the search window
				driver.findElement(By.xpath("//input[@id='search-box-input' and @data-reactid='.0.0.0.1.0']")).sendKeys("s3");
				utilityFileWriteOP.writeToLog(TC_no, "S3 Keyword", "Entered!!!",Resultpath);
				driver.findElement(By.xpath("//input[@id='search-box-input' and @data-reactid='.0.0.0.1.0']")).sendKeys(Keys.RETURN);


				
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Search field is populated with ", "Keyword "+searchKey,Resultpath);
			
			String awsDynamoDB_sc1=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Link related to keyword "+searchKey, "Appeared!!!",Resultpath);
			
			String awsDynamoDB_sc2=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Link related to keyword "+searchKey, "Clicked!!!",Resultpath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' rtlog ']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "RTLOG folder ", "Appeared!!!",Resultpath);
			
			String awsDynamoDB_sc3=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' rtlog ']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Inbound folder ", "Clicked!!!",Resultpath);
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' pending ']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Pending folder ", "Appeared!!!",Resultpath);
			
			String awsDynamoDB_sc4=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc4, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' pending ']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Pending folder ", "Clicked!!!", Resultpath);
			
			
			String awsDynamoDB_sc7=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc7, driver);
			
			
			
			
			for (String rtlog:RTLOGnames){
				
/*				<input type="text" class="input-field ng-pristine ng-valid ng-empty ng-touched" id="get-filter-value" ng-trim="false" ng-model="filters.input" ng-keydown="filterKeyDown($event)" ng-keyup="filterKeyUp($event)" ng-focus="openDropdown()" placeholder="Type a prefix and press Enter to search. Press ESC to clear." focus-when="prefixSearchInput">
				driver.findElement(By.xpath("//input[@type='text' and @class='input-field ng-pristine ng-valid ng-empty ng-touched']")).click();
				<div id="search-box" class="search-box">
	            <!-- ngIf: !noBackendCalls --><span class="search-icon ng-scope" ng-if="!noBackendCalls"><i class="awsui-icon search"></i></span><!-- end ngIf: !noBackendCalls -->
	            <span>
	                <span ng-show="shouldShowTag()" class="show-tag ng-scope ng-hide" translate="tagsearch.tag">tag</span>
	                <input type="text" class="input-field ng-pristine ng-valid ng-empty ng-touched" id="get-filter-value" ng-trim="false" ng-model="filters.input" ng-keydown="filterKeyDown($event)" ng-keyup="filterKeyUp($event)" ng-focus="openDropdown()" placeholder="Type a prefix and press Enter to search. Press ESC to clear." focus-when="prefixSearchInput">
	                
	            </span>
	        </div>*/
				//*[@id="get-filter-value"]
				
				
				driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).sendKeys(rtlog);
				driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).sendKeys(Keys.RETURN);
				
				
	        /*driver.findElement(By.xpath("//input[@type='text' and class='input-field ng-pristine ng-untouched ng-valid ng-empty']")).sendKeys(rtlog);
	        driver.findElement(By.xpath("//input[@type='text' and class='input-field ng-pristine ng-untouched ng-valid ng-empty']")).sendKeys(Keys.RETURN);*/
	        Thread.sleep(1000);
			if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+rtlog+"')]")).isDisplayed())
				{
					res=true;
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog+".dat", "Exists in pending folder!!!",Resultpath);
					
					driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+rtlog+"')]")).click();
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog+".dat", "Clicked!!", Resultpath);
					
					String awsDynamoDB_sc8=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc8, driver);
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Download']")));
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "File detail page of "+rtlog+".dat", "Appeared!!!" , Resultpath);
					
					String awsDynamoDB_sc9=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc9, driver);
					
					driver.findElement(By.xpath("//span[text()='Download']")).click();
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Download button", "Clicked!!",Resultpath);
					
					String awsDynamoDB_sc6=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc6, driver);
					
					Thread.sleep(20000);
					
					//Robot robot=new Robot();
					//robot.keyPress(KeyEvent.VK_ENTER);
					
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog, "Downloaded!!",Resultpath);
					
					res = true;
				}
			
			else{
				SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog+".dat", "Not Exists in pending folder!!!",Resultpath);
			}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}
	}
	
	
	
	
	
	
	
	
	
	public static boolean ValidateItemDetailsInRTLOG (String[] RTLOGnames , String downloadfilepath ,String rtlogtrandate, String[] items, String[] quantity, String totalamount,String orderid,String TC_no , String Resultpath){
		
		boolean res = true;	
		BufferedReader br = null;
		String[] starr1 = null;
		
		
		try{
			int itemcount = 0 ;
			int flag=0;
			
			
			
			
			for (int rtlog=0; rtlog<RTLOGnames.length;rtlog++){
				
			br = new BufferedReader(new java.io.FileReader(downloadfilepath+"/"+RTLOGnames[rtlog]+".DAT"));
			String currentline = null;
			String currentline1 = null;
			while((currentline=br.readLine())!=null){
				String starr[] = currentline.split("\\s+");
				//System.out.println("first string: "+starr[0]);
				
				
				
				
				
				if(starr[0].contains("FHEAD")){
					String businessdate = starr[0].substring(19, 27);
					//System.out.println("business date : "+businessdate);
					String rtlogdate = starr[0].substring(33, 41);
					//System.out.println("rtlogdate: "+rtlogdate);
					//System.out.println("rtlogtrandate: "+rtlogtrandate.trim());
					if((businessdate.equals(rtlogdate)) && (rtlogdate.equals(rtlogtrandate.trim()))){
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Business date matched with Order creation date: ","PASS" ,Resultpath);				
					}
					else{
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Business date haven't matched with Order creation date: ","FAIL" ,Resultpath);
					}
				}
				
				
				
				
				
				else if(starr[0].contains("THEAD")){
					//System.out.println("rtlogtrandate array : "+starr[1]);
					if(starr[1].contains(rtlogtrandate)){
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Tran date matched with Order creation date: ","PASS" ,Resultpath);
					}
					else
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Tran date haven't matched with Order creation date: ","FAIL" ,Resultpath);
				}
				
					
				
				
				
				
				
				 if(starr[0].contains("THEAD")){
					 System.out.println("inside item validation thead: "+starr[6]);
					 System.out.println(orderid);
						if(starr[6].equals(orderid)) {
							
							System.out.println("inside item validation");
							//validate if order exists
							SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Orderid found in RTLOG: ",starr[6] ,Resultpath);
							while( ((currentline1=br.readLine())!=null)) {
							 
								starr1 = currentline1.split("\\s+");
								if((starr1[0].contains("TTEND")))
								break;
								else{
								System.out.println("starr[0]"+starr1[0]);
								if(starr1[0].contains("TITEM")){
									System.out.println("item: "+starr1[3]+"/n quantity: "+starr1[5]);
									System.out.println("item: "+items[itemcount]+" quantity: "+quantity[itemcount]);
									if(starr1[3].equals(items[itemcount]) && starr1[5].contains(quantity[itemcount])){
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Item and quantity matched in RTLOG: ",starr1[3]+ " "+starr1[5],Resultpath);
									}
									else{
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Item and quantity not matched in RTLOG: ",starr1[3]+ " "+starr1[5] ,Resultpath);
									}
									itemcount++;
								}
								
								}
								flag = 1;
								
								
								
							}
							
							 if(starr1[0].contains("TTEND") && flag==1){
									System.out.println("total amount: "+starr1[2]);
									if(starr1[2].contains(totalamount)){
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount matched in RTLOG: ",starr1[2] ,Resultpath);
									}
									else
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount haven't matched in RTLOG: ",starr1[2] ,Resultpath);
								}				
								if(flag==1){
									break;
								} 

								 
						}
				 }
						
				 
				while((currentline=br.readLine())!=null){
					String[] numarr = currentline.split("\\s+");
					if(numarr[0].contains("THEAD")){
						System.out.println("PASS");
					}
					else
						System.out.println("FALSE");
				}
				 
				 //System.out.println(flag);
				/* if(starr1[0].contains("TTEND") && flag==1){
						System.out.println("total amount: "+starr[2]);
						if(starr[2].contains(totalamount)){
							SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount matched in RTLOG: ",starr[2] ,Resultpath);
						}
						else
							SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount haven't matched in RTLOG: ",starr[2] ,Resultpath);
					}				
					if(flag==1){
						break;
					}*/
				 
				
			}
			res = true;
			
			}
			
			br.close();
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			res=false;
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "A problem occurred while validating RTLOG file", "Due to "+e,Resultpath);
		}
		
		
		return res;
	}
	
	
	//******************************************************
	//Method Name: aws_login
	//Method Description: Method to login to AWS
	//Created By: Morrissons TCS Automation Team
	//*******************************************************
	public static boolean Ecs_task_runner_job_trigger(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String taskname , String Resultpath , String Screenshotpath,XWPFRun xwpfrun, ExtentTest logger)
	{
		{
			String[] tasknames= taskname.split(",");
			boolean res=false;
			try{
				//Click on AWS button
				Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
				String AfterAWSLogin_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AfterAWSLogin_WP1, driver);
				String oldTab = driver.getWindowHandle();
				driver.findElement(By.xpath("//a[@class='particles-button particles-button--regular particles-button--secondary particles-button particles-button--regular particles-button--secondary console-signin-button aws-020551178989' and contains(text(),'AWS Console')]")).click();
				utilityFileWriteOP.writeToLog(TC_no, "NonProd-m-ordering AWS Console", "Clicked!!!",Resultpath,xwpfrun);
				//Window or switched Tabs Handling
				ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				System.out.println(newTab.size());
				newTab.remove(oldTab);
				// change focus to new tab
				driver.switchTo().window(newTab.get(0));        
				//Waiting for AWS Services Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input' ]")));
				boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
				if(imgres1)
				{
					utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath,xwpfrun);
					// Take Screenshot
					String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					//Clicking on the ECS option
					driver.findElement(By.xpath("(//span[text()='ECS'])[3]")).click();
					utilityFileWriteOP.writeToLog(TC_no, "ECS Option ", "Clicked!!!",Resultpath);
					//Waiting for ECS Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath);
					// Take Screenshot
					String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					//loop for the number of jobs to run
					for(int i=0;i<tasknames.length;i++){
						//click on task definitions				
						driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
						Thread.sleep(2000);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));
						driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
						WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
						JavascriptExecutor executor2 = (JavascriptExecutor)driver;
						executor2.executeScript("arguments[0].click();", element2);		
						//loop
						boolean taskvisible =false;
						driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys(tasknames[i]);
						utilityFileWriteOP.writeToLog(TC_no, "Task name: "+tasknames[i], "Entered!!!",Resultpath);
						int count=0;
						driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
						while(taskvisible!=true){
							try{
							taskvisible= driver.findElement(By.xpath("//a[contains(text(),'"+tasknames[i]+"')]")).isDisplayed();
							}
							catch(Exception e){
							System.out.println("Visibilty of task"+taskvisible);
							driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();
							count++;
							if(count>15)
								break;
							}
						}
						System.out.println("task found");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("//a[ contains(text(),'"+tasknames[i]+"') ]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("//div[@class='ui-grid-selection-row-header-buttons ui-grid-icon-ok ng-scope']")).click();
						driver.findElement(By.xpath("(//a[contains(text(),'"+tasknames[i]+"')])[2]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
						driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
						driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
						// Take Screenshot
						String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
						utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
						logger.log(LogStatus.PASS, "Run Task Screen is displayed.");
						logger.log(LogStatus.PASS,logger.addScreenCapture(RunTask_TC002_REG_WP2));
						Thread.sleep(2000);
						driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
						utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
						
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
						WebElement element = driver.findElement(By.xpath("//a[contains(text(),'ecs-euw1-n-wholesale-vpc001-general-001')]"));
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", element);
						System.out.println("Selected cluster");
						utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
						logger.log(LogStatus.PASS, "nonprod-ECS-ECSCluster-W68V9ZHCS399 option selected");
						logger.log(LogStatus.PASS,logger.addScreenCapture(ClusterDropDown_TC002_REG_WP2));
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
						WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
						JavascriptExecutor executor1 = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", element1);
						System.out.println("Clicked Run Task");
						String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskTriggered, driver);
						utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
						logger.log(LogStatus.PASS, "RunTask Button clicked");
						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskTriggered));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
						//  Take Screenshot
						String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskSuccess, driver);
						System.out.println("Transfer Request task Triggered Successfully");
						Thread.sleep(1000);
					}
					utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
					res=true; 		
					return res;
				}else
				{
					res =false;
					return res;
				}
				
			}
			catch (Exception e) 
			{	
				e.printStackTrace();
				res=false;
				System.out.println("The error is "+e);
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",Resultpath);
				return res;
			}

			finally 
			{
				return res;
			}


		}
		
	}	
	
	//******************************************************
	//Method Name: ValidateandDownloadRTLOGfromPending
	//Method Description: Method to download RTLog
	//Created By: Morrissons TCS Automation Team
	//*******************************************************
	public static boolean ValidateandDownloadRTLOGfromPending(WebDriver driver, WebDriverWait wait, String searchKey,  String TC_no , ArrayList<String> RTLOGnames, String resultpath , String Screenshotpath,XWPFRun xwpfrun, ExtentTest logger)
	{
		boolean res=false;
		try
		{
			resultpath=System.getProperty("user.dir")+"/"+resultpath;
			String AfterAWSLogin_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
			TakeScreenShot.saveScreenShot(AfterAWSLogin_WP1, driver);
			String oldTab = driver.getWindowHandle();
			driver.findElement(By.xpath("//a[@class='particles-button particles-button--regular particles-button--secondary particles-button particles-button--regular particles-button--secondary console-signin-button aws-020551178989' and contains(text(),'AWS Console')]")).click();
			utilityFileWriteOP.writeToLog(TC_no, "NonProd-m-ordering AWS Console", "Clicked!!!",resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "NonProd-m-ordering AWS Console is Clicked!!!");
			logger.log(LogStatus.PASS, logger.addScreenCapture(AfterAWSLogin_WP1));
			//Window or switched Tabs Handling
			ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			//System.out.println(newTab.size());
			newTab.remove(oldTab);
			// change focus to new tab
			driver.switchTo().window(newTab.get(0));        
			//Waiting for AWS Services Page
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
			utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",resultpath);
			// Take Screenshot
			String AWSServicePage_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
			TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
			//Enter Lambda keyword in the search window
			driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("s3");
			utilityFileWriteOP.writeToLog(TC_no, "S3 Keyword", "Entered!!!",resultpath);
			logger.log(LogStatus.PASS, "S3 Keyword is Entered!!!");
			logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
			driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'S3')]")).click();
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Search field is populated with ", "Keyword "+searchKey,resultpath);
			String awsDynamoDB_sc1=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Link related to keyword "+searchKey, "Appeared!!!",resultpath,xwpfrun);
			String awsDynamoDB_sc2=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Link related to keyword "+searchKey, "Clicked!!!",resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Link related to keyword "+searchKey+" is Clicked!!!");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc2));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' rtlog ']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "RTLOG folder ", "Appeared!!!",resultpath,xwpfrun);
			String awsDynamoDB_sc3=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' rtlog ']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Inbound folder ", "Clicked!!!",resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Inbound folder is Clicked!!!");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc3));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' pending ']")));
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Pending folder ", "Appeared!!!",resultpath,xwpfrun);
			String awsDynamoDB_sc4=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc4, driver);
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' pending ']")).click();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Pending folder ", "Clicked!!!",resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Pending folder is Clicked!!!");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc4));
			String awsDynamoDB_sc7=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc7, driver);
			for (String rtlog:RTLOGnames){
				driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).sendKeys(rtlog);
				driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).sendKeys(Keys.RETURN);
				Thread.sleep(1000);
				if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+rtlog+"')]")).isDisplayed())
				{
					res=true;
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog+".dat", "Exists in pending folder!!!",resultpath);
					driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+rtlog+"')]")).click();
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog+".dat", "Clicked!!", resultpath);
					String awsDynamoDB_sc8=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc8, driver);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Download']")));
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "File detail page of "+rtlog+".dat", "Appeared!!!" , resultpath,xwpfrun);
					String awsDynamoDB_sc9=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc9, driver);
					driver.findElement(By.xpath("//span[text()='Download']")).click();
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Download button", "Clicked!!",resultpath,xwpfrun);
					String awsDynamoDB_sc6=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc6, driver);
					Thread.sleep(2000);
					SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog, "Downloaded!!",resultpath,xwpfrun);
					logger.log(LogStatus.PASS,  "The file "+rtlog+" is Downloaded!!!");
					logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc6));
					res = true;
				}
			
			else{
				SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "The file "+rtlog+".dat", "Not Exists in pending folder!!!",resultpath,xwpfrun);
			}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=resultpath+"Screenshot/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			logger.log(LogStatus.FAIL,  "A problem occurred while navigating through AWS S3 bucket");
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,resultpath);
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "A problem occurred while navigating through AWS S3 bucket", "",resultpath,xwpfrun);
		}
		finally
		{
			return res;
		}
	}

}
