package salesProcessing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import AWS.AWS_Utilities;
import SandPiper_All_Functions.RDS_validiation;
import SandPiperUtilities.MyException;
import SandPiperUtilities.ProjectConfigurations;
import SandPiperUtilities.Reporting_Utilities;
import SandPiperUtilities.RowGenerator;
import SandPiperUtilities.excelCellValueWrite;
import SandPiperUtilities.getCurrentDate;
import SandPiperUtilities.utilityFileWriteOP;

public class Extract_RTLOGname_RDS_and_download_RTLOG_AWS {
	//*******************************Initialise variables*************************
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String Downloadfilepath;
	String DriverPath;
	String DriverName;
	String ServerName;
	String SheetName;	
	String TestDataPath;	
	String ResultPath="";
	String Testlogpath;
	String Screenshotpath;
	Integer stepnum=0;
	ExtentReports extent;
	ExtentTest logger;
	//Setting up Environment Variable of org.apache.commons.logging.Log
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
	}
	@Before
	public void setUp() throws Exception {
		//********************Loading Properties from Config file*************************
		DriverPath=ProjectConfigurations.LoadProperties("Sandpiper_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Sandpiper_DriverName");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("Sandpiper_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Sandpiper_SheetName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("Sandpiper_Downloadfilepath");
		Screenshotpath = ProjectConfigurations.LoadProperties("Sandpiper_Screenshotpath");
		ProjectConfigurations.LoadProperties("AutomationServerNum");
		//*********************Setting up Result Path*****************************
		
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath();
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		//********************Creating the Header Details of the HTML log*************************
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);		
		System.setProperty(DriverName, DriverPath);
		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"SOPExtentReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
	}
	@After
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();
		driver.quit();
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);		
		utilityFileWriteOP.writeToLog("*********************************End**********************************",ResultPath );
	}
	@Test
	public void test() throws IOException {
		//*************************Initialising the variables******************************
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Order_Ship_Sales_Stocks";
		String AwsUsername = null;
		String Awspassword = null;
		String Searchkey = null;
		String columnname = null;
		String columnvalue= null;
		String orderid = null;
		String rtlogitems = null;
		String rtlogquantity = null;
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        //-----------------------Extent Report-----------
        logger = extent.startTest("Extract RTLog Name and download RTLog from AWS");
        //-----------------------------------------------	
        String consolidatedScreenshotpath="";
		int r=0;
		try{
			int rows = 0;
			int occurances = 0;			
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));		
			Sheet sheet1 = wrk1.getSheet(SheetName);			
			rows = sheet1.getRows();
			for(r=1;r<rows;r++){
				TestCaseNo = sheet1.getCell(0, r).getContents().trim();				
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();
				AwsUsername = sheet1.getCell(35, r).getContents().trim();
				Awspassword= sheet1.getCell(36, r).getContents().trim();
				sheet1.getCell(37, r).getContents().trim();
				sheet1.getCell(38, r).getContents().trim();
				columnname = sheet1.getCell(39, r).getContents().trim();
				columnvalue = sheet1.getCell(40, r).getContents().trim();
				orderid = sheet1.getCell(16, r).getContents().trim();
				Searchkey= sheet1.getCell(42, r).getContents().trim();
				sheet1.getCell(43, r).getContents().trim();
				rtlogitems = sheet1.getCell(44, r).getContents().trim();
				rtlogquantity = sheet1.getCell(45, r).getContents().trim();
				sheet1.getCell(46, r).getContents().trim();
				String[] columnanames = columnname.split(",");
				String[] columnvalues = columnvalue.split(",");
				String[] orderids = orderid.split(",");
				rtlogitems.split(",");
				rtlogquantity.split(",");
				if(occurances>0){
			    	if(!(TestKeyword.contentEquals(Keyword))){
				    	break;
					}
				}
				if(Keyword.equalsIgnoreCase(TestKeyword)){
					occurances = occurances +1;
					consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//-------------------HTML Header--------------------
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					//----------------------------------------------------
					//----Waiting for ONE minutes---------------
					
					
					System.out.println("Waiting for maximum of 5 minute ");
					
					
					//********************Declaring environment variables for stepnumber*******************
					
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					//----------------------------------------------------

					//******************************Validate Status in RDS********************************
					boolean statusvalidation=false;
					int flag=1;
					System.out.println("Storeid and Sales_process_status validation");
					for(int i=0;i<4;i++){
						Thread.sleep(30000);
						statusvalidation = RDS_validiation.statusvalidateordersInRDS(columnanames, orderids, columnvalues, TestCaseNo, ResultPath,xwpfRun);
						if(statusvalidation==true) {
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Order Details Validation in RDS", "Store id:"+columnvalues[0].trim()+" and sales_process_status:"+columnvalues[1].trim()+" Details Validation in storepick_sales_service.SALES_DETAILS in RDS", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "Store id:"+columnvalues[0].trim()+" and sales_process_status:"+columnvalues[1].trim()+" Details Validation in storepick_sales_service.SALES_DETAILS in RDS is successful");
							flag=1;
							break;
						}
						
						else{
							flag=0;
							continue;
						}
					}
					if(flag==0){
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Order Details Validation in RDS", "Error Occured during store id and sales_process_status Details Validation in storepick_sales_service.SALES_DETAILS in RDS.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Store id:"+columnvalues[0].trim()+" and sales_process_status:"+columnvalues[1].trim()+" Details Validation in storepick_sales_service.SALES_DETAILS in RDS is not correct");
					}
					stepnum++;
					//******************************Extract RTLog name from RDS********************************
					ArrayList<String> RTLOGnames = RDS_validiation.ExtractRTLOGnamefromRDS(columnanames, orderids, columnvalues, TestCaseNo, ResultPath);
					//****************************Put the RTLog name in the Test Data Sheet********************
					for(String i:RTLOGnames){
						System.out.println("Fetching RTLOG names from RDS database");
						System.out.println("RTLOG name: " +i+ " fetched successfully from RDS" );
				        excelCellValueWrite.writeValueToCell(i+",", r, 41, TestDataPath, SheetName);
					}
					if(!(RTLOGnames.get(0).trim().equalsIgnoreCase("null"))){
						System.out.println("Download path is "+System.getProperty("user.dir")+Downloadfilepath);
						//**************************Setting up Chrome Preferences and options***********************
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
						ChromeOptions options = new ChromeOptions();
						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 30);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		 
						driver.get("https://manage.rackspace.com");
						driver.manage().window().maximize();
						//*****************************Loging to AWS*********************
						boolean awslogin = AWS_Utilities.aws_login(driver, wait, TestCaseNo, AwsUsername, Awspassword, Screenshotpath, ResultPath,xwpfRun, logger);
						if(awslogin==true) {
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS LOGIN", "AWS Logged In Successfully.", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "AWS login is successful");
						}
						else{
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS LOGIN", "Error Occured during AWS Login.", "FAIL", ResultPath);
							logger.log(LogStatus.FAIL, "AWS login is unsuccessful");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}
						stepnum++;
						//*******************************Download RTLog**********************************
						boolean downloadrtlogs = AWS_Utilities.ValidateandDownloadRTLOGfromPending(driver, wait, Searchkey, TestCaseNo, RTLOGnames, ResultPath, Screenshotpath,xwpfRun, logger);
						if(downloadrtlogs==true){
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "RTLog Details Download", "RTLog is downloaded Successfully.", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "RTLog is downloaded Successfully.");
						}
						else{
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "RTLog Details Download", "Error Occured RTLog download", "FAIL", ResultPath);
							logger.log(LogStatus.FAIL, "RTLog is not downloaded Successfully.");
							throw new MyException("Test stopped because of failure. Please check the execution log");
						}
						stepnum++;
						
						if(awslogin && statusvalidation && downloadrtlogs) {
							Final_Result="PASS"; 
							excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);							  
							Assert.assertTrue( TestCaseNo+"--"+TestCaseName,true);
						}	
						else {
							Final_Result="FAIL"; 
							excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);					 
							Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
						}
					}else{
						
					}
				}
				
			}
			
		}catch(Exception e){
			stepnum++;
			e.printStackTrace();
			Final_Result="FAIL"; 
		    excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
		    System.out.println(TestCaseNo);
		    System.out.println(TestCaseName);
		    Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}
		finally
		 {   
			//*****************************Assigning value to System variable***********************************
			System.setProperty("stepnumber", stepnum.toString());
			//*************************************************************************************************
		        FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"3.docx");
				doc.write(out1);
			        out1.close();
			        doc.close();
			 //Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
		        RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		 }
			
		}
}