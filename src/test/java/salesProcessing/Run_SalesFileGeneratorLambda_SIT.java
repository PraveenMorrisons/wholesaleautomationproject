package salesProcessing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import AWS.AWS_Utilities;
import SandPiperUtilities.MyException;
import SandPiperUtilities.ProjectConfigurations;
import SandPiperUtilities.Reporting_Utilities;
import SandPiperUtilities.RowGenerator;
import SandPiperUtilities.excelCellValueWrite;
import SandPiperUtilities.getCurrentDate;
import SandPiperUtilities.utilityFileWriteOP;

public class Run_SalesFileGeneratorLambda_SIT {
	//*******************************Initialise variables*************************
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String Downloadfilepath;
	String DriverPath;
	String DriverName;
	String ServerName;
	String SheetName;	
	String TestDataPath;	
	String ResultPath="";
	String Testlogpath;
	String Screenshotpath;
	ExtentReports extent;
	ExtentTest logger;
	//Setting up Environment Variable of org.apache.commons.logging.Log
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
	}
	@Before
	public void setUp() throws Exception {
		//********************Loading Properties from Config file*************************
		DriverPath=ProjectConfigurations.LoadProperties("Sandpiper_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Sandpiper_DriverName");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("Sandpiper_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Sandpiper_SheetName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("Sandpiper_Downloadfilepath");
		Screenshotpath = ProjectConfigurations.LoadProperties("Sandpiper_Screenshotpath");
		//*********************Setting up Result Path*****************************
		
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath();
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		//********************Creating the Header Details of the HTML log*************************
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		//********************Initialising the Driver**********************************************
		System.setProperty(DriverName, DriverPath);
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("safebrowsing.enabled", "false"); 
		chromePrefs.put("download.directory_upgrade", true);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);	
		options.setExperimentalOption("useAutomationExtension", false);
		driver = new ChromeDriver(options);	
		wait=new WebDriverWait(driver, 30);			
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		 
		driver.get("https://manage.rackspace.com");
		driver.manage().window().maximize();
		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"SOPExtentReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
	}
	@After
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();
		driver.quit();
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);		
		utilityFileWriteOP.writeToLog("*********************************End**********************************",ResultPath);
	}
	@Test
	public void test() throws IOException {
		//*************************Initialising the variables******************************
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Order_Ship_Sales_Stocks";
		String AwsUsername = null;
		String Awspassword = null;
		String Searchkey = null;
		String jobname = null;
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        //-----------------------Extent Report-----------
		logger = extent.startTest("Run SalesFileGeneratorLamda_SIT job");
		//-----------------------------------------------
        String consolidatedScreenshotpath="";
		int r=0;
		try{
			int rows = 0;
			int occurances = 0;			
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));		
			Sheet sheet1 = wrk1.getSheet(SheetName);			
			rows = sheet1.getRows();
			//*******************Assigning values to the variables**************************
			for(r=1;r<rows;r++){
				TestCaseNo = sheet1.getCell(0, r).getContents().trim();				
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();
				AwsUsername = sheet1.getCell(35, r).getContents().trim();
				Awspassword= sheet1.getCell(36, r).getContents().trim();
				Searchkey = sheet1.getCell(37, r).getContents().trim();
				jobname = sheet1.getCell(38, r).getContents().trim();
				if(occurances>0){
			    	if(!(TestKeyword.contentEquals(Keyword))){
				    	break;
			    	}
				}
				if(Keyword.equalsIgnoreCase(TestKeyword)){
					occurances = occurances +1;
					consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//-------------------HTML Header--------------------
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					//********************Declaring environment variables for stepnumber*******************
					Integer stepnum=0;
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					//----------------------------------------------------
					
					//******************************Login to AWS****************************
					boolean AwsLogin = AWS_Utilities.aws_login(driver, wait, TestCaseNo, AwsUsername, Awspassword, Screenshotpath, ResultPath,xwpfRun, logger);
					if(AwsLogin==true) {
						 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS LOGIN", "AWS Logged In Successfully.", "PASS", ResultPath);
						 logger.log(LogStatus.PASS, "AWS login is successful");
					}
					else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS LOGIN", "Error Occured during AWS Login.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "AWS login is unsuccessful");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					//**********************Method to trigger Lamda job****************************
					boolean LambdaJobRun = AWS_Utilities.Lamda_job_trigger(driver, wait, TestCaseNo, Searchkey, jobname, ResultPath, Screenshotpath,xwpfRun, logger);
					if(LambdaJobRun==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Lambda JOB", "Lambda JOB triggered Successfully.", "PASS", ResultPath);
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Lambda JOB", "Error Occured during Lambda JOB.", "FAIL", ResultPath);
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					//*****************************Assigning value to System variable***********************************
					System.setProperty("stepnumber", stepnum.toString());
					//*************************************************************************************************
					//******************Validating the Result****************************
					if((AwsLogin) && LambdaJobRun) {
						 Final_Result="PASS"; 
						 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);							  
						 Assert.assertTrue( TestCaseNo+"--"+TestCaseName,true);
					}else {
						 Final_Result="FAIL"; 
						 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);					 
						 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
					}
				}
				
			}
			
		}catch(Exception e){
			
			e.printStackTrace();
			Final_Result="FAIL"; 
		    excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
		    System.out.println(TestCaseNo);
		    System.out.println(TestCaseName);
		    Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}
		finally
		{   
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"2.docx");
			doc.write(out1);
			out1.close();
			doc.close();
			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		}
	}
}
