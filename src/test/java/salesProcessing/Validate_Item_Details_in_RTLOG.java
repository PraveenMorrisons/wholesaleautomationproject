package salesProcessing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import SandPiper_All_Functions.RDS_validiation;
import SandPiperUtilities.ProjectConfigurations;
import SandPiperUtilities.Reporting_Utilities;
import SandPiperUtilities.RowGenerator;
import SandPiperUtilities.excelCellValueWrite;
import SandPiperUtilities.getCurrentDate;
import SandPiperUtilities.utilityFileWriteOP;

public class Validate_Item_Details_in_RTLOG {
	//*******************************Initialise variables*************************
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String Downloadfilepath;
	String DriverPath;
	String DriverName;
	String ServerName;
	String SheetName;	
	String TestDataPath;	
	String ResultPath="";
	String Testlogpath;
	String Screenshotpath;
	ExtentReports extent;
	ExtentTest logger;
	//Setting up Environment Variable of org.apache.commons.logging.Log
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
	}
	
	@Before
	public void setUp() throws Exception {
		//********************Loading Properties from Config file*************************	
		DriverPath=ProjectConfigurations.LoadProperties("Sandpiper_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Sandpiper_DriverName");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("Sandpiper_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Sandpiper_SheetName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("Sandpiper_Downloadfilepath");
		Screenshotpath = ProjectConfigurations.LoadProperties("Sandpiper_Screenshotpath");
		//*********************Setting up Result Path*****************************
		
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath();
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		//********************Creating the Header Details of the HTML log*************************
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);		
		System.setProperty(DriverName, DriverPath);
		//******************************Setup chrome Preferences*****************************
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("download.default_directory", Downloadfilepath);		
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);		
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"SOPExtentReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
		
	}
	
	
	
	

	@After
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();
		
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);		
		utilityFileWriteOP.writeToLog("*********************************End**********************************",ResultPath);
	}

	
	
	
	
	@Test
	public void test() throws IOException {
		//*************************Initialising the variables******************************
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Order_Ship_Sales_Stocks";
		String orderid = null;
		String rtlogitems = null;
		String rtlogquantity = null;
		String totalamount = null;
		String rtlogtrandate = null;
		String rtlogname = null;
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        //-----------------------Extent Report-----------
        logger = extent.startTest("Validate Item Details in RTLog");
        //-----------------------------------------------	
        String consolidatedScreenshotpath="";
		int r=0;
		try{
			int rows = 0;
			int occurances = 0;			
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));		
			Sheet sheet1 = wrk1.getSheet(SheetName);			
			rows = sheet1.getRows();
			for(r=1;r<rows;r++){
				TestCaseNo = sheet1.getCell(0, r).getContents().trim();				
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();
				sheet1.getCell(39, r).getContents().trim();
				sheet1.getCell(40, r).getContents().trim();
				orderid = sheet1.getCell(16, r).getContents().trim();
				sheet1.getCell(42, r).getContents().trim();
				totalamount = sheet1.getCell(43, r).getContents().trim().replace("//.", "");
				rtlogitems = sheet1.getCell(44, r).getContents().trim();
				rtlogquantity = sheet1.getCell(45, r).getContents().trim();
				rtlogtrandate = sheet1.getCell(46, r).getContents().trim();
				rtlogname = sheet1.getCell(41, r).getContents().trim();
				String[] items = rtlogitems.split(",");
				String[] quantity = rtlogquantity.split(",");
				String[] rtlognames = rtlogname.split(",");
				if(occurances>0){
			    	if(!(TestKeyword.contentEquals(Keyword))){
			    		break;
					}
			    }
				if(Keyword.equalsIgnoreCase(TestKeyword)){
					occurances = occurances +1;
					consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//-------------------HTML Header--------------------
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					//********************Declaring environment variables for stepnumber*******************
					Integer stepnum=0;
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					//----------------------------------------------------
					//----------------------------------------------------
					//****************************Validate Item Details in RTLOG**********************
					boolean rtlogvalidation= RDS_validiation.ValidateItemDetailsInRTLOG(rtlognames, System.getProperty("user.dir")+Downloadfilepath, rtlogtrandate, items, quantity, totalamount,orderid, TestCaseNo, ResultPath,xwpfRun);
					if(rtlogvalidation) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "RTLog Details Validation", "RTLog Details Validated Successfully.", "PASS", ResultPath);
						Final_Result="PASS"; 
						logger.log(LogStatus.PASS, "RTLog Details Validated Successfully.");
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);							  
						Assert.assertTrue( TestCaseNo+"--"+TestCaseName,true);
						System.out.println("\n\n\n");
						System.out.println("--------------------------------------------------------------");
						System.out.println("  Result for Sales processing of Sandpiper Order: "+Final_Result);
						System.out.println("--------------------------------------------------------------");
					 }
					else {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "RTLog Details Validation", "Error Occured RTLog Details Validation.", "FAIL", ResultPath);
						Final_Result="FAIL"; 
						logger.log(LogStatus.FAIL, "RTLog Details Validated NOT Successfully.");
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);					 
						Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			Final_Result="FAIL"; 
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
		    Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}finally
		{   
			 FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"4.docx");
			 doc.write(out1);
			 out1.close();
			 RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
			 doc.close();
		 }
			
		}
}
