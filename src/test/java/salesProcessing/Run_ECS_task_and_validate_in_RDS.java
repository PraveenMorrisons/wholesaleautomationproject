package salesProcessing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import AWS.AWS_Utilities;
import SandPiper_All_Functions.SandPiper_GetCall;
import SandPiper_All_Functions.RDS_validiation;
import SandPiperUtilities.MyException;
import SandPiperUtilities.ProjectConfigurations;
import SandPiperUtilities.Reporting_Utilities;
import SandPiperUtilities.RowGenerator;
import SandPiperUtilities.excelCellValueWrite;
import SandPiperUtilities.getCurrentDate;
import SandPiperUtilities.utilityFileWriteOP;

public class Run_ECS_task_and_validate_in_RDS {
	//************************************Declaring Variables********************************
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	String TestDataPath;
	String TemporaryFilePath;
	String ResultPath="";
	String Screenshotpath;
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	ExtentReports extent;
	ExtentTest logger;
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
		}


	@Before
	public void setUp() throws Exception {
		//********************Initialising data from configuration file*********************
		DriverPath=ProjectConfigurations.LoadProperties("Sandpiper_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Sandpiper_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Sandpiper_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("Sandpiper_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("Sandpiper_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Sandpiper_SheetName");
		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("Sandpiper_ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("Sandpiper_TemporaryFilePath");
		Screenshotpath = ProjectConfigurations.LoadProperties("Sandpiper_Screenshotpath");
		
		//*********************Setting up Result Path*****************************
		
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath();
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		//*********************Setting up Web Driver***********************************
		System.setProperty(DriverName, DriverPath);
		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"SOPExtentReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
	}

	@After
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();
		driver.quit();
		utilityFileWriteOP.writeToLog("*********************************END**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
	}

	@Test
	public void test() throws IOException {
		//********************************Initialising variables****************************
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Order_Ship_Sales_Stocks";
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String ColumnNames = null;
		String ItemIDs = null;
		String AllOrderHeaders =null;
		String AllMessageHeaders = null;
		String AllShipDetailsHeaders = null;
		String AllBilltoDetailsHeaders = null;
		String AllBilltoTaxDetailsHeaders = null;
		String consolidatedScreenshotpath="";
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();
		String columnname = null; 
		String AwsUsername = null;
		String Awspassword = null;
		String Searchkey = null;
		String jobname = null;
		HashMap<String, String> Payload_map = null;
		ArrayList<String> ItemDetailsList = new ArrayList<String>();
		//-----------------------Extent Report-----------
		logger = extent.startTest("Run ECS task and validate in RDS");
		//-----------------------------------------------
		int r = 0;
		try{
			int rows = 0;
			int occurances = 0;
			int ItemDetailsoccurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
			rows = sheet1.getRows();
			int itemdetailsrows = sheet2.getRows();
			int j=1;
			int item_row=-1;
			for( j=1; j<itemdetailsrows; j++) {
				Keyword = sheet2.getCell(0, j).getContents().trim();
				if(ItemDetailsoccurances>0){
			    	if(!(TestKeyword.contentEquals(Keyword))){
			    		break;
				    }
				}
				item_row=j;
				ItemIDs = sheet2.getCell(1, j).getContents().trim();
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					ItemDetailsoccurances=ItemDetailsoccurances+1;
					String[] AllItemsInItemDetails = ItemIDs.split(",");
					for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
						String ItemDetailsJson = sheet2.getCell(c, j).getContents().trim();
						ItemDetailsList.add(ItemDetailsJson);
					}
				}
			}
			for(r=1; r<rows; r++) {
				TestCaseNo = sheet1.getCell(0, r).getContents().trim();
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();
				ProxyHostName = sheet1.getCell(5, r).getContents().trim();
				ProxyPort = sheet1.getCell(6, r).getContents().trim();
				SYSUserName = sheet1.getCell(7, r).getContents().trim();
				SYSPassWord = sheet1.getCell(8, r).getContents().trim();
				TargetHostName = sheet1.getCell(9, r).getContents().trim();
				TargetPort = sheet1.getCell(10, r).getContents().trim();
				TargetHeader = sheet1.getCell(11, r).getContents().trim();
				UrlTail = sheet1.getCell(12, r).getContents().trim();
				ApiKey = sheet1.getCell(13, r).getContents().trim();
				AuthorizationKey = sheet1.getCell(14, r).getContents().trim();
				AuthorizationValue =sheet1.getCell(15, r).getContents().trim();
				AllOrderIDs = sheet1.getCell(16, r).getContents().trim();
				AllMessageTypes = sheet1.getCell(17, r).getContents().trim();
				AllShipToLocationIds = sheet1.getCell(18, r).getContents().trim();
				sheet1.getCell(19, r).getContents().trim();
				AllOrderAttributes = sheet1.getCell(21, r).getContents().trim();
				AllMessageAttributes = sheet1.getCell(22, r).getContents().trim();
				sheet1.getCell(23, r).getContents().trim();
				AllBillToAddressAttributes = sheet1.getCell(24, r).getContents().trim();
				AllBillToTaxAddressAttributes = sheet1.getCell(25, r).getContents().trim();
				ColumnNames = sheet1.getCell(31, r).getContents().trim();
				AllOrderHeaders = sheet1.getCell(21, 0).getContents().trim();
				AllMessageHeaders = sheet1.getCell(22, 0).getContents().trim();
				AllShipDetailsHeaders = sheet1.getCell(23, 0).getContents().trim();
				AllBilltoDetailsHeaders = sheet1.getCell(24, 0).getContents().trim();
				AllBilltoTaxDetailsHeaders = sheet1.getCell(25, 0).getContents().trim();
				columnname = sheet1.getCell(34, r).getContents().trim();
				AwsUsername = sheet1.getCell(35, r).getContents().trim();
				Awspassword= sheet1.getCell(36, r).getContents().trim();
				Searchkey = sheet1.getCell(47, r).getContents().trim();
				jobname = sheet1.getCell(48, r).getContents().trim();
				String shippedquantity=sheet1.getCell(33, r).getContents().trim();
				String[] ship= shippedquantity.split(",");
				if(occurances>0){
			        if(!(TestKeyword.contentEquals(Keyword))){
			        	break;
				    }
				}
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					occurances=occurances+1;
					Payload_map = new HashMap<String, String>();
					String OrderID[] = AllOrderIDs.split(",");
					String ShipToLocationId[] = AllShipToLocationIds.split(",");
					AllMessageTypes.split(",");
					ColumnNames.split(",");
				// static order attributes...................
					String OrderHeaders[] = AllOrderHeaders.split(",");
					String MessageHeaders[] = AllMessageHeaders.split(",");
					String ShipDetailsHeaders[] = AllShipDetailsHeaders.split(",");
					String BilltoDetailsHeaders[] = AllBilltoDetailsHeaders.split(",");
					String BilltoTaxDetailsHeaders[] = AllBilltoTaxDetailsHeaders.split(",");
				for(int i=0; i<OrderHeaders.length; i++) {
					Payload_map.put(OrderHeaders[i], AllOrderAttributes.split(",")[i]); 
				}
				for(int i=0; i<MessageHeaders.length; i++) {
					Payload_map.put(MessageHeaders[i], AllMessageAttributes.split(",")[i]); 
				}
				for(int i=0; i<ShipDetailsHeaders.length; i++) {
					Payload_map.put(ShipDetailsHeaders[i], AllShipDetailsHeaders.split(",")[i]); 
				}
				for(int i=0; i<BilltoDetailsHeaders.length; i++) {
					Payload_map.put(BilltoDetailsHeaders[i], AllBillToAddressAttributes.split(",")[i]); 
				}
				for(int i=0; i<BilltoTaxDetailsHeaders.length; i++) {
					Payload_map.put(BilltoTaxDetailsHeaders[i], AllBillToTaxAddressAttributes.split(",")[i]); 
				}
				boolean rdsValidation = true;
				boolean login = true;
				boolean taskrun = true;
				File screenshotpath = new File(ResultPath+"Screenshot/"+TestCaseNo+"/");
				screenshotpath.mkdirs();
				consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
				//-------------------HTML Header--------------------
				Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
				//----------------------------------------------------
				//********************Declaring environment variables for stepnumber*******************
				Integer stepnum=0;
				try{
					stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
				}catch(Exception e){
					System.setProperty("stepnumber", "1");
					stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
				}
				//************************************************************************************
				Payload_map.put("locationId", "9"+ShipToLocationId[0]); 
				ArrayList<String> priceConfirmedEachesAmountList = SandPiper_GetCall.priceConfirmedEachesAmountList(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[0], TestCaseNo);
				ArrayList<String> priceconfirmedTax= SandPiper_GetCall.priceConfirmedtaxrateList(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[0], TestCaseNo);
				ArrayList<String> MinList = SandPiper_GetCall.skuMinExtract(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[0], TestCaseNo,ResultPath,xwpfRun);
				if(MinList==null) {
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Order Service Call to Extarct skuMin", "Error Occured during Extarction skuMin.", "FAIL", ResultPath);
					logger.log(LogStatus.FAIL, "Error Occured during Extarction skuMin.");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			    }else
			    {
			    	Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Order Service Call to Extarct skuMin", "Extarcted skuMin Successfully.", "PASS", ResultPath);
			    	logger.log(LogStatus.PASS, "Extracted skuMin Successfully.");
				}
				stepnum++;
				//*************************Setting up driver properties****************************************
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				chromePrefs.put("download.prompt_for_download", "false");
				chromePrefs.put("download.", "false");
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", chromePrefs);
				options.setExperimentalOption("useAutomationExtension", false);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(cap);
				wait=new WebDriverWait(driver, 30);			
				driver.manage().deleteAllCookies();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		 
				driver.get("https://manage.rackspace.com");
				driver.manage().window().maximize();
				//**********************************Login to AWS*****************************************************
				boolean AwsLogin = AWS_Utilities.aws_login(driver, wait, TestCaseNo, AwsUsername, Awspassword, Screenshotpath, ResultPath, xwpfRun, logger);
				if(AwsLogin==false){
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS Login", "Logged in to AWS successfully", "FAIL", ResultPath);
					logger.log(LogStatus.FAIL, "AWS login is unsuccessful");
					throw new MyException("Test stopped because of failure. Please check the execution log");
				}else{
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS Login", "Error occured while logging in to the AWS", "PASS", ResultPath);
					logger.log(LogStatus.PASS, "AWS login is successful");
				}
				login = login && AwsLogin;
				stepnum++;
				//**************************************Run ECS Task************************************************
				boolean LambdaJobRun = AWS_Utilities.Ecs_task_runner_job_trigger(driver, wait, TestCaseNo, Searchkey, jobname, ResultPath, Screenshotpath, xwpfRun, logger);
				if(LambdaJobRun==true) {
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "ECS Task Runner JOB", "ECS Task Runner JOB triggered Successfully.", "PASS", ResultPath);
					logger.log(LogStatus.PASS, "ECS Task Runner JOB triggered Successfully.");
				}else{
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "ECS Task Runner JOB", "Error Occured during ECS Task Runner JOB.", "FAIL", ResultPath);
					logger.log(LogStatus.FAIL, "Error Occured during ECS Task Runner JOB.");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
				taskrun = taskrun && LambdaJobRun;
				stepnum++;
				//**************************************RDS Validations************************************************
				Boolean McColls_I32a_ValidateInRDS = RDS_validiation.validateInRDS_MultipleOrder(OrderID[0], columnname.split(","), ItemIDs.split(","), Payload_map, ItemDetailsList, priceConfirmedEachesAmountList, MinList, ResultPath, TestCaseNo,r,TestDataPath,SheetName, priceconfirmedTax,ship, xwpfRun,ItemDetailsSheetName,item_row);
				if(McColls_I32a_ValidateInRDS==true) {
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Order Details Validation in RDS", "Order Details Validated in RDS Successfully.", "PASS", ResultPath);
					logger.log(LogStatus.PASS, "RDS validation is done and passed in storepick_sales_service.SALES_DETAILS .");
				}else{
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Order Details Validation in RDS", "Error Occured during Order Details Validation in RDS.", "FAIL", ResultPath);
					logger.log(LogStatus.PASS, "RDS validation is done and passed in storepick_sales_service.SALES_DETAILS .");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
				rdsValidation = rdsValidation && McColls_I32a_ValidateInRDS;
				stepnum++;
				//**************************************************************************************************
				//*****************************Assigning value to System variable***********************************
				System.setProperty("stepnumber", stepnum.toString());
				//*************************************************************************************************
				if( (rdsValidation) && (login) && (taskrun) ) {
					Final_Result = "PASS";
					excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
					Assert.assertTrue(TestCaseName, true);
				}else{
					Final_Result = "FAIL";
					excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
					Assert.assertTrue(TestCaseName, false);
				}
			}
		}
	}catch(Exception e) {
		e.printStackTrace();
		Final_Result = "FAIL";
		excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
		Assert.assertTrue(TestCaseName, false);
	}finally{   
		FileOutputStream out = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
		doc.write(out);
		out.close();
		doc.close();
		RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
	}
}

}
