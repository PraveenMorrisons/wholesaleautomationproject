package TestScripts_OrderCreation;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import SandPiperUtilities.*;
import SandPiper_All_Functions.RDS_validiation;
import SandPiper_All_Functions.SandPiper_GetCall;
import SandPiper_All_Functions.SandPiper_PostCall;

public class CreateWholesaleOrder {

	//String URL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	String TestDataPath;
	String TemporaryFilePath;
	String ResultPath="";
	ExtentReports extent;
	ExtentTest logger;
	
	@Before
	public void setUp() throws Exception {
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************", ResultPath);	
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(), ResultPath);
		//********************************Load values from preoperties file*********************************
		DriverPath=ProjectConfigurations.LoadProperties("Sandpiper_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Sandpiper_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Sandpiper_DriverType");
		TestDataPath=ProjectConfigurations.LoadProperties("Sandpiper_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Sandpiper_SheetName");
		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("Sandpiper_ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("Sandpiper_TemporaryFilePath");
		//*********************************Initialising Result Path Variable*************************************

		//*********************Setting up Result Path*****************************
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath();
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		//*********************Creating Header in Report********************************
		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"SOPExtentReport.html", true);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
	}

	@After
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		utilityFileWriteOP.writeToLog("*********************************End**********************************",ResultPath);
	}
	
	

	@Test
	public void test() throws IOException {
		//************************************Initialize Variables***********************************************
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Order_Ship_Sales_Stocks";	
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;
		String ColumnNames = null;
		String ItemIDs = null;
		String Quantities="0";
		String OrderCount="";   // Default value
		//-----------------------Extent Report-----------
		logger = extent.startTest("CreateWholesaleOrder");
		//-----------------------------------------------
		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();
		String consolidatedScreenshotpath="";
		//-------------------------------------------------
		int r = 0;
		try{
			int rows = 0;
			int occurances = 0;
			int ItemDetailsoccurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
			rows = sheet1.getRows();
			int itemdetailsrows = sheet2.getRows();
			System.out.println(itemdetailsrows);
			//************************Identifying Keyword*******************************
			for(int i=1; i<itemdetailsrows; i++) {
				Keyword = sheet2.getCell(0, i).getContents().trim();
				System.out.println(sheet2.getCell(0, i).getContents().trim());
				if(ItemDetailsoccurances>0){
					if(!(TestKeyword.contentEquals(Keyword))){
						break;
					}
				}
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					ItemDetailsoccurances=ItemDetailsoccurances+1;
					ItemIDs = sheet2.getCell(1, i).getContents().trim();
					System.out.println("Matched "+Keyword + sheet2.getCell(0, i).getContents().trim());
				}
			}
			
			System.setProperty("stepnumber", "1");
			Integer stepnum=Integer.parseInt(System.getProperty("stepnumber"));
			//*************************************Create Order ID*************************************
			
			for(r=1; r<rows; r++) {
					String OrderIDGeneration = RDS_validiation.TestData_OrderIdGeneration(TestDataPath, SheetName, TestCaseNo, ResultPath, r);
					 if(!(OrderIDGeneration==null)) {
						 System.out.println("Order Id created successfully");
						 utilityFileWriteOP.writeToLog("Order ID Generation Script", "Successfully Created Order IDs for "+SheetName,ResultPath);
						 logger.log(LogStatus.PASS, "Successfully Created Order IDs for "+SheetName);
					 }
					 else {
						 utilityFileWriteOP.writeToLog("Order ID Generation Script", "Failed to create Order IDs for "+SheetName,ResultPath);
						 Assert.assertTrue(TestCaseName, false);
						 logger.log(LogStatus.FAIL, "Failed to create Order IDs for "+SheetName);
					 }
				
				 //-----------------------  TestData -------End of Order Id Creation----------------------------
				
				
				TestCaseNo = sheet1.getCell(0, r).getContents().trim();
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();
				ProxyHostName = sheet1.getCell(5, r).getContents().trim();
				ProxyPort = sheet1.getCell(6, r).getContents().trim();
				SYSUserName = sheet1.getCell(7, r).getContents().trim();
				SYSPassWord = sheet1.getCell(8, r).getContents().trim();
				TargetHostName = sheet1.getCell(9, r).getContents().trim();
				TargetPort = sheet1.getCell(10, r).getContents().trim();
				TargetHeader = sheet1.getCell(11, r).getContents().trim();
				UrlTail = sheet1.getCell(12, r).getContents().trim();
				ApiKey = sheet1.getCell(13, r).getContents().trim();
				AuthorizationKey = sheet1.getCell(14, r).getContents().trim();
				AuthorizationValue =sheet1.getCell(15, r).getContents().trim();
				AllOrderIDs = OrderIDGeneration;
				AllMessageTypes = sheet1.getCell(17, r).getContents().trim();
				AllShipToLocationIds = sheet1.getCell(18, r).getContents().trim();
				ShipToDeliverAt = sheet1.getCell(19, r).getContents().trim();
				AllOrderAttributes = sheet1.getCell(21, r).getContents().trim();
				AllMessageAttributes = sheet1.getCell(22, r).getContents().trim();
				AllShipToAddressAttributes = sheet1.getCell(23, r).getContents().trim();
				AllBillToAddressAttributes = sheet1.getCell(24, r).getContents().trim();
				AllBillToTaxAddressAttributes = sheet1.getCell(25, r).getContents().trim();
				Quantities = sheet1.getCell(59, r).getContents().trim();
				OrderCount=sheet1.getCell(60, r).getContents().trim();
				System.out.println("All OrderId is "+AllOrderIDs);
				
				ColumnNames = sheet1.getCell(31, r).getContents().trim();
				
				if(occurances>0){
					if(!(TestKeyword.contentEquals(Keyword))){
						break;
					}

				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					occurances=occurances+1;

					String OrderID[] = AllOrderIDs.split(",");
					String ShipToLocationId[] = AllShipToLocationIds.split(",");
					String MessageType[] = AllMessageTypes.split(",");
					ColumnNames.split(",");
					//System.out.println(ItemIDs);
					String AllItemIDs[] = ItemIDs.split(",");

					String qtys[]= Quantities.split(",");
					boolean post = true;
					boolean get = true;
					int order = 0;
					File screenshotpath = new File(System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_"; 
					//-------------------HTML Header--------------------
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);

					//----------------------------------------------------
					
					//--------------------------TestData --------OrderID Creation---------------------------------
					//--------------------------TestData --------Item Details Generation---------------------------------			 
					 
					for(int i=0; i<AllItemIDs.length; i++) {
						String quantityType = SandPiper_All_Functions.SandPiper_GetCall.GetOrderQuantityType_WebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIDs[i], TestCaseNo, ResultPath);
						String RestGetItemDetails = SandPiper_All_Functions.SandPiper_GetCall.GetCallItemDetailsFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIDs[i], "skuPin",quantityType, String.valueOf(i+1), qtys[i], TestCaseNo, ResultPath);
						excelCellValueWrite.writeValueToCell(RestGetItemDetails, r, 2+i, TestDataPath, ItemDetailsSheetName);
					}

		
		 
	
					//--------------------------TestData --------Item Details Generation---------------------------------
					
					//*******************************Create Order******************************
					Boolean RestPost = SandPiper_PostCall.CreateOrder(TestDataPath, ItemDetailsSheetName, ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], OrderCount,MessageType[0], ShipToLocationId[0], ShipToDeliverAt, AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes, AllItemAttributes, TestKeyword, TestCaseNo,ResultPath,xwpfRun);
					if(RestPost==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Order Created Successfully.");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error Occured during Order Creation.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					post = post && RestPost;
					stepnum++;
					//**********************************Validate the created order status***********************
					Boolean OrderStatus =  SandPiper_GetCall.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "confirmed", TestCaseNo,ResultPath,xwpfRun);
					if(OrderStatus==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Order Status Validation", "Order Status Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Order Status Validated Successfully.");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Order Status Validation", "Error Occured during Order Status Validation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error Occured during Order Status Validation.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					//******************************Validate Item Status*************************
					Boolean RestGet = SandPiper_GetCall.GetCallItemStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "confirmed", TestCaseNo,ResultPath,xwpfRun,logger);
					if(RestGet==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Item Status Validated Successfully.");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error Occured during Item Status Validation.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					get = get && RestGet;
					Thread.sleep(5000);
					if( (post) && (get)) {
						Final_Result = "PASS";
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
						Assert.assertTrue(TestCaseName, true);
					}
					else {
						Final_Result = "FAIL";
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
						Assert.assertTrue(TestCaseName, false);
					}
				}
				excelCellValueWrite.writeValueToCell(AllOrderIDs, r, 16, TestDataPath, SheetName);
			}
			
			System.setProperty("stepnumber", stepnum.toString());
		}catch(Exception e) {
			e.printStackTrace();
			Final_Result = "FAIL";
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
			Assert.assertTrue(TestCaseName, false);
		}

		finally
		{   
			
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
			doc.write(out1);
			out1.close();
			doc.close();
			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		}


	}

}
