package SandPiper_All_Functions;
import org.openqa.selenium.By;



import java.util.Arrays;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import SandPiperUtilities.*;

import java.awt.Robot;
import java.awt.event.KeyEvent;




public class Amazon_Utilities {
	

	public static boolean homeLink(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		boolean logo=false;
		try{
			
				
			
				driver.findElement(By.xpath("//span[contains(text(),'Home')]")).click();
				utilityFileWriteOP.writeToLog(TC_no, "Go to HomePage", "PASS",ResultPath);
				
			
				 String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         
		          
		        	 
		        	 res=true;
			 
		        
			
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Switched to Home", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	public static boolean homeLogo(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		boolean logo=false;
		try{
			
				
			
				driver.findElement(By.xpath("//img[@src='app/images/morrisons_logo.svg']")).click();
				utilityFileWriteOP.writeToLog(TC_no, "Go to HomePage", "PASS",ResultPath);
				
			
				 String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         
		         Thread.sleep(3000);
		         
		         try
		         {
		          logo =driver.findElement(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")).isDisplayed();
		         }
		         catch(Exception e)
		         {
		        	 
		         }
		        
		         
		         if (logo) 
		         {
		        	 String AfterlogoValidate=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
			         TakeScreenShot.saveScreenShot(AfterlogoValidate, driver);
			         
		        	 
		        	 res=true;
					
				}
		         
		         else
		         {
		        	 res=false;
		         }
		         
		        
		         return res;
			
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Switched to Home", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	
	

	public static boolean SwitchtoBottom(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		boolean logo=false;
		try{
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control' and contains(text(),'ADVANCED SEARCH ')]"))).isDisplayed();
			
				driver.findElement(By.xpath("//a[@class='pdlinks' and contains(text(),'Go to Bottom ')]")).click();
				utilityFileWriteOP.writeToLog(TC_no, "Switched to Bottom ", "PASS",ResultPath);
				
			
				 String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         
		         Thread.sleep(3000);
		         
		         try
		         {
		          logo =driver.findElement(By.xpath("//img[@src='app/images/morrisons-logo.svg']")).isDisplayed();
		         }
		         catch(Exception e)
		         {
		        	 
		         }
		        
		         
		         if (logo) 
		         {
		        	 String AfterlogoValidate=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
			         TakeScreenShot.saveScreenShot(AfterlogoValidate, driver);
			         
		        	 
		        	 res=true;
					
				}
		         
		         else
		         {
		        	 res=false;
		         }
		         
		      
		         return res;
			
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Switched to Bottom", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	

	
	

	public static boolean SwitchtoTop(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		boolean logo=false;
		try{
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control' and contains(text(),'ADVANCED SEARCH ')]"))).isDisplayed();
			
				try {
				
				WebElement el=	driver.findElement(By.xpath("//button[@class='topbtn' and @id='topBtn']"));
				if(el.isDisplayed())
				{
					el.click();
					utilityFileWriteOP.writeToLog(TC_no, "Switched to Top ", "PASS",ResultPath);
				}
					
				} 
				
				catch (Exception e) 
				{
					utilityFileWriteOP.writeToLog(TC_no, "No. of records are less, Top not Required ", "PASS",ResultPath);
				}
				
				
				
			
				 String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         
		         Thread.sleep(3000);
		         
		         try
		         {
		          logo =driver.findElement(By.xpath("//img[@src='app/images/morrisons_logo.svg']")).isDisplayed();
		         }
		         catch(Exception e)
		         {
		        	 
		         }
		         
		         
		         if (logo) 
		         {
		        	 String AfterlogoValidate=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
			         TakeScreenShot.saveScreenShot(AfterlogoValidate, driver);
			         
		        	 
		        	 res=true;
					
				}
		         
		         else
		         {
		        	 res=false;
		         }
		         
		     
		         return res;
			
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Switched to Top", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	

	
	
	public static boolean OrderSearchValidation(String Category, String value,WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  
                  String[] ordDets=new String[15];
                  for(int i=0;i<13;i++)
                  {
                	  ordDets[i]=driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td["+(i+1)+"]")).getText();
                  }
                  ordDets[6]=ordDets[6].substring(0,10);
                  
                  if (Category.equalsIgnoreCase("channel"))
                  {
                	  if (ordDets[1].equalsIgnoreCase(value))
                	  {
                		  utilityFileWriteOP.writeToLog(TC_no, "Validation of Channel in Order Search  ", "PASS",ResultPath);
                	  }
                	  else
                	  {
                		  res=false;
                		  utilityFileWriteOP.writeToLog(TC_no, "Validation of Channel in Order Search  ", "FAIL",ResultPath);
                	  }
                  }
                  if (Category.equalsIgnoreCase("status"))
                  {
                	  if (ordDets[2].equalsIgnoreCase(value))
                	  {
                		  utilityFileWriteOP.writeToLog(TC_no, "Validation of Status in Order Search  ", "PASS",ResultPath);
                	  }
                	  else
                	  {
                		  res=false;
                		  utilityFileWriteOP.writeToLog(TC_no, "Validation of Status in Order Search  ", "FAIL",ResultPath);
                	  }
                  }
                  if (Category.equalsIgnoreCase("OrderDate"))
                  {
                	  if (ordDets[6].equalsIgnoreCase(value))
                	  {
                		  utilityFileWriteOP.writeToLog(TC_no, "Validation of OrderDate in Order Search  ", "PASS",ResultPath);
                	  }
                	  else
                	  {
                		  res=false;
                		  utilityFileWriteOP.writeToLog(TC_no, "Validation of OrderDate in Order Search  ", "FAIL",ResultPath);
                	  }
                  }
                  
                  
                  res=true;
                  return res;
           }	
           catch(Exception e)
           {
                  res=false;    
                  System.out.println("The exception is: "+e);
                  utilityFileWriteOP.writeToLog(TC_no, "Order Details ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
           
    }
	
	public static boolean Change_Customer(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		  try{
			  
			  driver.findElement(By.xpath("//a[@class='Product-title-anchor' and contains(text(),'Change Customer')]")).click();
			  
			  Thread.sleep(3000);
			  if (driver.findElement(By.xpath("//h3[@class='margin-zero product-title-top']")).getText().equalsIgnoreCase("Amazon"))
			  {
				  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='app/images/P&H.png']"))).isDisplayed();
				// Take Screenshot			   
		  	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		  	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
				  driver.findElement(By.xpath("//img[@src='app/images/P&H.png']")).click();
				  utilityFileWriteOP.writeToLog(TC_no, "Change to PH Customer ", "Pass",ResultPath);
			  }
			  else
			  {
				  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='app/images/Amazon.png']"))).isDisplayed();
				// Take Screenshot			   
		  	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		  	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
				  driver.findElement(By.xpath("//img[@src='app/images/Amazon.png']")).click();
				  utilityFileWriteOP.writeToLog(TC_no, "Change to Amazon Customer ", "Pass",ResultPath);
			  }
			  
			  res=true;
  			
  	       
       	      return res;
          }
          catch(Exception e)
          {
                 res=false;  
                
                 utilityFileWriteOP.writeToLog(TC_no, "Change Customer Link ", "Not Found!!!",ResultPath);
                 System.out.println(e);
                 // Take Screenshot                   
            String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
            TakeScreenShot.saveScreenShot(AfterLogin, driver);
            return res;
          }
          finally
          {
                 return res;
          }
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static String[] concatAll(String[] first, String[]... rest)
	{
	  int totalLength = first.length;
	  for (String[] array : rest) 
	  {
	    totalLength += array.length;
	  }
	  
	  String[] result = Arrays.copyOf(first, totalLength);
	  int offset = first.length;
	  
	  for (String[] array : rest)
	  {
	    System.arraycopy(array, 0, result, offset, array.length);
	    offset += array.length;
	  }
	  
	  return result;
	}

	
	
	
	public static boolean AWW_CustomerSelect(String ch, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try{
			
			if(ch.equalsIgnoreCase("Amazon"))
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='app/images/Amazon.png']"))).isDisplayed();
				driver.findElement(By.xpath("//img[@src='app/images/Amazon.png']")).click();
				utilityFileWriteOP.writeToLog(TC_no, "Customer selection:: ", "PASS",ResultPath);
				res=true;
			}
			else if(ch.equalsIgnoreCase("P&H"))
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='app/images/P&H.png']"))).isDisplayed();
				driver.findElement(By.xpath("//img[@src='app/images/P&H.png']")).click();
				utilityFileWriteOP.writeToLog(TC_no, "Customer selection:: ", "PASS",ResultPath);
				res=true;
			}
			else if(ch.equalsIgnoreCase("mccolls"))
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='app/images/mccolls.png']"))).isDisplayed();
				driver.findElement(By.xpath("//img[@src='app/images/mccolls.png']")).click();
				utilityFileWriteOP.writeToLog(TC_no, "Customer selection:: ", "PASS",ResultPath);
				res=true;
			}
			else
			{
				res=false;			
				utilityFileWriteOP.writeToLog(TC_no, "Customer selection:: ", "FAIL",ResultPath);
			}
			
			return res;
			
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Customer selection:: ", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	public static boolean Invalid_Search_Criteria(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
			{
				Thread.sleep(5000);
				//driver.findElement(By.xpath("//p[contains(text(),'No data available')]")).isDisplayed();
				
				int size=driver.findElements(By.xpath("//p[contains(text(),'No data available')]")).size();
				
				if(size>0)
				{
					driver.findElement(By.xpath("//p[contains(text(),'No data available')]")).isDisplayed();
					System.out.println("No Records found for given criteria::::"+size);
			
					// Take Screenshot			   
					String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
					TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         
		      
		       
					utilityFileWriteOP.writeToLog(TC_no, "No Order Details Present  ", "Validated!!!",ResultPath);
					res=true;
					return res;
				}
				else
				{
					throw new Exception("No Warning MSG displayed on screen:::"+size);
				}
			}
			
			catch(Exception e)
			{
				res=false;	
				System.out.println("The exception is: "+e);
				utilityFileWriteOP.writeToLog(TC_no, "Order Details Not Present Msg ", "Not Found!!!",ResultPath);
				
				// Take Screenshot			   
		         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         return res;
			}
			
	}

	
	
	public static boolean Shipped_From_Validation(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
			{
			
				String shippedFrom=driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td[5]")).getText();
				System.out.println("The Shipped From Data is::::"+shippedFrom);
				// Take Screenshot			   
		         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         
		      
		       
				utilityFileWriteOP.writeToLog(TC_no, "The Shipped From Data is:::: "+shippedFrom,ResultPath );
				res=true;
				return res;
			}
			
			catch(Exception e)
			{
				res=false;	
				System.out.println("The exception is: "+e);
				utilityFileWriteOP.writeToLog(TC_no, "Shipped From Data ", "Not Found!!!",ResultPath);
				
				// Take Screenshot			   
		         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
		         return res;
			}
			
	}

	
	
	
	
	

public static boolean PrintSearch(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  
                 
               
                 
              // Store the current window handle
                     String winHandleBefore = driver.getWindowHandle();

                     // Perform the click operation that opens new window
                     driver.findElement(By.xpath("//div[@id='printNav']/div/ul/li[1]/a/img[@src='app/images/print.png']")).click();

                     // Switch to new window opened
                     for(String winHandle : driver.getWindowHandles()){
                         driver.switchTo().window(winHandle);
                     }

                     // Perform the actions on new window
                     
                     Thread.sleep(7000);
                     
                     
                     
                    
                     
                     
              // Take Screenshot                   
            String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
            TakeScreenShot.saveScreenShot(AfterLogin, driver);
            utilityFileWriteOP.writeToLog(TC_no, "Print ", "PASS",ResultPath);
                     // Close the new window, if that window no more required
                     driver.close();

                     // Switch back to original browser (first window)
                     driver.switchTo().window(winHandleBefore);

                     // Continue with original browser (first window)
                  
                     res=true;
                     return res;
           }  
           catch(Exception e)
           {
                  res=false;    
                  System.out.println("The exception is: "+e);
                  utilityFileWriteOP.writeToLog(TC_no, "Print ", "Failed",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
           
    }

	






public static boolean AWW_dropdownOp_Channel(String dd, String ch, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
{
       boolean res=false;
       try{
              wait.until(ExpectedConditions.elementToBeClickable(By.id(dd))).isDisplayed();
              driver.findElement(By.id(dd)).click();
              Thread.sleep(3000);
              Select dropdown = new Select(driver.findElement(By.id(dd)));
              dropdown.selectByValue(ch);
              utilityFileWriteOP.writeToLog(TC_no, "DropDown Option selected for "+dd+":: " , "PASS",ResultPath);
              
              String AfterClick=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
         TakeScreenShot.saveScreenShot(AfterClick, driver);
         
              res=true;
              return res;
       }
       catch(Exception e)
       {
              res=false;                 
              utilityFileWriteOP.writeToLog(TC_no, "DropDown Option ", "Not Found!!!",ResultPath);
              System.out.println(e);
              // Take Screenshot                   
         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
         TakeScreenShot.saveScreenShot(AfterLogin, driver);
         return res;
       }
       finally
       {
              return res;
       }
       
}






	
	
	
	
	
	
	
	
	
	
	
	
	/*public static boolean OrderDetailsValidation(WebDriver driver,WebDriverWait wait, String TC_no,String DriverSheetPath, String ResultPath)
	{
		boolean res=false;
		WebDriver driverDyanmo=null;
		WebDriverWait dynamowait =null;
		try{
			
			try
			{
				driver.findElement(By.xpath("//p[contains(text(),'No data available')]")).isDisplayed();
				System.out.println("No Records found for given criteria");
				utilityFileWriteOP.writeToLog(TC_no, "No Records found for given criteria  ", "FAIL",ResultPath);
				
			}
			
			catch(Exception e)
			{
				
				String[] ordDets=new String[15];
				
				for(int i=0;i<13;i++)
				{
					if (i==11)
						continue;
					ordDets[i]=driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td["+(i+1)+"]")).getText();
				}
				ordDets[6]="null";
			//	ordDets[6]=ordDets[6].substring(0, 10);
			//	ordDets[6]=ordDets[6].substring(6, 10)+"-"+ordDets[6].substring(3, 5)+"-"+ordDets[6].substring(0, 2);
				ordDets[7]=ordDets[7].substring(6, 10)+"-"+ordDets[7].substring(3, 5)+"-"+ordDets[7].substring(0, 2);
			
				int j=0;
				for(int i=0;i<13;i++)
				{
					if (i==11)
					{
						continue;
					
					}
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					if(ordDets[i].contentEquals("-"))
												
						excelCellValueWrite.writeValueToCell("null", 1, j, DriverSheetPath, "Order");
					
					else
						
						excelCellValueWrite.writeValueToCell(ordDets[i], 1, j, DriverSheetPath, "Order");
						
					j++;
					
					
					
					
					
					
				}
				// Take Screenshot			   
		         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin, driver);
				
			//Validate Dynamo DB values...	
				
				
				
				driverDyanmo=new ChromeDriver();
					dynamowait=new WebDriverWait(driverDyanmo, 50);
					
					driverDyanmo.manage().deleteAllCookies();
					driverDyanmo.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				 
				 
					driverDyanmo.get("https://manage.rackspace.com");
					driverDyanmo.manage().window().maximize();
					Workbook wrk1 =  Workbook.getWorkbook(new File(DriverSheetPath));
					  //Obtain the reference to the first sheet in the workbook
					    Sheet sheet1 = wrk1.getSheet("DynamoDB");
					    String DynamoName = sheet1.getCell(0,1).getContents().trim();
						String DynamoPass = sheet1.getCell(1,1).getContents().trim();
				 
				 boolean loginAWS=Amazon_RackSpace.aws_login(driverDyanmo, dynamowait, TC_no, DynamoName, DynamoPass, ResultPath);
					//String awsDynamoDB_sc="TestData/Results/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					//TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
					if(loginAWS==false)
					{
						throw new Exception("Dynamo DB navigation error");
						//return false;
					}
					
					boolean navToDynamoDB=Amazon_RackSpace.navigateToDynamoDB_Table(driverDyanmo, dynamowait, TC_no, ResultPath);
					//String awsDynamoDB_sc1="TestData/Results/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					//TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
					if(navToDynamoDB==false)
					{
						throw new Exception("Dynamo DB navigation error");
						//return false;
					}
					
					boolean validateInDynamoDB=Amazon_RackSpace.validateSingleOrderMultipleItemsDynamoDB("Order", driverDyanmo, dynamowait, TC_no, "wholesale.core.orderHeaders", "amazon", ordDets[0], false,DriverSheetPath, ResultPath);
							
					//String awsDynamoDB_sc2="TestData/Results/"+TC_no+"/"+getCurrentDate.getTimeStamp()+"_ScreenShot"+".png";	       
					//TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
					System.out.println("validateInDynamoDB "+validateInDynamoDB);
					if(validateInDynamoDB==false)
					{
						throw new Exception("Dynamo DB validation error");
						//return false;
					}
					
			}
			
			driverDyanmo.quit();
	       
			utilityFileWriteOP.writeToLog(TC_no, "Order Details Validated in Dynamo DB  ", "PASS",ResultPath);
			res=true;
			return res;
		}
		catch(Exception e)
		{
			res=false;	
			System.out.println("The exception is: "+e);
			utilityFileWriteOP.writeToLog(TC_no, "Order Details Validation ", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        
				
				driverDyanmo.quit();
	         
	         return res;
		}
		finally
		{
			return res;
		}
		
	}*/
	
	public static boolean SalesOrd_SS(String type, String data, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try{
			String typeID="";
			
			if(type.equalsIgnoreCase("Sales Order"))
				 typeID="orderIdSearch";
			if(type.equalsIgnoreCase("Product Code"))
				typeID="productcodeSearch";
			
			
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id(typeID))).isDisplayed();
			driver.findElement(By.xpath("//div[@id='"+typeID+"']/input")).sendKeys(data);
			Thread.sleep(3000);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
			
	         
	        
	         
	     	Robot robot = new Robot();
	     	
	     	robot.keyPress(KeyEvent.VK_ENTER);
	     	robot.keyRelease(KeyEvent.VK_ENTER);
	     	
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
			
	       
			utilityFileWriteOP.writeToLog(TC_no, "Enter Sales Order Search Option:"+type+"-"+data, "PASS",ResultPath);
			res=true;
			return res;
		}
		catch(Exception e)
		{
			res=false;	
			System.out.println("The exception is: "+e);
			utilityFileWriteOP.writeToLog(TC_no, "Input Option ", "Not Found!!!",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
		
	}
	public static boolean AWW_dropdownOp(String dd, String ch, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try{
			wait.until(ExpectedConditions.elementToBeClickable(By.id(dd))).isDisplayed();
			driver.findElement(By.id(dd)).click();
			Thread.sleep(3000);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			driver.findElement(By.xpath("//select[@id='"+dd+"']/option[contains(text(),'"+ch+"')]")).click();
			//dataVal[0]="Channel";
			//dataVal[1]=driver.findElement(By.xpath("//select[@id='"+dd+"']/option[contains(text(),'"+ch+"')]")).getAttribute("value",ResultPath);
			utilityFileWriteOP.writeToLog(TC_no, "DropDown Option selected for "+dd+":: " , "PASS",ResultPath);
			res=true;
			return res;
		}
		catch(Exception e)
		{
			res=false;			
			
			if(dd.equalsIgnoreCase("catalogueType"))
				utilityFileWriteOP.writeToLog(TC_no, "Catalog Type "+ch+" Not Found", "FAIL",ResultPath);
			
			
			utilityFileWriteOP.writeToLog(TC_no, "DropDown Option selected:: ", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
		
	}

	public static boolean AWW_Login(String un, String pw, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath) 
	{
		
		boolean res =false;
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username' and @placeholder='Email address']")));
			
			
			driver.findElement(By.xpath("//input[@id='username' and @placeholder='Email address']")).sendKeys(un);
			driver.findElement(By.xpath("//input[@id='password' and @placeholder='********']")).sendKeys(pw);
			
		//	utilityFileWriteOP.writeToLog(TC_no, "Login page ", "Found!!!",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			driver.findElement(By.xpath("//button[contains(text(),'LOGIN')]")).click();
			
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")));
			
			utilityFileWriteOP.writeToLog(TC_no, "Login with user credentials:: ", " PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Login with user credentials:: ", " FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}

	
	public static boolean AWW_Logout(String un, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath) 
	{
		
		boolean res =false;
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'"+un+"')]")));
			Thread.sleep(3000);
			driver.findElement(By.xpath("//button[contains(text(),'"+un+"')]")).click();
		
			
	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Logout')]")));
	         
	      //   utilityFileWriteOP.writeToLog(TC_no, "Logout Menu", "Found!!!",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	         
	         driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
	         
	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username' and @placeholder='Email address']")));
				
			
			utilityFileWriteOP.writeToLog(TC_no, "Logout from Application:: ", "PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin2, driver);
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "Logout from Application:: ", "FAIL",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	
	
	
	
	
	
	
	
	
	public static boolean AdvanceSearchLink(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.id("advSearchIcon"))).isDisplayed();
                  driver.findElement(By.id("advSearchIcon")).click();
                  Thread.sleep(3000);
                  
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;                 
                  utilityFileWriteOP.writeToLog(TC_no, "Advance Search ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
           
           
    }
	public static boolean AdvanceSearchButton(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control' and contains(text(),'ADVANCED SEARCH ')]"))).isDisplayed();
                  try{
                  driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control' and contains(text(),'ADVANCED SEARCH ')]")).click();
                  Thread.sleep(3000);
                  }
                  catch(WebDriverException e)
                  {
                	  
                	 
                	  driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control' and contains(text(),'ADVANCED SEARCH ')]")).click();
                      Thread.sleep(3000);
                  }
                  Thread.sleep(6000);
                  Robot robot = new Robot();
         			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
         			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "ADVANCE SEARCH ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
	public static int OrderCount(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           int res=0;
           try{
        	   Robot robot = new Robot();
   			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
   			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
   			
   			
               res=driver.findElements(By.xpath("//div[@id='exportable']/table/tbody/tr")).size();
               
               excelCellValueWrite.writeValueToCell_sheet(res+"" , 1, 1, 0, "TestData/DriverExcel.xls");
               System.out.println("Number of Orders: "+res);
               
               utilityFileWriteOP.writeToLog(TC_no, "Number of Orders:::"+res, ResultPath);
   			
   			// Take Screenshot			   
   	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
   	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
        	      
        	   return res;
           }
           catch(Exception e)
           {
                  res=0;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Order Count ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
	
	public static boolean Open_Order_SS(String ordNo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                 Thread.sleep(3000);
               //  driver.findElement(By.xpath("//td[contains(text(),'"+ordNo+"')]")).click();
                 
                 
                 WebElement element = driver.findElement(By.xpath("//td[contains(text(),'"+ordNo+"')]"));

    	         JavascriptExecutor executor = (JavascriptExecutor)driver;
    	         executor.executeScript("arguments[0].click()", element);
    	         
               res=true;
                     // Take Screenshot                   
                String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin, driver);
                    
                 return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Order Open Link ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    
	public static boolean Open_Order(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                 
                 driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr/td[1]")).click();
               res=true;
                     // Take Screenshot                   
                String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin, driver);
                    
                 return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Order Open Link ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
	
	
	
	
	
	public static boolean CatalogueManagementButton(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='menu-product-name pad-lft-right-15' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                  try{
                	  
                	  driver.findElement(By.xpath("//div[@class='menu-product-name pad-lft-right-15' and contains(text(),'Catalogue Management')]")).click();
                	  Thread.sleep(3000);
                  }
                  catch(WebDriverException e)
                  {
                	  driver.findElement(By.xpath("//div[@class='menu-product-name pad-lft-right-15' and contains(text(),'Catalogue Management')]")).click();
                	  Thread.sleep(3000);
                  }
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Management", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }

	public static boolean ManagementRequestButton(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
              
                	  
                	  driver.findElement(By.xpath("//h5[@class='border-top-grey-1 padd-top-5 curs-pointer font-light font-1-17em' and contains(text(),'Manage Requests')]")).click();
                	
                	  Thread.sleep(1000);
                
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                 
                		  res=true;
                	
                  
            
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Management Requests", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
             return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	
	

    public static boolean ProductRequest_Search(String Cat_Type1, String Cat_Type2, WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
            
           
           wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Manage New Product Requests')]"))).isDisplayed();
           
           try{
        	   
         
        	   
         	  driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[2]/div[2]/div[1]/div/select")).click();
         	  
        	  driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[2]/div[2]/div[1]/div/select/option[contains(text(),'"+Cat_Type1+"')]")).click();
         	  
         	  driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[2]/div[2]/div[2]/div[2]/select")).click();
         	  
         	  driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[2]/div[2]/div[2]/div[2]/select/option[contains(text(),'"+Cat_Type2+"')]")).click();
              
              driver.findElement(By.xpath("//button[@class='btn btn-default npr-popup-btn mar-bottom-1 padd-lr-adj' and contains(text(),'View')]")).click();
                  
                 
                  
                  Thread.sleep(1000);
                  
         // Take Screenshot                     
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                 
                  
                  try{
                	  
                	  
                	  boolean valid=driver.findElement(By.xpath("//h4[@class='dis-inlineblc' and contains(text(),'"+Cat_Type2+"')]")).isDisplayed();
                	  
                	  if (valid) {
    					
                		  res=true;
                		
                          //System.out.println(valid);
                	         // Take Screenshot                     
        	                  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
        	                  TakeScreenShot.saveScreenShot(AfterLogin1, driver);
        	              
                	  }
                  }
                  
                  catch(Exception e){
                	  
                	  utilityFileWriteOP.writeToLog(TC_no, ""+Cat_Type2+"", "not Found",ResultPath);
                	  
                      // Take Screenshot                     
                   String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                   TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                       

                	  
                  }

           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 return (res);
           }
           finally
           {
                  return (res);
           }
    }
    

    public static boolean ProductRequest_Search(String Cat_Type1, WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
            
           
           wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Manage New Product Requests')]"))).isDisplayed();
           
           try{
        	   
          	  
         	  driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[2]/div[2]/div[2]/div[2]/select")).click();
         	  
         	  driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[2]/div[2]/div[2]/div[2]/select/option[contains(text(),'"+Cat_Type1+"')]")).click();
              
              driver.findElement(By.xpath("//button[@class='btn btn-default npr-popup-btn mar-bottom-1 padd-lr-adj' and contains(text(),'View')]")).click();
                  
              Thread.sleep(1000);
                  
              try{
            	  
            	  
            	  boolean valid=driver.findElement(By.xpath("//h4[@class='dis-inlineblc' and contains(text(),'"+Cat_Type1+"')]")).isDisplayed();
            	  
            	  if (valid) {
					
            		  res=true;
            		
                      //System.out.println(valid);
            	         // Take Screenshot                     
    	                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
    	                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
    	              
            	  }
              }
              
              catch(Exception e){
            	  
            	  utilityFileWriteOP.writeToLog(TC_no, ""+Cat_Type1+"", "not Found",ResultPath);
            	  
                  // Take Screenshot                     
               String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
               TakeScreenShot.saveScreenShot(AfterLogin, driver);
                   

            	  
              }
                 
                  
                  Thread.sleep(1000);
        
           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 return (res);
           }
           finally
           {
                  return (res);
           }
    }
    

    public static boolean NewRequest_Search(String Req_Ch, String Req_Type, WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           boolean CatRes=false;
           
         
           
           wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'New Product Requests')]"))).isDisplayed();
           
           try{
        	   
         	 
         	  driver.findElement(By.xpath("//select[@name='requestChannel']")).click();
 	  
         	  
         	  driver.findElement(By.xpath("//select[@name='requestChannel']/option[contains(text(),'"+Req_Ch+"')]")).click();
         	   
         	  
         	  driver.findElement(By.xpath("//select[@name='requestType']")).click();
        	  
        	  
        	  driver.findElement(By.xpath("//select[@name='requestType']/option[contains(text(),'"+Req_Type+"')]")).click();
        	   
                  
        	  driver.findElement(By.xpath("//button[@class='pop-up-button' and @type='button']")).click();
        	  
        	  try{
        		  boolean v1=driver.findElement(By.xpath("//h5[contains(text(),'Product Lookup')]")).isDisplayed();
        		  boolean v2=driver.findElement(By.xpath("//input[@id='productcode']")).isDisplayed();
        		  boolean v3=driver.findElement(By.xpath("//textarea[@name='comment']")).isDisplayed();
        		  boolean v4=driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).isDisplayed();
        		  boolean v5=driver.findElement(By.xpath("//button[contains(text(),'OK')]")).isDisplayed();
        		  
        		  
        		  if (v1&&v2&&v3&&v4&&v5) {
        			  
        			  res=true;
        			  
        		  	}
        		  
        	  	}
        	  
        	  catch(Exception e)
        	  {
        		  utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Validation", "Failed",ResultPath);
        	  }
        	  
                  
                  
                  Thread.sleep(2000);
                  
         // Take Screenshot                     
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                       

           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return (res);
           }
           finally
           {
                  return (res);
           }
    }
    
    

	public static boolean CreateRequestButton(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                  try{
                	  
                	  driver.findElement(By.xpath("//h5[@class='border-top-grey-1 padd-top-5 curs-pointer font-light font-1-17em' and contains(text(),'Create Requests')]")).click();
                	  Thread.sleep(1000);
                  }
                  catch(WebDriverException e)
                  {
                	  driver.findElement(By.xpath("//h5[@class='border-top-grey-1 padd-top-5 curs-pointer font-light font-1-17em' and contains(text(),'Create Requests')]")).click();
                	  Thread.sleep(1000);
                  }
                  
                  
                  try{
                	  
                	  boolean valid=driver.findElement(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'New Product Requests')]")).isDisplayed();
                	  
                	  if (valid) {
						
                		  res=true;
                		  
                		  
                		  // Take Screenshot                   
                          String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                          TakeScreenShot.saveScreenShot(AfterLogin, driver);
                         
                	  	}
                  }
                  catch(Exception e){
                	  
                	  utilityFileWriteOP.writeToLog(TC_no, "Create Requests Page", "Not Found!!!",ResultPath);
                	  // Take Screenshot                   
                      String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                      TakeScreenShot.saveScreenShot(AfterLogin, driver);
                     
                  }
                  
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Create Requests", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	public static boolean ViewExportButton(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//h5[@class='border-top-grey-1 padd-top-5 curs-pointer font-light font-1-17em' and contains(text(),'View List Of Exports')]")).click();
                	  Thread.sleep(1000);
                 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Morrisons Full Range Export Page", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	



	public static boolean ProdLookUp(String min,WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           
           WebElement desc;
           
           try{
                  
        	   driver.findElement(By.xpath("//input[@name='productcode']")).sendKeys(""+min+"");
         	  
         	  driver.findElement(By.xpath("//i[@class='glyphicon glyphicon-search search-icon']")).click();
         	  
         	try{
         		
         	
         	   desc=driver.findElement(By.xpath("//td[contains(text(),'"+min+"')]"));
         	   
         	   boolean prod=desc.isDisplayed();
         	  
         	  if(prod)
         	  {
         		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                   TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                   driver.findElement(By.xpath("//button[contains(text(),'OK')]")).click();
         	  }
         	  
         	
         	
         	}
         	catch(Exception e)
         	{
         		  
         		
       		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                 TakeScreenShot.saveScreenShot(AfterLogin, driver);
                
                 utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Error Popup", "Found!!!",ResultPath);
                 
       		  driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).click();
       		  
         	}
         	
         	
         	 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Error Popup", "Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	

	public static boolean addProdLookUp(String min,WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           
           WebElement desc;
           
           try{
                  
        	   driver.findElement(By.xpath("//input[@name='productcode']")).sendKeys(""+min+"");
         	  
         	  driver.findElement(By.xpath("//i[@class='glyphicon glyphicon-search search-icon']")).click();
         	  
         	try{
         		
         	
         	   desc=driver.findElement(By.xpath("//td[contains(text(),'"+min+"')]"));
         	   
         	   boolean prod=desc.isDisplayed();
         	  
         	  if(prod)
         	  {
         		  Thread.sleep(1000);
         		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                   TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                   driver.findElement(By.xpath("//img[@class='img-responsive sqrIcon-npr cursor-pointer']")).click();
          		  
          		   String txt = desc.getText();
          		   
          		   //System.out.println(txt+ "  Legacy-MIN-PIN details added");
          		  
                   driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).click();
                   
                   try{
                	   
                	   boolean home=driver.findElement(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'New Product Requests')]")).isDisplayed();
                   
	                   if(home)
		                   {
		                	   res=true;
		                	   String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
		                       TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		                    
		                   }
	                   }
                   catch(Exception e)
                   {
                	   String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                       TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                    
                   }
         	  }
         	
         	
         	}
         	catch(Exception e)
         	{
         		  
         		
       		   
                 utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Error Popup", "Found!!!",ResultPath);
                 
       		  driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).click();
       		  
         	}
         	
         	
         	 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Error Popup", "Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	

	public static boolean addProdLookUp_OKButton(String min,WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           
           WebElement desc;
           
           try{
                  
        	   driver.findElement(By.xpath("//input[@name='productcode']")).sendKeys(""+min+"");
         	  
         	  driver.findElement(By.xpath("//i[@class='glyphicon glyphicon-search search-icon']")).click();
         	  
         	try{
         		
         	
         	   desc=driver.findElement(By.xpath("//td[contains(text(),'"+min+"')]"));
         	   
         	   boolean prod=desc.isDisplayed();
         	  
         	  if(prod)
         	  {
         		  Thread.sleep(1000);
         		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                   TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                   driver.findElement(By.xpath("//img[@class='img-responsive sqrIcon-npr cursor-pointer']")).click();
          		  
          		   String txt = desc.getText();
          		   
          		   //System.out.println(txt+ "  Legacy-MIN-PIN details added");
          		  
                   driver.findElement(By.xpath("//button[contains(text(),'OK')]")).click();
                   
                   try{
                	   
                   boolean valid=driver.findElement(By.xpath("//button[@class='btn submit-btn adv-search-btn pull-right' and contains(text(),'Submit')]")).isDisplayed();
                   
                   Thread.sleep(1000);
               	
            		  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                      TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                    
                   
                   if (valid) {
					
                	   
                	   res=true;
                   	}
                   }
                   catch(Exception e)
                   {
                	   utilityFileWriteOP.writeToLog(TC_no, "Submit Button ", "Disabled!!!",ResultPath);
                   }
                  
         	  }
         	  
         	
         	
         	}
         	catch(Exception e)
         	{
         		  
         		
       		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                 TakeScreenShot.saveScreenShot(AfterLogin, driver);
                
                 utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Error Popup", "Found!!!",ResultPath);
                 
       		  driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).click();
       		  
         	}
         	
         	
         	 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Error Popup", "Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	
	
	public static boolean ViewPrevExport(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Morrisons Full Range Export')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//button[@class='btn btn-default npr-popup-btn mar-bottom-1' and contains(text(),'Initiate New Full Range Export')]")).click();
                	  Thread.sleep(1000);
                 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                  driver.findElement(By.xpath("//button[@class='succ-fulrange-nav-btn' and contains(text(),'View Previous Exports')]")).click();
                  
                  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                 
                  
                  
                  try{
                	  
                      boolean home =driver.findElement(By.xpath("//h4[@class='pad-lft-right-15 dis-inlineblc' and contains(text(),'Morrisons Full Range Exports')]")).isDisplayed();
                      
                      if (home) {
                    	  
                    	  res=true;
						
                      }
                  }
                  catch(Exception e){
                	  
                  }
               
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Morrisons Full Range Export Page", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	
	public static boolean DataTeamButtaon(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'DATA TEAM')]")).click();
                	  Thread.sleep(1000);
                 
                	  
                	  try{
                    	  
                          
                          boolean valid=driver.findElement(By.xpath("//li[@class='active']/a[contains(text(),'Basic')]")).isDisplayed();
                          
                          if (valid) {
                        	
                        	  res=true;
                          	}
                          
                          }
                          
                          catch(Exception e){
                        	  
                        	  utilityFileWriteOP.writeToLog(TC_no, "Basic Tab", "Not Found!!!",ResultPath);
                          }
                          
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Data Team Page", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    } 
	
	
	public static boolean SupplyTeamButtaon(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'SUPPLY CHAIN')]")).click();
                	  Thread.sleep(1000);
                 
                	  
                	  try{
                    	  
                          
                          boolean valid=driver.findElement(By.xpath("//li[@class='active']/a[contains(text(),'Main')]")).isDisplayed();
                          
                          if (valid) {
                        	
                        	  res=true;
                          	}
                          
                          }
                          
                          catch(Exception e){
                        	  
                        	  utilityFileWriteOP.writeToLog(TC_no, "Main Tab", "Not Found!!!",ResultPath);
                          }
                          
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Supply Team Page", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    } 
	
	public static boolean CommercialTeamButtaon(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'COMMERCIAL')]")).click();
                	  Thread.sleep(1000);
                 
                  try{
                	  
                  
                  boolean valid=driver.findElement(By.xpath("//li[@class='active']/a[contains(text(),'Summary')]")).isDisplayed();
                  
                  if (valid) {
                	
                	  res=true;
                  	}
                  
                  }
                  
                  catch(Exception e){
                	  
                	  utilityFileWriteOP.writeToLog(TC_no, "Summary Tab", "Not Found!!!",ResultPath);
                  }
                  
               // Take Screenshot
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                  
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Commercial Team Page", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    } 
	
	public static boolean datateam_Tab(String tab,WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Data Team Review')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//a[@href='#"+tab+"']")).click();
                	  Thread.sleep(1000);
                	  
                	  
                	  try{
                    	  
                          
                          boolean valid=driver.findElement(By.xpath("//li[@class='active']/a[contains(text(),'"+tab+"')]")).isDisplayed();
                          
                          if (valid) {
                        	
                        	  res=true;
                          	}
                          
                          }
                          
                          catch(Exception e){
                        	  
                        	  utilityFileWriteOP.writeToLog(TC_no, ""+tab+" Tab", "Not Displayed!!!",ResultPath);
                          }
                 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, ""+tab+" Tab", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	
	public static boolean commercialteam_Tab(String tab,WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Commercial Team Review')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//a[contains(text(),'"+tab+"')]")).click();
                	  Thread.sleep(1000);
                 
                	  
                	  
                	  try{
                    	  
                          
                          boolean valid=driver.findElement(By.xpath("//li[@class='active']/a[contains(text(),'"+tab+"')]")).isDisplayed();
                          
                          if (valid) {
                        	
                        	  res=true;
                          	}
                          
                          }
                          
                          catch(Exception e){
                        	  
                        	  utilityFileWriteOP.writeToLog(TC_no, ""+tab+" Tab", "Not Displayed!!!",ResultPath);
                          }
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, ""+tab+" Tab", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  


	public static boolean supplyteam_Tab(String tab,WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Supply Chain Team Review')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//a[@href='#"+tab+"']")).click();
                	  Thread.sleep(1000);
                	  
                	  
                	  try{
                    	  
                          
                          boolean valid=driver.findElement(By.xpath("//li[@class='active']/a[contains(text(),'"+tab+"')]")).isDisplayed();
                          
                          if (valid) {
                        	
                        	  res=true;
                          	}
                          
                          }
                          
                          catch(Exception e){
                        	  
                        	  utilityFileWriteOP.writeToLog(TC_no, ""+tab+" Tab", "Not Displayed!!!",ResultPath);
                          }
                 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, ""+tab+" Tab", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	public static boolean PendRvw_Count(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Catalogue Management')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'DATA TEAM')]")).click();
                	  Thread.sleep(1000);
                 
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                  
                  driver.findElement(By.xpath("//span[@class='bradcrumb-text' and contains(text(),'Catalogue Management')]")).click();
                  
            	  driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'COMMERCIAL')]")).click();
            	  Thread.sleep(1000);
             
              // Take Screenshot                   
              String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
              TakeScreenShot.saveScreenShot(AfterLogin1, driver);
              
              
              driver.findElement(By.xpath("//span[@class='bradcrumb-text' and contains(text(),'Catalogue Management')]")).click();
              
        	  driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'SUPPLY CHAIN')]")).click();
        	  Thread.sleep(1000);
         
          // Take Screenshot                   
          String AfterLogin11=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
          TakeScreenShot.saveScreenShot(AfterLogin11, driver);
          
                  
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, " Different types of Pending Review Page ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	
	
	public static boolean DataTeam_ListView(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           try{
                  
                  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'Data Team Review')]"))).isDisplayed();
                
                	  
                	  driver.findElement(By.xpath("//img[@src='app/images/SquareIconOpen.svg']")).click();
                	  Thread.sleep(2000);
                 
                	  
                	  try{
                    	  
                          
                          boolean valid=driver.findElement(By.xpath("//h5[contains(text(),'Product Review')]")).isDisplayed();
                          
                          if (valid) {
                        	
                        	  
                        	  
                        	  // Take Screenshot                   
                              String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                              TakeScreenShot.saveScreenShot(AfterLogin, driver);
                                                      	  
                        	  driver.findElement(By.xpath("//button[contains(text(),'Close')]")).click();
                        	  
                        	  res=true;
                        	  
                          	}
                          
                          }
                          
                          catch(Exception e){
                        	  
                        	  utilityFileWriteOP.writeToLog(TC_no, "Product Review Page", "Not Found!!!",ResultPath);
                          }
                          
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Product Review Page", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    } 
	

	public static boolean ProdLookUpClose(WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
    {
           boolean res=false;
           
           WebElement desc;
           
           try{
        	   
        	   
        	   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[@class='pad-lft-right-30' and contains(text(),'Product Lookup')]"))).isDisplayed();
                  
               // Take Screenshot                   
               String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
               TakeScreenShot.saveScreenShot(AfterLogin, driver);
        	  
         	  driver.findElement(By.xpath("//button[@class='close-icon']")).click();
         	  
         
         	
                 res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Popup", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
                  return res;
           }
           finally
           {
                  return res;
           }
           
    }  
	
	
	  public static boolean NewRequest_SearchbyChannel(String Req_Ch, String Req_Type, WebDriver driver,WebDriverWait wait, String TC_no,String ResultPath)
	    {
	           boolean res=false;
	           boolean CatRes=false;
	           
	         
	           
	           wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text bold' and contains(text(),'New Product Requests')]"))).isDisplayed();
	           
	           try{
	        	   
	         	 
	         	  driver.findElement(By.xpath("//select[@name='requestChannel']")).click();
	 	  
	         	  
	         	  driver.findElement(By.xpath("//select[@name='requestChannel']/option[contains(text(),'"+Req_Ch+"')]")).click();
	         	   
	         	  
	         	  
	                  
	        	  driver.findElement(By.xpath("//button[@class='pop-up-button' and @type='button']")).click();
	        	  
	        	  try{
	        		  boolean v1=driver.findElement(By.xpath("//h5[contains(text(),'Product Lookup')]")).isDisplayed();
	        		  boolean v2=driver.findElement(By.xpath("//input[@id='productcode']")).isDisplayed();
	        		  boolean v3=driver.findElement(By.xpath("//textarea[@name='comment']")).isDisplayed();
	        		  boolean v4=driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).isDisplayed();
	        		  boolean v5=driver.findElement(By.xpath("//button[contains(text(),'OK')]")).isDisplayed();
	        		  
	        		  
	        		  if (v1&&v2&&v3&&v4&&v5) {
	        			  
	        			  res=true;
	        			// Take Screenshot                     
		                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
		                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
		          
	        			  
	        		  	}
	        		  
	        	  	}
	        	  
	        	  catch(Exception e)
	        	  {
	        		  utilityFileWriteOP.writeToLog(TC_no, "Product LookUp Validation", "Failed",ResultPath);
	        		  
	        		  // Take Screenshot                     
	                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
	                       

	        	  }
	        	  
	                  
	                  
	                  Thread.sleep(2000);
	                  
	        
	           
	                  return (res);
	           }
	           catch(Exception e)
	           {
	                  res=false;                 
	                  e.printStackTrace();
	                  //System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
	                  
	                  // Take Screenshot                   
	                  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	                  TakeScreenShot.saveScreenShot(AfterLogin, driver);
	                  return (res);
	           }
	           finally
	           {
	                  return (res);
	           }
	    }
	    
	    

	  public static boolean AdvanceSearch_EnterKey(String ch,String status,WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {
	           boolean res=false;
	           try{
	                  
	        	   
	        	   	  
					boolean  advSrchL= Amazon_Utilities.AdvanceSearchLink(driver, wait, TC_no, ResultPath);
					boolean  Channel=Amazon_Utilities.AWW_dropdownOp("channel", ch, driver, wait, TC_no, ResultPath);
                   
					boolean Status=Amazon_Utilities.AWW_dropdownOp("status", status, driver, wait, TC_no, ResultPath);
					  
					Robot r = new Robot();
					r.keyPress(KeyEvent.VK_ENTER);
					r.keyRelease(KeyEvent.VK_ENTER);
					
		            Thread.sleep(10000);
		            
		            Robot robot = new Robot();
         			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
         			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
                  // Take Screenshot                   
		             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
		             TakeScreenShot.saveScreenShot(AfterLogin, driver);
		             
		           // boolean ChValidate=Amazon_Utilities.OrderSearchValidation("channel",ch,driver, wait, TC_no, ResultPath);
		            boolean StValidate=Amazon_Utilities.OrderSearchValidation("status",status,driver, wait, TC_no, ResultPath);
	                  
	                  
	                  // Take Screenshot                   
	             String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	             
	            res=advSrchL&&Channel&&Status&&StValidate;
	            return res;
	            
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "ADVANCE SEARCH ", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(AfterLogin, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
		

	
}


