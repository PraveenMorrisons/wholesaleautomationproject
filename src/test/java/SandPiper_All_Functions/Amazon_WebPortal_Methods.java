package SandPiper_All_Functions;

import org.openqa.selenium.By;

import java.util.*;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
 










import java.util.List;
import java.util.Arrays;

import SandPiperUtilities.*;

import java.awt.Robot;
import java.awt.event.KeyEvent;


public class Amazon_WebPortal_Methods {
	
	

	public static boolean addProd_CatalogIncomplete(String[] prodDets, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		boolean success=false;
		
		try
		{
			boolean rangeCatalog= Amazon_Utilities.AWW_dropdownOp("catalogueType", prodDets[0] , driver, wait, TC_no, ResultPath);
			////System.out.println("Range/Catalogue Type Entered");
			
			boolean availability= Amazon_Utilities.AWW_dropdownOp("availabilityStatus", prodDets[3] , driver, wait, TC_no, ResultPath);
			////System.out.println("Availability Type Entered");
			
			boolean workflow= Amazon_Utilities.AWW_dropdownOp("workflowStatusName", prodDets[4] , driver, wait, TC_no, ResultPath);
			////System.out.println("WorkFlow Type Entered");
			
			driver.findElement(By.id("description")).sendKeys(prodDets[5]);
			////System.out.println("Description Entered");
			
			
			
			driver.findElement(By.id("identifier_MIN")).sendKeys(prodDets[1]);
			////System.out.println("MIN Entered");
			
			utilityFileWriteOP.writeToLog(TC_no, "New Product Mandatory Details Entered:::", " PASS",ResultPath);
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        
	         Thread.sleep(3000);
	         
	        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
	         
	         WebElement element =  driver.findElement(By.className("prdt-save-btn"));

	         JavascriptExecutor executor = (JavascriptExecutor)driver;
	         executor.executeScript("arguments[0].click()", element);
	         
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	         
	        
	       
	        // //System.out.println("Save button clicked");
	         
	         
	         
	         utilityFileWriteOP.writeToLog(TC_no, "Clicked Save Button:::", " PASS",ResultPath);
			
			// Take Screenshot			   
	        
	         try{
	        	 
	         
	         success= driver.findElement(By.className("prdct-succs")).isDisplayed();
	         
	         if (success) 
	         {
	        	 res=false;
	         }
	         else
	         {
	        	 res=true;
	        	 
	        	 utilityFileWriteOP.writeToLog(TC_no, "POPUP 'Please Select an Item from the List' Displayed", "PASS",ResultPath);
	         }
         }
	         
	         
	         catch(Exception e)
	         {
	        	 
	        	 res=true;
	        	 
	        	 utilityFileWriteOP.writeToLog(TC_no, "POPUP 'Please Select an Item from the List' Displayed", "PASS",ResultPath);
	        	 
	        	 
	         }
 
			
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "Adding new Product to Catalogue:::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	
	public static boolean ItemPage_ChangeCust(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		boolean cust=false;
		
		
		try
		{
		
			try{
				
			
			cust = driver.findElement(By.xpath("//a[@class='Product-title-anchor' and contains(text(),'Change Customer')]")).isDisplayed();
			
			if (cust) 
			{
			
				res=true;
				
				utilityFileWriteOP.writeToLog(TC_no, "Change Customer Option Available", "PASS",ResultPath);
				 
				
			}
			
			else
			{
				res=false;
				
				utilityFileWriteOP.writeToLog(TC_no, "Change Customer Option Not Available", "PASS",ResultPath);
				 
				
		         
			}
			}
			
			catch(Exception e)
			{

				res=false;
				
				utilityFileWriteOP.writeToLog(TC_no, "Change Customer Option Not Available", "PASS",ResultPath);
				 
				
			}
			
			String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	        TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
			return res;
			
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			

			utilityFileWriteOP.writeToLog(TC_no, "Change Customer Option Not Available", "PASS",ResultPath);
			 
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static boolean Active_StandaloneSearch_Validation(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath) 
	{
		boolean res=false;
		try
		{
			
			
			String class1;
			String class2;
			
			Thread.sleep(5000);
			
			
			driver.findElement(By.id("orderIdSearch")).isDisplayed();
			driver.findElement(By.id("productcodeSearch")).isDisplayed();
			
			//System.out.println("Standalone Search is active");
			
			Amazon_Utilities.AdvanceSearchLink(driver, wait, TC_no, ResultPath);
	         
			//System.out.println("Advance Search is open now ");
			
			
			Thread.sleep(5000);
			
			class1=driver.findElement(By.id("orderIdSearch")).getAttribute("class");
			class2=driver.findElement(By.id("productcodeSearch")).getAttribute("class");
					
			
			if(class1.contains("disable") && class2.contains("disable"))
			{
				//System.out.println("Standalone search is disabled");
				
				utilityFileWriteOP.writeToLog(TC_no, "Standalone Search Disabled", " PASS",ResultPath);
			
				// Take Screenshot			   
				String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
				TakeScreenShot.saveScreenShot(AfterLogin, driver);
			}
			
			else
			{
				throw new Exception("Standalone search is not disabled after advance search is opened");
			}
			
			
			Amazon_Utilities.AdvanceSearchLink(driver, wait, TC_no, ResultPath);
	         
			//System.out.println("Advance Search is closed now ");
			
			Thread.sleep(5000);
			
			
			
			driver.findElement(By.id("orderIdSearch")).isDisplayed();
			driver.findElement(By.id("productcodeSearch")).isDisplayed();
	         
			
				//System.out.println("Standalone search is enabled");
				utilityFileWriteOP.writeToLog(TC_no, "Standalone Search Enabled", " PASS",ResultPath);
			
				// Take Screenshot			   
				String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
				TakeScreenShot.saveScreenShot(AfterLogin1, driver);
			
			
			res=true;
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	
	
	public static boolean addProd_Catalog(String[] prodDets, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
			boolean rangeCatalog= Amazon_Utilities.AWW_dropdownOp("catalogueType", prodDets[0] , driver, wait, TC_no, ResultPath);
			//System.out.println("Range/Catalogue Type Entered");
			
			boolean availability= Amazon_Utilities.AWW_dropdownOp("availabilityStatus", prodDets[3] , driver, wait, TC_no, ResultPath);
			//System.out.println("Availability Type Entered");
			
			boolean workflow= Amazon_Utilities.AWW_dropdownOp("workflowStatusName", prodDets[4] , driver, wait, TC_no, ResultPath);
			//System.out.println("WorkFlow Type Entered");
			
			driver.findElement(By.id("description")).sendKeys(prodDets[5]);
			//System.out.println("Description Entered");
			
			
			
			driver.findElement(By.id("identifier_MIN")).sendKeys(prodDets[1]);
			//System.out.println("MIN Entered");
			
			utilityFileWriteOP.writeToLog(TC_no, "New Product Mandatory Details Entered:::", " PASS",ResultPath);
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        
	         Thread.sleep(3000);
	         
	         ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
	         
	         WebElement element = driver.findElement(By.xpath("//div[@class='input-group pos col-md-12 col-sm-12 col-lg-12']/div/button"));

	         JavascriptExecutor executor = (JavascriptExecutor)driver;
	         executor.executeScript("arguments[0].click()", element);
	         
	         //System.out.println("Save button clicked");
	         
	         utilityFileWriteOP.writeToLog(TC_no, "Clicked Save Button:::", " PASS",ResultPath);
			
	         Thread.sleep(3000);
			// Take Screenshot			   
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	         
	         try{
	        	 if(driver.findElement(By.xpath("//strong[contains(text(),'Some thing went wrong')]")).isDisplayed())
	        	 {
	        		 utilityFileWriteOP.writeToLog(TC_no, "Network error, Cannot Add Product", " FAIL",ResultPath);
	        		 res=false;
	        		 
	        	 }
	        	 return res;
	         }
	         catch(Exception e)
	         {
	        
	         
	         WebElement element1 = driver.findElement(By.className("prdct-succs"));
	         
	         JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	         executor1.executeScript("arguments[0].click()", element1);
	         
	         
	         //System.out.println("Confirmation Dialog Displayed");
	         utilityFileWriteOP.writeToLog(TC_no, "Success Confirmation Displayed:::", " PASS",ResultPath);
				
				// Take Screenshot			   
		         String AfterLogin2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		         
		         WebElement element2 = driver.findElement(By.xpath("//button[contains(text(),'Close') and @type='button']"));
		         JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		         executor2.executeScript("arguments[0].click()", element2);
		         
		         
	         //System.out.println("Dialog box closed");
	         
			res=true;
			return res;
	        }
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "Adding new Product to Catalogue:::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
		finally
		{
			return res;
		}
	}
	
	
	
	
	
	

	public static boolean addProd_Catalog_val(String prodMIN, String prodName, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{

		boolean res=false;
		try
		{
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[@class='menu-product-name' and contains(text(),'Catalogue Enquiry')]")).click();
			
			Thread.sleep(3000);
			
			
			driver.findElement(By.xpath("//div[contains(text(),'Add New Product')]")).click();
			
			for(String winHandle : driver.getWindowHandles())
			{
			    driver.switchTo().window(winHandle);
			}
			
			
			String imgSrc1=driver.findElement(By.xpath("//img[contains(@class,'img-responsive max')]")).getAttribute("src");
			////System.out.println("The src before dataload is:::"+imgSrc1);
			
			driver.findElement(By.id("productid")).sendKeys(prodMIN);
		//	//System.out.println("Prod Min Entered!!! "+prodMIN);
			
			driver.findElement(By.xpath("//span[@id='search-img']/i")).click();
			
			Thread.sleep(3000);
			
			String imgSrc2=driver.findElement(By.xpath("//img[contains(@class,'img-responsive max')]")).getAttribute("src");
			////System.out.println("The src after dataload is:::"+imgSrc2);
			
			if(!(imgSrc1.contentEquals(imgSrc2)))
			{
				
				String prodName2=driver.findElement(By.id("productName")).getAttribute("value");
				
				////System.out.println("The value is:::"+prodName2);
				
				if(prodName2.contains(prodName))
				{
					
					////System.out.println("Product Name is loaded");
				
					// Take Screenshot			   
					String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
					TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	         
	         	res=true;
				return res;
				
				}
				
				else
				{
					////System.out.println("Product Name Wrong:::FAIL",ResultPath);
					res=false;
					return res;
				}
				
			}
			else
			{
				////System.out.println("Product Image Wrong:::FAIL",ResultPath);
				res=false;
				return res;
			}
			
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	

    public static boolean Catalogue_Tab( WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           
           try{
                  
                  driver.findElement(By.xpath("//div[@class='menu-product-name' and contains(text(),'Catalogue Enquiry')]")).click();

                  
                  //Amazon_Utilities.AWW_dropdownOp("catalogueType", Cat_Type, driver, wait, TC_no, ResultPath);
                  Thread.sleep(1000);
                 
                  
                 
               // Take Screenshot                     
                  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Tab Entered", "PASS",ResultPath);   
                  
                  res=true;
                  Thread.sleep(2000);
         

           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return (res);
           }
           finally
           {
                  return (res);
           }
    }
    
    public static boolean Validate_Catalogue_Product(String CatType, String ProductNo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
   	{
   		boolean res=false;
   		try{
   			
   			
   			// Store the current window handle
   			String winHandleBefore = driver.getWindowHandle();

   			// Perform the click operation that opens new window
   			driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr/td[1]")).click();
   			
   			// Switch to new window opened
   			for(String winHandle : driver.getWindowHandles()){
   			    driver.switchTo().window(winHandle);
   			}

   			// Perform the actions on new window
   			Thread.sleep(3000);
   			if(driver.findElement(By.xpath("//div[@id='productDetailHeader']/div[2]/div/div[1]/div")).getText().equals(ProductNo))
   			{
   				if (driver.findElement(By.xpath("//div[@id='productDetailHeader']/div[2]/div/div[2]/div")).getText().equals(CatType))
   				{
   					res=true;
   					// Take Screenshot			   
   	    	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
   	    	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
   	    	         utilityFileWriteOP.writeToLog(TC_no, "Item "+ProductNo+" Catalogue Type "+CatType, "PASS",ResultPath);
   				}
   			}
   			
   			Thread.sleep(3000);
   			
              	
              		 
              	 
               
   			
               
   			// Close the new window, if that window no more required
   			driver.close();

   			// Switch back to original browser (first window)
   			driver.switchTo().window(winHandleBefore);

   			// Continue with original browser (first window)
   			return res;
   		}
   		catch(Exception e)
   		{
   			res=false;			
   			e.printStackTrace();
   			//System.out.println(e);
   			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
   			
   			// Take Screenshot			   
   	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
   	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
   	         return res;
   		}
   		finally
   		{
   			return res;
   		}
   	}

	
	
	
	
	
	
	public static boolean product_Catalog(String rangeCatalog, String prodDesc, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
			driver.findElement(By.xpath("//div[@class='menu-product-name' and contains(text(),'Catalogue Enquiry')]")).click();
			
			Thread.sleep(3000);
			Amazon_Utilities.AWW_dropdownOp("catalogueType", rangeCatalog, driver, wait, TC_no, ResultPath);
			Thread.sleep(3000);
			driver.findElement(By.id("catalogueCode")).sendKeys(prodDesc);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        
			driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'SEARCH ')]")).click();
			
			int rowCount= driver.findElements(By.xpath("//div[@id='exportable']/table/tbody/tr")).size();
			
			//System.out.println("The no. of Search Results:::"+rowCount);
			
			excelCellValueWrite.writeValueToCell_sheet(rowCount+"" , 2, 1, 0, "TestData/DriverExcel.xls");
			
			
			// Take Screenshot			   
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
	         
	         
	         String[] prodDets= new String [10];
	         
	         for(int i=0;i<7;i++)
	         {
	        	 prodDets[i]=driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td["+(i+1)+"]")).getText();
	        	 
	        	 if(prodDets[i].contentEquals("-"))
						
					excelCellValueWrite.writeValueToCell_sheet("null", 2, 1, (i+1), "TestData/DriverExcel.xls");
					
	        	 else
						
					excelCellValueWrite.writeValueToCell_sheet(prodDets[i], 2, 1, (i+1), "TestData/DriverExcel.xls");
	         }
	         
	        
			driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td")).click();
			
			for(String winHandle : driver.getWindowHandles())
			{
			    driver.switchTo().window(winHandle);
			}
			
			
			// Take Screenshot			   
	         String AfterLogin2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin2, driver);
	         
			driver.findElement(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")).click();
			
			
			utilityFileWriteOP.writeToLog(TC_no, "Product Page:: Opened", ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin3=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin3, driver);
	         
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	public static boolean InvalidSearch_ShippedTo(String ShipTo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
		
			driver.findElement(By.id("shipto")).sendKeys(ShipTo);
		
			
			utilityFileWriteOP.writeToLog(TC_no, "Shipped To Invalid Data::"," Entered", ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
	         
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e.toString(),ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	public static boolean HomeLink_Validation(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
		
			driver.findElement(By.xpath("//span[@class='bradcrumb-text underline' and contains(text(),'Home')]")).click();
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")));
			
			utilityFileWriteOP.writeToLog(TC_no, "HomeLink Redirecting to Homescreen:::", " PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
	         
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "HomeLink not Redirecting to Homescreen:::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	
	public static boolean HomeIMG_Validation(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
		
			driver.findElement(By.xpath("//img[@src='app/images/morrisons_logo.svg']")).click();
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")));
			
			utilityFileWriteOP.writeToLog(TC_no, "HomeIMG Redirecting to Homescreen:::", " PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
	         
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "HomeIMG not Redirecting to Homescreen:::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	public static boolean SearchOrder_Invoice(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try{
			
		
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
			
			
		
			
			
				wait.until(ExpectedConditions.elementToBeClickable(By.id("invoicedateshow"))).isDisplayed();
				driver.findElement(By.id("invoicedateshow")).click();
				
				String date="21-04-2016";
				
				String[] dmy= date.split("-");
				
				////System.out.println("The Date is : "+dmy[0]+" "+dmy[1]+" "+dmy[2]);
				
				
				String day=dmy[0];
				String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};
				
				int monthIndex= Integer.parseInt(dmy[1]);
				
				String month=monthList[monthIndex-1];
				String year=dmy[2];
				int y= Integer.parseInt(year);
				////System.out.println("The Date is : "+day+" "+month+" "+year);
				Thread.sleep(2000);
				
				
				
				
				
				
				
				
				
				
                driver.findElement(By.xpath("(.//*[contains(@id,'datepicker--') and contains(@id,'-title')])[6]")).click();
				
				
                int yr = Calendar.getInstance().get(Calendar.YEAR);
                Thread.sleep(5000);
                
                
                for(int i=0;i<(yr-y);i++)
                		
                  driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
                		
                Thread.sleep(5000);

                driver.findElement(By.xpath("//button[@class='btn btn-default']/span[text()='"+month+"']")).click();
                
                Thread.sleep(5000);
				
                driver.findElement(By.xpath("(//button[@class='btn btn-sm btn-default']/span[text()='"+day+"'])[2]")).click();
				
                Thread.sleep(5000);
                
                driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control' and contains(text(),'ADVANCED SEARCH ')]")).click();
                
				res=true;
			
			
			return res;
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	
	
	
	
	public static boolean SearchOrder_OrderDate(String date, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try{
			
			/*wait.until(ExpectedConditions.elementToBeClickable(By.id("advSearchIcon"))).isDisplayed();
			driver.findElement(By.id("advSearchIcon")).click();
			
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);*/
			
			
			
			
			
				wait.until(ExpectedConditions.elementToBeClickable(By.id("orderdateshow"))).isDisplayed();
				driver.findElement(By.id("orderdateshow")).click();
				
				//String date="21-04-2016";
				
				String[] dmy= date.split("/");
				
				//System.out.println("The Date is : "+dmy[0]+" "+dmy[1]+" "+dmy[2]);
				
				
				String day=dmy[0];
				String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};
				
				int monthIndex= Integer.parseInt(dmy[1]);
				
				String month=monthList[monthIndex-1];
				String year=dmy[2];
				int y= Integer.parseInt(year);
				//System.out.println("The Date is : "+day+" "+month+" "+year);
				Thread.sleep(5000);
				
								
				
                driver.findElement(By.xpath("(.//*[contains(@id,'datepicker--') and contains(@id,'-title')])[5]")).click();
				
				
                int yr = Calendar.getInstance().get(Calendar.YEAR);
                Thread.sleep(5000);
                
                  
                for(int i=0;i<(yr-y);i++)
                	driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
              
               
                Thread.sleep(5000);

                driver.findElement(By.xpath("//button[@class='btn btn-default']/span[text()='"+month+"']")).click();
                System.out.println(day);
                Thread.sleep(5000);
				try
				{
					Thread.sleep(5000);
					 System.out.println("TRY1");
					 
					 
					 
					 WebElement element5=driver.findElement(By.xpath("(.//*[@class='btn btn-sm btn-default']/span[contains(text(),'"+day+"')])[2]"));
                     
                     JavascriptExecutor executor5 = (JavascriptExecutor)driver;
                     executor5.executeScript("arguments[0].click()", element5);
                 

					 
					
					 
					 
                
                System.out.println("Click 1");
				}
				catch(WebDriverException e)
				{
					e.printStackTrace();
					System.out.println("TRY1");
					Thread.sleep(5000);
					
					
					WebElement element5=driver.findElement(By.xpath("(.//*[@class='btn btn-sm btn-default']/span[contains(text(),'"+day+"')])[2]"));
                    
                    JavascriptExecutor executor5 = (JavascriptExecutor)driver;
               executor5.executeScript("arguments[0].click()", element5);
                

					
					
					
					System.out.println("Click 2");
					
				}
                
                Thread.sleep(5000);
                
                
             // Take Screenshot			   
   	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
   	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
   	         
               
                
               
                
				res=true;
                
				return res;
		}
		catch(Exception e)
		{
			res=false;			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         return res;
		}
		finally
		{
			return res;
		}
	}
	public static boolean Validate_Product(String ordNo, String ProductNo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  
                  
                  // Store the current window handle
                  String winHandleBefore = driver.getWindowHandle();

                  // Perform the click operation that opens new window
                  boolean ordClick=Amazon_Utilities.Open_Order_SS(ordNo, driver, wait, TC_no, ResultPath);

                  // Switch to new window opened
                  for(String winHandle : driver.getWindowHandles()){
                      driver.switchTo().window(winHandle);
                  }

                  // Perform the actions on new window
                  
                  Thread.sleep(3000);
                  int size= driver.findElements(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/tbody/tr")).size();
         //System.out.println("Item List Size "+size);
         for (int i=1;i<=size;i++)
         {
            String val=driver.findElement(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/tbody/tr["+i+"]/td[2]")).getText();
           //System.out.println("Search "+val+" "+ProductNo); 
            if (val.equals(ProductNo))
            {
                   res=true;
                   //System.out.println("Match found "+val+" "+ProductNo); 
                   break;
            }
         }
              Thread.sleep(3000);   
         // Take Screenshot                     
             String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  // Close the new window, if that window no more required
                  driver.close();

                  // Switch back to original browser (first window)
                  driver.switchTo().window(winHandleBefore);

                  // Continue with original browser (first window)
                  return res;
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
	
	
	
	
	
	
	
	public static boolean editProd_Catalog(String[] prodDets, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[contains(text(),'Catalogue Enquiry')]")).click();
			Thread.sleep(3000);
			Amazon_Utilities.AWW_dropdownOp("catalogueType", prodDets[0], driver, wait, TC_no, ResultPath);
			Thread.sleep(3000);
			driver.findElement(By.id("catalogueCode")).sendKeys(prodDets[1]);
			Thread.sleep(3000);
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			
			//utilityFileWriteOP.writeToLog(TC_no, "Catalogue type Entered:::", " PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
			
			Thread.sleep(3000);
			
			try
			{
			//System.out.println("Catalogue Results Displayed");
			WebElement element5=driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td[1]"));
			JavascriptExecutor executor5 = (JavascriptExecutor)driver;
	        executor5.executeScript("arguments[0].click()", element5);
	         
			}
			catch(Exception e)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Product not found in LIST", " FAIL",ResultPath);
				throw new Exception("Product not found in LIST");
			}
			
			//System.out.println("Product Page Opened");
			 for(String winHandle : driver.getWindowHandles())
			 {
                 driver.switchTo().window(winHandle);
             }
			 
			 Thread.sleep(5000);
			
			
				  driver.findElement(By.xpath("//span[contains(text(),'Edit')]")).click();
				
			
			 
			System.out.println("Edit button clicked");
			

			/*try
			{
			//System.out.println("Catalogue Results Displayed");
			WebElement element7=driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]"));
			JavascriptExecutor executor7 = (JavascriptExecutor)driver;
	        executor7.executeScript("arguments[0].click()", element7);
	        
	        System.out.println("Edit Button Click");
	         
			}
			catch(Exception e)
			{
				System.out.println("Edit Button not Click");
				
				utilityFileWriteOP.writeToLog(TC_no, "Edit Button Click", " FAIL",ResultPath);
				
			}*/
			
			
			utilityFileWriteOP.writeToLog(TC_no, "Click on Edit button ::", " PASS",ResultPath);
			
			Thread.sleep(3000);
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         	Thread.sleep(3000);
	         
	         	driver.findElement(By.xpath("//div[@contains(text(),'CS')]")).click();
	         	
	         	driver.findElement(By.xpath("//input[@id='pricing_CS']")).clear();
	         	
	         	Thread.sleep(2000);
	         	
	         	driver.findElement(By.xpath("//input[@id='pricing_CS']")).sendKeys(prodDets[2]);
				
				/*driver.findElement(By.id("identifier_MIN")).clear();
				
				driver.findElement(By.id("identifier_MIN")).sendKeys(prodDets[1]);
				//System.out.println("MIN Entered");
				
				utilityFileWriteOP.writeToLog(TC_no, "Enter New Product Mandatory Details::", " PASS",ResultPath);
				// Take Screenshot			   
		         String AfterLogin2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		        */
		         Thread.sleep(3000);
		         
		         ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
		         
		         WebElement element = driver.findElement(By.xpath("//button[@class='prdt-save-btn']"));

		         JavascriptExecutor executor = (JavascriptExecutor)driver;
		         executor.executeScript("arguments[0].click()", element);
		         
		         //System.out.println("Save button clicked");
		         
		         utilityFileWriteOP.writeToLog(TC_no, "Click on Save Button::", " PASS",ResultPath);
				
				// Take Screenshot			   
		         String AfterLogin3=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		         
		         
		         
		         
		         WebElement element1 = driver.findElement(By.xpath("//a[@class='back-cat' and contains(text(),'Back to Catalogue')]"));
		         //WebElement element1 = driver.findElement(By.xpath("//h4[@class='prdct-succs text-center']"));
		         
		         JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		         executor1.executeScript("arguments[0].click()", element1);
		         
		         Thread.sleep(3000);
		         //System.out.println("Confirmation Dialog Displayed");
		         utilityFileWriteOP.writeToLog(TC_no, "Validate Success Confirmation::", " PASS",ResultPath);
					
					// Take Screenshot			   
			         String AfterLogin4=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
			         TakeScreenShot.saveScreenShot(AfterLogin4, driver);
			         
			         
			         
			       /*  WebElement element2 = driver.findElement(By.xpath("//button[contains(text(),'Close') and @type='button']"));
			         JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			         executor2.executeScript("arguments[0].click()", element2);
			         
			         
		         //System.out.println("Dialog box closed"); */
		         
			         try{
			        	 if(driver.findElement(By.xpath("//strong[contains(text(),'Some thing went wrong')]")).isDisplayed())
			        	 {
			        		 utilityFileWriteOP.writeToLog(TC_no, "Network error, Cannot Add Product", " FAIL",ResultPath);
			        		 res=false;
			        		 
			        	 }
			        	 return res;
			         }
			         catch(Exception e)
			         {    
			         
			         
		         
	         
		         WebElement element3 = driver.findElement(By.className("view-prdctbtn"));
		         
		         JavascriptExecutor executor3 = (JavascriptExecutor)driver;
		         executor3.executeScript("arguments[0].click()", element3);
		         
		         //System.out.println("View Product clicked");
		         
		         utilityFileWriteOP.writeToLog(TC_no, "Click on View Product::", " PASS",ResultPath);
					
				
		         
			res=true;
			return res;
			         }
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "Edit Product Details::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
	
	
	public static boolean editProd_Validation(String[] prodDets, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
			Thread.sleep(5000);
			String[] editDets=new String[6];
			
			
			editDets[0]= driver.findElement(By.xpath("//div[@id='productDetailHeader']/div[2]/div/div[2]/div")).getText();
			editDets[1]= driver.findElement(By.xpath("//div[@id='productDetailHeader']/div[2]/div/div/div")).getText();
			editDets[2]= driver.findElement(By.xpath("//h2[@class='orders-heading data-row bottom-line font-size-1-7em text-bold']")).getText();
			
			
			editDets[3]= driver.findElement(By.xpath("//div[@id='productDetailHeader']/div[3]/div/div/div")).getText();
			editDets[4]= driver.findElement(By.xpath("//div[@class='orders-heading data-row avail-text data-row']")).getText();
			editDets[5]= driver.findElement(By.xpath("//div[@id='productDetailHeader']/div[4]/div/div/div")).getText();
			
			
			
			/*for(int i=0;i<6;i++)
				//System.out.println("Prod Val::: "+editDets[i]);
			
			for(int i=0;i<6;i++)
				//System.out.println("Item Val::: "+prodDets[i]);
			*/
			
			
			if(Arrays.equals(prodDets, editDets))
			{
				//System.out.println("Product Details Match");
			}
			else
			{
				throw new Exception("Product Details Do Not Match:: FAIL");
			}
			
			
			utilityFileWriteOP.writeToLog(TC_no, "Validate Edited Products Details::", " PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
	         
	         
			res=true;
			return res;
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "Validate Edited Products Details::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
		
	}
	
	
	

    public static boolean Verify_Expand(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           //System.out.println("::Start Expand::");
           try{
                  String arr[]= {"Line","Identifier","Base","Alternate","Description","UOM","Ordered","Confirmed","Picked","Shipped","Currency","Tax","Net","Total","Current","Stock","Value"};              
                  // Store the current window handle
                  String winHandleBefore = driver.getWindowHandle();

                  // Perform the click operation that opens new window
                  boolean ordClick=Amazon_Utilities.Open_Order(driver, wait, TC_no, ResultPath);

                  // Switch to new window opened
                  for(String winHandle : driver.getWindowHandles()){
                      driver.switchTo().window(winHandle);
                  }

                  // Perform the actions on new window
                  Thread.sleep(3000);
                  for(int i=1;i<=arr.length;i++)
                  {
                        
                        if (i==1)
                               driver.findElement(By.xpath("//*[@id='orderItemTable_product']")).click();
                        else if(i==6)
                               driver.findElement(By.xpath("//*[@id='orderItemTable_quantity']")).click();
                        else if(i==11)
                        driver.findElement(By.xpath("//*[@id='orderItemTable_price']")).click();
                        else if(i==15)
                               driver.findElement(By.xpath("//*[@id='orderItemTable_status']")).click();
                        
                         if(driver.findElement(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/thead/tr[2]/th["+i+"]")).isDisplayed()==true && driver.findElement(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/thead/tr[2]/th["+i+"]")).getText().equals(arr[i-1])==true)
                        {
                               res=true;
                               //System.out.println(arr[i-1]+" Displayed");
                        
                        }
                        else
                        {
                               res=false;
                               break;
                        }
                  }
                  

                  
                  
                  
                  
         // Take Screenshot                     
             String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin1, driver);
           //System.out.println("::Finish Expand::");
                  return res;
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    
    }
    public static boolean Verify_Collapse(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try{
                  //System.out.println("::Start Collapse::");
                  String arr[]= {"Line","Identifier","Base","Alternate","Description","UOM","Ordered","Confirmed","Picked","Shipped","Currency","Tax","Net","Total","Current","Stock","Value"};              
                  

                  // Perform the actions on new window
                  Thread.sleep(3000);
                  for(int i=1;i<=arr.length;i++)
                  {
                        
                        if (i==1)
                               driver.findElement(By.xpath("//*[@id='orderItemTable_product']")).click();
                        else if(i==6)
                               driver.findElement(By.xpath("//*[@id='orderItemTable_quantity']")).click();
                        else if(i==11)
                        driver.findElement(By.xpath("//*[@id='orderItemTable_price']")).click();
                        else if(i==15)
                               driver.findElement(By.xpath("//*[@id='orderItemTable_status']")).click();
                        if (i==3||i==4||i==8||i==9||i==10||i==12||i==14||i==16||i==17)
                        {
                               if(driver.findElement(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/thead/tr[2]/th["+i+"]")).isDisplayed()==false)
                               {
                                      res=true;
                                      //System.out.println(arr[i-1]+" Not Displayed");
                               
                               }
                               else
                               {
                                      res=false;
                                      break;
                               }
                        }
                        else
                        {
                               if(driver.findElement(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/thead/tr[2]/th["+i+"]")).isDisplayed()==true && driver.findElement(By.xpath("//div[@id='export']/div[2]/table[@id='itemDetailDrillDown']/thead/tr[2]/th["+i+"]")).getText().equals(arr[i-1])==true)
                               {
                                      res=true;
                                      //System.out.println(arr[i-1]+" Displayed");
                               
                               }
                               else
                               {
                                      res=false;
                                      break;
                               }
                        }
                  }
                  

                  
                  
                  
                  
         // Take Screenshot                     
             String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  

           //System.out.println("::Finish Collapse::");
                  return res;
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    public static boolean Catalogue_Search(String Cat_Type, String ProdNo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           boolean CatRes=false;
           try{
                  
                  driver.findElement(By.xpath("//div[@class='menu-product-name' and contains(text(),'Catalogue Enquiry')]")).click();

                  
                  //Amazon_Utilities.AWW_dropdownOp("catalogueType", Cat_Type, driver, wait, TC_no, ResultPath);
                  Thread.sleep(3000);
                  CatRes = Amazon_Utilities.AWW_dropdownOp("catalogueType", Cat_Type, driver, wait, TC_no, ResultPath);
                  
                 
                  
                  driver.findElement(By.xpath("//*[@id='catalogueCode']")).sendKeys(ProdNo);
                  driver.findElement(By.xpath("//button[@type='submit']")).click();
                  res=true;
                  Thread.sleep(5000);
         // Take Screenshot                     
             String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  

           
                  return (res && CatRes);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return (res && CatRes);
           }
           finally
           {
                  return (res && CatRes);
           }
    }
    
    
    
    public static boolean Validate_Catalogue_Search(String ProdNo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           
           try{
                  int size= driver.findElements(By.xpath("//*[@id='exportable']/table/tbody/tr")).size();
                  for (int i=1;i<=size;i++)
                  {
                  
                  if (driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr/td["+i+"]")).getText().equals(ProdNo))
                  {
                        res=true;
                        //System.out.println("Item Present in Result");
                        break;
                  }
                  else
                  {
                        res=false;
                        //System.out.println("Item Not Present in Result");
                        
                  }
                  }
                  Robot robot = new Robot();
                  robot.keyPress(KeyEvent.VK_PAGE_DOWN);
                  robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
                  Thread.sleep(5000);
         // Take Screenshot                     
             String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  

           
                  return res;
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }

    
    /*
    
    public static boolean Validate_Order_Header_Details(String[] ordQnt,String[] shipAdd,String[] invoiceInfo,String[] tranInfo,String[] billAdd,String SalesOrdNo, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           	boolean res=false;
           
           
           	WebDriver driverDyanmo=null;
   			WebDriverWait dynamowait =null;
   		
           
           try{
                 
        	   Thread.sleep(3000);
        	   Amazon_Utilities.SalesOrd_SS("Sales Order", SalesOrdNo, driver, wait, TC_no, ResultPath);
        	   
        	   driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td")).click();
        	   
        	   
        	 
        	   	for(String winHandle : driver.getWindowHandles())
   				{
        		   driver.switchTo().window(winHandle);
   				}
        	   	
        	 // Take Screenshot                     
                String AfterLogin0=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin0, driver);
                Thread.sleep(3000);
                
                String customer=driver.findElement(By.xpath("//h4[@class='margin-zero product-title-top']")).getText();
        	   	
        	   	
        	   	
        	   	Thread.sleep(3000);
        	   	driver.findElement(By.id("order_Qnt_Info")).click();
        	   	
        	   	String[] ord_Qnt=new String[8];
        	   	String[] ord_Qnt_val=new String[8];
        	   	for(int i=0;i<8;i++)
        	   	{
        	   		ord_Qnt[i]=driver.findElement(By.xpath("//div[@id='OrderQuantityInfo']/div["+(i+1)+"]/h4")).getText();
        	   		ord_Qnt_val[i]=driver.findElement(By.xpath("//div[@id='OrderQuantityInfo']/div["+(i+1)+"]/span")).getText();
        	   	}
        	   	
        	 // Take Screenshot                     
                String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin, driver);
                
                
                if(Arrays.equals(ord_Qnt, ordQnt))
                {
                	//System.out.println("Order Quantity Headers Match");
                	utilityFileWriteOP.writeToLog(TC_no, "Order Quantity Headers MATCH ", "PASS",ResultPath);
                    
                }
                else
                {
                	throw new Exception("Order Quantity Headers MisMatch");
                }
        	   	
                
        	   	driver.findElement(By.id("order_Qnt_Info")).click();
        	   	
        	   	
        		
        	   	Thread.sleep(3000);
        	   	driver.findElement(By.id("ship_add")).click();
        	   	
        	   	String[] ship_add=new String[7];
        	   	String[] ship_add_val=new String[7];
        	   	for(int i=0;i<7;i++)
        	   	{
        	   		ship_add[i]=driver.findElement(By.xpath("//div[@id='ShippingAdd']/div["+(i+1)+"]/h4")).getText();
        	   		ship_add_val[i]=driver.findElement(By.xpath("//div[@id='ShippingAdd']/div["+(i+1)+"]/span")).getText();
        	   	}
        	   	
        	 // Take Screenshot                     
                String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                
                
                if(Arrays.equals(ship_add, shipAdd))
                {
                	//System.out.println("Shipping Address Headers Match");
                	utilityFileWriteOP.writeToLog(TC_no, "Shipping Address Headers MATCH ", "PASS",ResultPath);
                    
                }
                else
                {
                	throw new Exception("Shipping Address Headers MisMatch");
                }
        	   	
        	   	driver.findElement(By.id("ship_add")).click();
        	   	
        	   	
        	   	
        	   	
        	   	Thread.sleep(3000);
        	   	driver.findElement(By.id("invoice_info")).click();
        	   	
        	   	String[] invoice_info=new String[4];
        	   	String[] invoice_info_val=new String[4];
        	   	for(int i=0;i<4;i++)
        	   	{
        	   		invoice_info[i]=driver.findElement(By.xpath("//div[@id='InvoiceInfo']/div["+(i+1)+"]/h4")).getText();
        	   		invoice_info_val[i]=driver.findElement(By.xpath("//div[@id='InvoiceInfo']/div["+(i+1)+"]/span")).getText();
        	   	}
        	   	
        	 // Take Screenshot                     
                String AfterLogin2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin2, driver);
                
        	   	
                if(Arrays.equals(invoice_info, invoiceInfo))
                {
                	//System.out.println("Invoice Information Headers Match");
                	utilityFileWriteOP.writeToLog(TC_no, "Invoice Information Headers MATCH ", "PASS",ResultPath);
                    
                }
                else
                {
                	throw new Exception("Invoice Information Headers MisMatch");
                }
                
                
        	   	driver.findElement(By.id("invoice_info")).click();
        	   	
        	   	
        	   	
        	   	
        	 	Thread.sleep(3000);
        	   	driver.findElement(By.id("trans_info")).click();
        	   	
        	   	String[] trans_info=new String[7];
        	   	String[] trans_info_val=new String[6];
        	   	
        	   	for(int i=0;i<7;i++)
        		{
        	   		trans_info[i]=driver.findElement(By.xpath("//div[@id='TransmissionInfo']/div["+(i+1)+"]/h4")).getText();
        		}
        	   		
        	   	
        	   	
        	   	
        	   	
        	   	trans_info_val[0]=driver.findElement(By.xpath("//div[@id='TransmissionInfo']/div/div")).getText();
        		trans_info_val[5]=driver.findElement(By.xpath("//div[@id='TransmissionInfo']/div[6]/div")).getText();
        		//trans_info_val[6]=driver.findElement(By.xpath("//div[@id='TransmissionInfo']/div[7]/span")).getText();
        	  
        		
        		for(int i=1;i<5;i++)
        	   	{
        	   		trans_info_val[i]=driver.findElement(By.xpath("//div[@id='TransmissionInfo']/div["+(i+1)+"]/span")).getText();
        	   	}
        		
        		String msgCreatedAt=trans_info_val[2];
        		trans_info_val[2]=msgCreatedAt.substring(0, msgCreatedAt.indexOf(" "));
        		trans_info_val[2]=trans_info_val[2].substring(6, 10)+"-"+trans_info_val[2].substring(3, 5)+"-"+trans_info_val[2].substring(0, 2);
        		
        	   	
        	   	
        	 // Take Screenshot                     
                String AfterLogin3=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin3, driver);
                
        	   	
                if(Arrays.equals(trans_info, tranInfo))
                {
                	//System.out.println("Transaction Information Headers Match");
                	utilityFileWriteOP.writeToLog(TC_no, "Transaction Information Headers MATCH ", "PASS",ResultPath);
                    
                }
                else
                {
                	throw new Exception("Transaction Information Headers MisMatch");
                }
                
                
        	   	driver.findElement(By.id("trans_info")).click();
        	   	
        	   	
        	   	

        	 	Thread.sleep(3000);
        	   	driver.findElement(By.id("bill_add")).click();
        	   	
        	   	String[] bill_add=new String[7];
        	   	String[] bill_add_val=new String[7];
        	   	for(int i=0;i<7;i++)
        	   	{
        	   		bill_add[i]=driver.findElement(By.xpath("//div[@id='BillingAdd']/div["+(i+1)+"]/h4")).getText();
        	   		bill_add_val[i]=driver.findElement(By.xpath("//div[@id='BillingAdd']/div["+(i+1)+"]/span")).getText();
        	   	}
        	   	
        	 // Take Screenshot                     
                String AfterLogin4=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                TakeScreenShot.saveScreenShot(AfterLogin4, driver);
                
                
                
                if(Arrays.equals(bill_add, billAdd))
                {
                	//System.out.println("Billing Address Headers Match");
                	utilityFileWriteOP.writeToLog(TC_no, "Billing Address Headers MATCH ", "PASS",ResultPath);
                    
                }
                else
                {
                	throw new Exception("Billing Address Headers MisMatch");
                }
        	   	
        	   	driver.findElement(By.id("bill_add")).click();
        	   	
        	  
        	   // Take Screenshot                     
             String AfterLogin5=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin5, driver);
                  
             
            	 String[] ordHeaders = Amazon_Utilities.concatAll(ord_Qnt_val,ship_add_val,trans_info_val,bill_add_val);
            	 
            	// //System.out.println(Arrays.toString(ordHeaders));
            	 
            	 for(int i=0;i<ordHeaders.length;i++)
            	 {
            		 if(ordHeaders[i].equalsIgnoreCase("Information not yet available"))
            		 {
            			 ordHeaders[i]="null";
            		 }
            	 }
           
            	 for(int i=0;i<ordHeaders.length;i++)
            		 Utilities.excelCellValueWrite.writeValueToCell_sheet(ordHeaders[i], 3, 1, i, "TestData/DriverExcel.xls");
            	 
            	 
            	 


            	 
            	 
            	 
            	 
            	 
            	 
            	 
            	 
            	 
            	 
            	 
            	//Validate Dynamo DB values...	
 				
 				
 				
 				driverDyanmo=new ChromeDriver();
 					dynamowait=new WebDriverWait(driverDyanmo, 50);
 					
 					driverDyanmo.manage().deleteAllCookies();
 					driverDyanmo.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
 				 
 				 
 					driverDyanmo.get("https://manage.rackspace.com");
 					driverDyanmo.manage().window().maximize();
 				 
 				 boolean loginAWS=Amazon_RackSpace.aws_login(driverDyanmo, dynamowait, TC_no, "SYSTCS5G", "k7yTf3sE", ResultPath);
 					String awsDynamoDB_sc=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	       
 					TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
 					if(loginAWS==false)
 					{
 						throw new Exception("Dynamo DB navigation error");
 						//return false;
 					}
 					
 					boolean navToDynamoDB=Amazon_RackSpace.navigateToDynamoDB_Table(driverDyanmo, dynamowait, TC_no, ResultPath);
 					String awsDynamoDB_sc1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	       
 					TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
 					if(navToDynamoDB==false)
 					{
 						throw new Exception("Dynamo DB navigation error");
 						//return false;
 					}
 					boolean validateInDynamoDB=false;
 					
 					if(customer.equalsIgnoreCase("PH"))
 					{
 					 validateInDynamoDB=Amazon_RackSpace.validateSingleOrderMultipleItemsDynamoDB("OrderHead", driverDyanmo, dynamowait, TC_no, "wholesale", customer, SalesOrdNo, false, "TestData/DriverExcel.xls", ResultPath);
 					}
 					else
 					{
 						 validateInDynamoDB=Amazon_RackSpace.validateSingleOrderMultipleItemsDynamoDB("OrderHead", driverDyanmo, dynamowait, TC_no, "wholesale", customer.toLowerCase(), SalesOrdNo, false, "TestData/DriverExcel.xls",ResultPath);
 	 				}
 					String awsDynamoDB_sc2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	       
 					TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
 					//System.out.println("validateInDynamoDB "+validateInDynamoDB);
 					if(validateInDynamoDB==false)
 					{
 						throw new Exception("Dynamo DB validation error");
 						//return false;
 					}
 					
 			
 			
 			driverDyanmo.quit();
 	       
 			utilityFileWriteOP.writeToLog(TC_no, "Order Details Validated in Dynamo DB  ", "PASS",ResultPath);
 			res=true;
 			return res;
 			
 			
 			
 			
 			
 			
 			
 			
 			
 			
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             
             
             driverDyanmo.quit();
             
             return res;
           }
           finally
           {
                  return res;
           }
    }*/
    
    public static boolean CatalogueEnquiryButton_CatMngPage(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try
           {
                  Thread.sleep(3000);
        	   driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueEnquiries.svg']")).click();
        	   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Enquiry ", "PASS!!!",ResultPath);
                 // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Enquiry ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }

    
    public static boolean OrderButton_CatMngPage(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try
           {
                  
        	   driver.findElement(By.xpath("//div[@class='padd-right-41']")).click();
        	   utilityFileWriteOP.writeToLog(TC_no, "Order Page ", "PASS!!!",ResultPath);
                 // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Order Page ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    
    public static boolean catEnqButton_CatMngPage(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try
           {
                  
        	   driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueEnquiries.svg']")).click();
        	   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Enquiry ", "PASS!!!",ResultPath);
                 // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Enquiry Page ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    
    
    public static boolean catMngButton_CatMngPage(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try
           {
                  
        	   driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
        	   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Management ", "PASS!!!",ResultPath);
                 // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
            res=true;
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Management Page ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    
    
    
    public static boolean Validate_Catalogue_Upload_Page(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try
           {
                  
        	   driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueUploadGreen.svg']")).click();
        	   Thread.sleep(3000);
        	   
        	   String Catalogue_head=driver.findElement(By.xpath("//h4[@class='pad-lft-right-15']")).getText();
        	   if (Catalogue_head.equalsIgnoreCase("Catalogue Upload"))
        	   {
        		   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Upload Page ", "PASS!!!",ResultPath);
        		   // Take Screenshot                   
        		   String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
        		   TakeScreenShot.saveScreenShot(AfterLogin, driver);
        		   res=true;
        	   }
        	   else
        	   {
        		   System.out.println("String not matched");
        		   res=false;
        		   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Upload Page ", "Not Found!!!",ResultPath);
                   
                   // Take Screenshot                   
              String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
              TakeScreenShot.saveScreenShot(AfterLogin, driver);
        	   }
                  return res;
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Upload Page ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    
    
    public static boolean Add_product(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           try
           {
                 Thread.sleep(5000);
                 
        	   driver.findElement(By.xpath("//img[@src='app/images/icon_6.png']")).click();
        	   for (String winHandle : driver.getWindowHandles()) {
        		    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
        		}

        		
        	   
        	   String placeholder=driver.findElement(By.xpath("//input[@id='productid']")).getAttribute("placeholder");
        	   System.out.println(placeholder);
        	   if(placeholder.equalsIgnoreCase("000000000"))
        	   {
        	   utilityFileWriteOP.writeToLog(TC_no, "Add product page  ", "PASS!!!",ResultPath);
                 // Take Screenshot                   
             String Add_product_page=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(Add_product_page, driver);
             driver.findElement(By.xpath("//input[@id='productid']")).sendKeys("012345678911");
             
             
              if(driver.findElement(By.xpath("//input[@id='productid']")).getAttribute("value").length()==10)
              {
            	  utilityFileWriteOP.writeToLog(TC_no, "10 digit validation  ", "PASS!!!",ResultPath);
                  // Take Screenshot                   
              String ten_digit_validation=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
              TakeScreenShot.saveScreenShot(ten_digit_validation, driver);
              res=true;
   		   return res;
              }else{
            	  System.out.println("10 digit validation fail");
            	  res=false;
       		   return res;
              }
            	  
             
            
             
        	   }else
        	   {
        		   System.out.println("Placeholder not match");
        		  
        		   res=false; 
        		   return res;
        	   }
            
                  
           }
           catch(Exception e)
           {
                  res=false;  
                  System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "Add product page  ", "Not Found!!!",ResultPath);
                  
                  // Take Screenshot                   
             String Add_product=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(Add_product, driver);
             return res;
           }
           finally
           {
                  return res;
           }
    }
    

    public static boolean CatalogueExport_Tab( WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           
           try{
                  
                  driver.findElement(By.xpath("//div[@class='menu-product-name' and contains(text(),'Catalogue Export')]")).click();

                  
                  Thread.sleep(1000);
                 
                  
                 
               // Take Screenshot                     
                  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  //utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Entered", "PASS",ResultPath);   
                  
                  
                  
                  try{
                 	 
                 	 boolean valid=driver.findElement(By.xpath("//span[@class='bradcrumb-text' and contains(text(),'Catalogue Export')]")).isDisplayed();
                 	 
                 	 if (valid) {
         				
                 		 res=true;
                 		 
         			}
                  }
                  catch(Exception e){
                	  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Page Validation", "FAIL",ResultPath);
                  }

           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return (res);
           }
           finally
           {
                  return (res);
           }
    }
    
    public static boolean catalogType_ExportTab(String cat_type, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           
           try{
        	   
	         	  driver.findElement(By.xpath("//select[@id='catalogueType']")).click();
	 	  
	         	  
	         	  driver.findElement(By.xpath("//select[@id='catalogueType']/option[contains(text(),'"+cat_type+"')]")).click();
	         	  
	         	 driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'VIEW')]")).click();
	         	 
                  
                  Thread.sleep(1000);
                 
                  
                 
               // Take Screenshot                     
                  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  //utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Entered", "PASS",ResultPath);   
                  
                  res=true;
                  
                  try{
                	  
                	  boolean valid = driver.findElement(By.xpath("//h4[contains(text(),'Search Results for Catalogue Files')]")).isDisplayed();
                	  
                	  if (valid) {
						
                		
                		  
                		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                          TakeScreenShot.saveScreenShot(AfterLogin, driver);
                            
                	  }
                  }
                  
                  catch(Exception e)
                  {
                	  System.out.println("Entered Invalid Details");
                	  utilityFileWriteOP.writeToLog(TC_no, "Entered Invalid Details", "Error Found",ResultPath);
                  }
         
                  
                  res=true;
           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return (res);
           }
           finally
           {
                  return (res);
           }
    }
    
    
    public static boolean intiate_CatalogueExport( WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           
           try{
                  
                  driver.findElement(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")).click();

                  try{
                  	  
                  	  boolean valid2 = driver.findElement(By.xpath("//div[@class='alert alert-danger text-center error-message']")).isDisplayed();
                  	  
                  	  if (valid2) {
       					
                  		res=true;
                  		
                  		
                  		  utilityFileWriteOP.writeToLog(TC_no, "No Catalogue Type Choosen", "PASS",ResultPath);
                  		  
                  		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                            TakeScreenShot.saveScreenShot(AfterLogin, driver);
                         return res;    
                  	  }
                    }
              	  catch(Exception e2){
              	 
               
                  
                  
         try{
        	 
        	 boolean valid=driver.findElement(By.xpath("//span[contains(text(),'Your request for intiating Catalogue Export has been accepted. File will be generated as')]")).isDisplayed();
        	 
        	 if (valid) {
				
        		 res=true;
        		 
        		  utilityFileWriteOP.writeToLog(TC_no, "New Catalogue Export Generated", "PASS",ResultPath);
        		  
        		  //System.out.println("New Catalogue Export Generated");
        		  
        		 driver.findElement(By.xpath("//button[@id='close-popup']")).click();
        		 return res;
			}
         }
         catch(Exception e){
        	 
        	 try{
           	  
           	  boolean valid1 = driver.findElement(By.xpath("//span[contains(text(),'Generation of Export Catalogue is already in Progress or just completed. See file')]")).isDisplayed();
           	  
           	  if (valid1) {
					
           		res=true;
           		
           		
           		  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Already Generated", "PASS",ResultPath);
           		 // System.out.println("Catalogue Export Already Generated");
           		  
           		 driver.findElement(By.xpath("//button[@id='close-popup']")).click();
           		  
           		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                     TakeScreenShot.saveScreenShot(AfterLogin, driver);
                      return res;
           	  }
             }
       	  catch(Exception e1){
       		  
       			 
       	  }
       	  	 String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             
            
        	 utilityFileWriteOP.writeToLog(TC_no, "Initaiate Catalogue Export", "FAIL",ResultPath);
        	 
        	 
         }
}
         	return (res);
         	
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return (res);
           }
           finally
           {
                  return (res);
           }
    }
    
    
    
    public static boolean addProd_Catalog_Cancel(String[] prodDets, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
			boolean rangeCatalog= Amazon_Utilities.AWW_dropdownOp("catalogueType", prodDets[0] , driver, wait, TC_no, ResultPath);
			//System.out.println("Range/Catalogue Type Entered");
			
			boolean availability= Amazon_Utilities.AWW_dropdownOp("availabilityStatus", prodDets[3] , driver, wait, TC_no, ResultPath);
			//System.out.println("Availability Type Entered");
			
			boolean workflow= Amazon_Utilities.AWW_dropdownOp("workflowStatusName", prodDets[4] , driver, wait, TC_no, ResultPath);
			//System.out.println("WorkFlow Type Entered");
			
			driver.findElement(By.id("description")).sendKeys(prodDets[5]);
			//System.out.println("Description Entered");
			
			
			
			driver.findElement(By.id("identifier_MIN")).sendKeys(prodDets[1]);
			//System.out.println("MIN Entered");
			
			utilityFileWriteOP.writeToLog(TC_no, "New Product Mandatory Details Entered:::", " PASS",ResultPath);
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        
	         Thread.sleep(3000);
	         
	         ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
	         
	         Thread.sleep(1000);
	         
	         WebElement element = driver.findElement(By.xpath("//button[contains(text(),'Cancel')]"));

	         JavascriptExecutor executor = (JavascriptExecutor)driver;
	         executor.executeScript("arguments[0].click()", element);
	         
	        // System.out.println(" Cancel button clicked");
	         
	         utilityFileWriteOP.writeToLog(TC_no, "Clicked  Cancel Button:::", " PASS",ResultPath);
			
	         Thread.sleep(1000);
				         
	         res=true;
	         return res;
		}  
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "Adding new Product to Catalogue:::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
		finally
		{
			return res;
		}
	}
	
    
    public static boolean catalogExport_Download(String cat_type, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
    {
           boolean res=false;
           
           try{
        	   
	         	  driver.findElement(By.xpath("//select[@id='catalogueType']")).click();
	 	  
	         	  
	         	  driver.findElement(By.xpath("//select[@id='catalogueType']/option[contains(text(),'"+cat_type+"')]")).click();
	         	  
	         	 
                  Thread.sleep(1000);
                 
                  
                 
               // Take Screenshot                     
                  String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                  TakeScreenShot.saveScreenShot(AfterLogin1, driver);
                  //utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Entered", "PASS",ResultPath);   
                  
                  res=true;
                  
                  driver.findElement(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")).click();

                      
             try{
            	 
            	 boolean valid=driver.findElement(By.xpath("//span[contains(text(),'Your request for intiating Catalogue Export has been accepted. File will be generated as')]")).isDisplayed();
            	 
            	 if (valid) {
    				
            		 res=true;
            		 
            		  utilityFileWriteOP.writeToLog(TC_no, "New Catalogue Export Generated", "PASS",ResultPath);
            		  
            		  String txt=driver.findElement(By.xpath("//span[contains(text(),'Your request for intiating Catalogue Export has been accepted. File will be generated as')]")).getText();
            		  

            		  String file=txt.substring(txt.lastIndexOf("as")+3, txt.length());
                 		
                 		//System.out.println(file);
            		   
            		  //System.out.println("New Catalogue Export Generated");
            		  
            		  Thread.sleep(1000);
                      
                      
                      
                      // Take Screenshot                     
                         String AfterLogin11=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                         TakeScreenShot.saveScreenShot(AfterLogin11, driver);
                         //utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Entered", "PASS",ResultPath);   
                        
            		 driver.findElement(By.xpath("//button[@id='close-popup']")).click();
            		 
            		  driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'VIEW')]")).click();
     	         	 
                      Thread.sleep(1000);
                      String AfterLogin111=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                      TakeScreenShot.saveScreenShot(AfterLogin111, driver);
                      
                      Thread.sleep(5000);
                      
                      List<WebElement> row=driver.findElements(By.xpath("//*[@id='exportable']/table/tbody/tr"));
                      
                      int rowCount=row.size();
                      
                      for (int i = 1; i <= rowCount; i++) 
                      { 					
	                     if(driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr["+i+"]/td[2]")).getText().equalsIgnoreCase(file))
	                     {
	                    	// System.out.println("file matched");
	                    	 driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr["+i+"]/td[3]")).click();
	                     }
	                                              
                  	}
                      
                    
            		 return res;
    			}
             }
             catch(Exception e){
            	 
            	 try{
               	  
               	  boolean valid1 = driver.findElement(By.xpath("//span[contains(text(),'Generation of Export Catalogue is already in Progress or just completed. See file')]")).isDisplayed();
               	  
               	  if (valid1) {
    					
               		res=true;
               		
               		
               		  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Already Generated", "PASS",ResultPath);
               		 // System.out.println("Catalogue Export Already Generated");
               		  
               		String txt=driver.findElement(By.xpath("//span[contains(text(),'Generation of Export Catalogue is already in Progress or just completed. See file')]")).getText();
          		  
               		String file=txt.substring(txt.lastIndexOf("file")+5, txt.length());
               		
               	//	System.out.println(file);
          		  
               	  Thread.sleep(1000);
                  
                  
                  
                  // Take Screenshot                     
                     String AfterLogin11=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                     TakeScreenShot.saveScreenShot(AfterLogin11, driver);
                     //utilityFileWriteOP.writeToLog(TC_no, "Catalogue Export Entered", "PASS",ResultPath);   
                    
               		  
               		 driver.findElement(By.xpath("//button[@id='close-popup']")).click();
               		  
               		  String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                         TakeScreenShot.saveScreenShot(AfterLogin, driver);
                         
                         driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'VIEW')]")).click();
        	         	 
                         Thread.sleep(1000);
                         String AfterLogin111=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
                         TakeScreenShot.saveScreenShot(AfterLogin111, driver);
                       
                       
                         Thread.sleep(5000);
                         
                         List<WebElement> row=driver.findElements(By.xpath("//*[@id='exportable']/table/tbody/tr"));
                         
                         int rowCount=row.size();
                         
                         for (int i = 1; i <= rowCount; i++) 
                         {
                        	 					
	                     if(driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr["+i+"]/td[2]")).getText().equalsIgnoreCase(file))
	                     {
	                    	// System.out.println("file matched");
	                    	 driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr["+i+"]/td[3]")).click();
	                     }
	                                              
                     	}
                         
                          return res;
               	  }
                 }
           	  catch(Exception e1){
           		  
           			 
           	  }
             }      
                  res=true;
           
                  return (res);
           }
           catch(Exception e)
           {
                  res=false;                 
                  e.printStackTrace();
                  //System.out.println(e);
                  utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
                  
                  // Take Screenshot                   
             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
             TakeScreenShot.saveScreenShot(AfterLogin, driver);
             return (res);
           }
           finally
           {
                  return (res);
           }
    }
    
    
	public static boolean addProd_Catalog_Invalid(String[] prodDets,String prodMIN, String prodName, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{

		boolean res=false;
		
		try
		{
			String winHandleBefore = driver.getWindowHandle();
			
			 boolean cat_val= Amazon_WebPortal_Methods.addProd_Catalog_val(prodMIN, prodName, driver, wait, TC_no, ResultPath);
			  
			 boolean add_cat= Amazon_WebPortal_Methods.addProd_Catalog_Cancel(prodDets, driver, wait, TC_no, ResultPath);
			  
			  driver.switchTo().window(winHandleBefore);
			  
			// Take Screenshot			   
		         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
			  
		       
					
			
					
			  try
			  {
		        	 
				  driver.findElement(By.xpath("//span[contains(text(),'Catalogue Search')]")).click();
				  
				  res=true&&add_cat&&cat_val;
				  return res;
			  }
			  catch(Exception e1)
			  {
				  e1.printStackTrace();
			  }
			}
		
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "FAIL::", "Exception Occured::"+e,ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
		 }
        finally
        {
               return (res);
        }
 }
 
	
	public static boolean editProd_Catalog_Cancel(String[] prodDets, WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	{
		boolean res=false;
		try
		{
			String winHandleBefore = driver.getWindowHandle();
			
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[contains(text(),'Catalogue Enquiry')]")).click();
			Thread.sleep(3000);
			Amazon_Utilities.AWW_dropdownOp("catalogueType", prodDets[0], driver, wait, TC_no, ResultPath);
			Thread.sleep(3000);
			driver.findElement(By.id("catalogueCode")).sendKeys(prodDets[1]);
			Thread.sleep(3000);
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			
			//utilityFileWriteOP.writeToLog(TC_no, "Catalogue type Entered:::", " PASS",ResultPath);
			
			// Take Screenshot			   
	         String AfterLogin1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin1, driver);
			
			Thread.sleep(3000);
			
			try
			{
			//System.out.println("Catalogue Results Displayed");
			WebElement element5=driver.findElement(By.xpath("//div[@id='exportable']/table/tbody/tr/td[contains(text(),'"+prodDets[1]+"')]"));
			
			JavascriptExecutor executor5 = (JavascriptExecutor)driver;
	        executor5.executeScript("arguments[0].click()", element5);
	         
			}
			catch(Exception e)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Product not found in LIST", " FAIL",ResultPath);
				throw new Exception("Product not found in LIST");
			}
			
			//System.out.println("Product Page Opened");
			 for(String winHandle : driver.getWindowHandles())
			 {
                 driver.switchTo().window(winHandle);
             }
			 
			 Thread.sleep(5000);

			String prodName= driver.findElement(By.xpath("//h2[@class='orders-heading data-row bottom-line font-size-1-7em text-bold']")).getText();
			
			if(prodName.contains(prodDets[2]))
			{
				//System.out.println("Product Name Match");	
			}
			else
			{
				throw new Exception("Product Name Not Matching");
			}
			
			String prodID= driver.findElement(By.xpath("//div[@class='orders-heading avail-text']")).getText();
			
			if(prodID.contains(prodDets[1]))
			{
				//System.out.println("Product MIN Match");	
			}
			else
			{
				throw new Exception("Product MIN Not Matching");
			}
			
			Thread.sleep(3000);
			//System.out.println("Edit button will be clicked");
			driver.findElement(By.className("edit-text")).click();
			
			//System.out.println("Edit button clicked");
			
			utilityFileWriteOP.writeToLog(TC_no, "Click on Edit button ::", " PASS",ResultPath);
			
			Thread.sleep(3000);
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         	Thread.sleep(3000);
	         
	         	boolean availability= Amazon_Utilities.AWW_dropdownOp("availabilityStatus", prodDets[3] , driver, wait, TC_no, ResultPath);
				//System.out.println("Availability Type Entered");
				
				//boolean workflow= Amazon_Utilities.AWW_dropdownOp("workflow", prodDets[4] , driver, wait, TC_no, ResultPath);
				Thread.sleep(3000);
				
				driver.findElement(By.id("workflow")).click();
				driver.findElement(By.xpath("//select[@id='"+"workflow"+"']/option[contains(@value,'"+prodDets[4] +"')]")).click();
				//System.out.println("WorkFlow Type Entered");
				
				Thread.sleep(3000);
				driver.findElement(By.xpath("//textarea[@name='productDesc']")).clear();
				driver.findElement(By.xpath("//textarea[@name='productDesc']")).sendKeys(prodDets[5]);
				//System.out.println("Description Entered");
				
				driver.findElement(By.id("identifier_MIN")).clear();
				
				driver.findElement(By.id("identifier_MIN")).sendKeys(prodDets[1]);
				//System.out.println("MIN Entered");
				
				utilityFileWriteOP.writeToLog(TC_no, "Enter New Product Mandatory Details::", " PASS",ResultPath);
				// Take Screenshot			   
		         String AfterLogin2=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
		         TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		        
		         Thread.sleep(3000);
		         
		         ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
		         
		         WebElement element = driver.findElement(By.xpath("//button[@class='prdt-save-btn' and contains(text(),'Cancel')]"));

		         JavascriptExecutor executor = (JavascriptExecutor)driver;
		         executor.executeScript("arguments[0].click()", element);
		         
		         //System.out.println("Save button clicked");
		         
		         utilityFileWriteOP.writeToLog(TC_no, "Click on Cancel Button::", " PASS",ResultPath);
				   
		         driver.switchTo().window(winHandleBefore);
				  
					// Take Screenshot			   
				         String AfterLogin11=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
				         TakeScreenShot.saveScreenShot(AfterLogin11, driver);
					  
				  			
					  try
					  {
				        	 
						  driver.findElement(By.xpath("//span[contains(text(),'Catalogue Search')]")).click();
						  
						  res=true;
						  return res;
					  }
					  catch(Exception e1)
					  {
						  e1.printStackTrace();
					  }
		         
		    
			return res;
			         
		}
		catch(Exception e)
		{
			//System.out.println("The exception is:::"+e,ResultPath);
			
			utilityFileWriteOP.writeToLog(TC_no, "Edit Product Details::", " FAIL",ResultPath);
			
			
			// Take Screenshot			   
	         String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";	               
	         TakeScreenShot.saveScreenShot(AfterLogin, driver);
	         
	         
			res=false;
			return res;
			
		}
	}
	
   
	
	 public static boolean Catalogue_Upload_Page(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {
	           boolean res=false;
	           try
	           {
	                  
	        	   driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueUploadGreen.svg']")).click();
	        	   Thread.sleep(3000);
	        	   
	        	   String Catalogue_head=driver.findElement(By.xpath("//span[contains(text(),'View uploaded catalogue files')]")).getText();
	        	   if (Catalogue_head.equalsIgnoreCase("View uploaded catalogue files"))
	        	   {
	        		   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Upload Page ", "PASS!!!",ResultPath);
	        		   // Take Screenshot                   
	        		   String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	        		   TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        		   res=true;
	        	   }
	        	   else
	        	   {
	        		   System.out.println("String not matched");
	        		   res=false;
	        		   utilityFileWriteOP.writeToLog(TC_no, "Catalogue Upload Page ", "Not Found!!!",ResultPath);
	                   
	                   // Take Screenshot                   
	              String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	              TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        	   }
	                  return res;
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Catalogue Upload Page ", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(AfterLogin, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	 
	 public static boolean Catalogue_Upload_Button(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {
	           boolean res=false;
	           try
	           {
	                  
	        	   driver.findElement(By.xpath("//button[contains(text(),'Upload new catalogue file')]")).click();
	        	   Thread.sleep(3000);
	        	   
	        	   String Catalogue_head=driver.findElement(By.xpath("//button[contains(text(),'browse for files')]")).getText();
	        	   if (Catalogue_head.equalsIgnoreCase("browse for files"))
	        	   {
	        		   utilityFileWriteOP.writeToLog(TC_no, "Browse for files for Catalogue Upload", "PASS!!!",ResultPath);
	        		   // Take Screenshot                   
	        		   String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	        		   TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        		   res=true;
	        	   }
	        	   else
	        	   {
	        		   System.out.println("String not matched");
	        		   res=false;
	        		   utilityFileWriteOP.writeToLog(TC_no, "Browse for files for Catalogue Upload", "Not Found!!!",ResultPath);
	                   
	                   // Take Screenshot                   
	              String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	              TakeScreenShot.saveScreenShot(AfterLogin, driver);
	        	   }
	                  return res;
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Browse for files for Catalogue Upload", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String AfterLogin=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(AfterLogin, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	 

	    public static boolean edit_description(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {    String gettext="";  
	    	 String test="Tesing";
	           boolean res=false;
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'DATA TEAM')]")).click();
	        	   utilityFileWriteOP.writeToLog(TC_no, "Data team page", "PASS!!!",ResultPath);
	                 // Take Screenshot                   
	             String Data_team=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(Data_team, driver);
	             
	           //input[@class="textbox-wid-bor ng-pristine ng-valid ng-touched"].sendKeys();
	             Thread.sleep(5000);
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).clear();
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).sendKeys(test);
	             utilityFileWriteOP.writeToLog(TC_no, "Eneter value ", "PASS!!!",ResultPath);
	             // Take Screenshot                   
	         String enter_description=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(enter_description, driver);
	             
	             //textbox-wid-bor ng-pristine ng-valid ng-touched
	             ////a[@href="#Nutritional"]  //a[@href="#Safety"]   //a[@href="#Battery"]   //a[@href="#Dimensions"]   #Financials   #Identifiers
	             //#Descriptional      #BWS   #Warehouse
	         Thread.sleep(1000);
	             driver.findElement(By.xpath("//a[@href='#Nutritional']")).click();
	             Thread.sleep(1000);
	             utilityFileWriteOP.writeToLog(TC_no, "Nutritiona ", "PASS!!!",ResultPath);
	             // Take Screenshot                   
	         String nutritional=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(nutritional, driver);
	              gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	              System.out.println(gettext);
	             if(gettext.equalsIgnoreCase(test))
	             {
	            	 System.out.println("Text match");
	            	 res=true;
	             }else
	             {
	            	 System.out.println("Text not match");
	            	 res=false;
	             }
	             Thread.sleep(1000);
	             driver.findElement(By.xpath("//a[@href='#Safety']")).click();
	             Thread.sleep(1000);
	             utilityFileWriteOP.writeToLog(TC_no, "Safety ", "PASS!!!",ResultPath);
	             // Take Screenshot                   
	         String Safety=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(Safety, driver);
	              gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	             if(gettext.equalsIgnoreCase(test))
	             {
	            	 System.out.println("Text match");
	            	 res=true;
	             }else
	             {
	            	 System.out.println("Text not match");
	            	 res=false;
	             }
	             Thread.sleep(1000);
	             driver.findElement(By.xpath("//a[@href='#Battery']")).click();
	             Thread.sleep(1000);
	             utilityFileWriteOP.writeToLog(TC_no, "Battery page ", "PASS!!!",ResultPath);
	             // Take Screenshot                   
	         String battery=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(battery, driver);
	             gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	            if(gettext.equalsIgnoreCase(test))
	            {
	           	 System.out.println("Text match");
	           	 res=true;
	            }else
	            {
	           	 System.out.println("Text not match");
	           	 res=false;
	            }
	             Thread.sleep(1000);
	            driver.findElement(By.xpath("//a[@href='#Dimensions']")).click();
	            Thread.sleep(1000);
	            utilityFileWriteOP.writeToLog(TC_no, "Dimensions page  ", "PASS!!!",ResultPath);
	            // Take Screenshot                   
	        String dimensions=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	        TakeScreenShot.saveScreenShot(dimensions, driver);
	            gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	           if(gettext.equalsIgnoreCase(test))
	           {
	          	 System.out.println("Text match");
	          	 res=true;
	           }else
	           {
	          	 System.out.println("Text not match");
	          	 res=false;
	           }
	           driver.findElement(By.xpath("//a[@href='#Financials']")).click();
	           Thread.sleep(1000);
	           utilityFileWriteOP.writeToLog(TC_no, "Financials page  ", "PASS!!!",ResultPath);
	           // Take Screenshot                   
	       String Financials=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	       TakeScreenShot.saveScreenShot(Financials, driver);
	           
	           gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	          if(gettext.equalsIgnoreCase(test))
	          {
	         	 System.out.println("Text match");
	         	 res=true;
	          }else
	          {
	         	 System.out.println("Text not match");
	         	 res=false;
	          }
	         
	          driver.findElement(By.xpath("//a[@href='#Identifiers']")).click();
	          Thread.sleep(1000);
	          utilityFileWriteOP.writeToLog(TC_no, "Identifiers page  ", "PASS!!!",ResultPath);
	          // Take Screenshot                   
	      String Identifiers=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	      TakeScreenShot.saveScreenShot(Identifiers, driver);
	          
	          gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	         if(gettext.equalsIgnoreCase(test))
	         {
	        	 System.out.println("Text match");
	        	 res=true;
	         }else
	         {
	        	 System.out.println("Text not match");
	        	 res=false;
	         } 
	         
	         driver.findElement(By.xpath("//a[@href='#Descriptional']")).click();
	         Thread.sleep(1000);
	         utilityFileWriteOP.writeToLog(TC_no, "Descriptional page  ", "PASS!!!",ResultPath);
	         // Take Screenshot                   
	     String Descriptional=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	     TakeScreenShot.saveScreenShot(Descriptional, driver);
	         gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	        if(gettext.equalsIgnoreCase(test))
	        {
	       	 System.out.println("Text match");
	       	 res=true;
	        }else
	        {
	       	 System.out.println("Text not match");
	       	 res=false;
	        }
	        
	        driver.findElement(By.xpath("//a[@href='#BWS']")).click();
	        Thread.sleep(1000);
	        utilityFileWriteOP.writeToLog(TC_no, "BWS page  ", "PASS!!!",ResultPath);
	        // Take Screenshot                   
	    String BWS=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	    TakeScreenShot.saveScreenShot(BWS, driver);
	        gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	       if(gettext.equalsIgnoreCase(test))
	       {
	      	 System.out.println("Text match");
	      	 res=true;
	       }else
	       {
	      	 System.out.println("Text not match");
	      	 res=false;
	      	return res;
	       }
	       driver.findElement(By.xpath("//a[@href='#Warehouse']")).click();
	       Thread.sleep(1000);
	       utilityFileWriteOP.writeToLog(TC_no, "Warehouse page  ", "PASS!!!",ResultPath);
	       // Take Screenshot                   
	   String Warehouse=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	   TakeScreenShot.saveScreenShot(Warehouse, driver);
	       gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	      if(gettext.equalsIgnoreCase(test))
	      {
	     	 System.out.println("Text match");
	     	 res=true;
	     	return res;
	      }else
	      {
	     	 System.out.println("Text not match");
	     	 res=false;
	     	return res;
	      }   
	           
	           
	           
	            	 
	            
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Data team page", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    //******************************Description**************************************
	    
	    
	    //**********************************Supply Chain
	    public static boolean supply_chain(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {    String gettext="";  
	    	 String test="Tesing";
	           boolean res=false;
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'SUPPLY CHAIN')]")).click();
	        	   utilityFileWriteOP.writeToLog(TC_no, "Supply Chain page", "PASS!!!",ResultPath);
	                 // Take Screenshot                   
	             String supply_chain=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(supply_chain, driver);
	             
	           //input[@class="textbox-wid-bor ng-pristine ng-valid ng-touched"].sendKeys();
	             Thread.sleep(5000);
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).clear();
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).sendKeys(test);
	             Thread.sleep(1000);
	             utilityFileWriteOP.writeToLog(TC_no, "Eneter value", "PASS!!!",ResultPath);
	             // Take Screenshot                   
	         String enter_value=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(enter_value, driver);
	             
	           
	       driver.findElement(By.xpath("//a[@href='#Warehouse']")).click();
	       Thread.sleep(1000);
	       utilityFileWriteOP.writeToLog(TC_no, "Warehouse page ", "PASS!!!",ResultPath);
	       // Take Screenshot                   
	   String Warehouse=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	   TakeScreenShot.saveScreenShot(Warehouse, driver);
	       gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	      if(gettext.equalsIgnoreCase(test))
	      {
	     	 System.out.println("Text match");
	     	 res=true;
	     	
	      }else
	      {
	     	 System.out.println("Text not match");
	     	 res=false;
	     	
	      }   
	           
	           
	           
	            	 
	            
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Data team page", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    
	    //*************End supply Chain
	    //******************Commercial***********************************************************
	    public static boolean commercial_page(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {   String digit="1"; 
	    	String gettext="";  
	    	 String test="Tesing";
	           boolean res=false;
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'COMMERCIAL')]")).click();
	        	   Thread.sleep(1000);
	        	   utilityFileWriteOP.writeToLog(TC_no, "Commercial page", "PASS!!!",ResultPath);
	                 // Take Screenshot                   
	             String commercail_page=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(commercail_page, driver);
	             
	           //input[@class="textbox-wid-bor ng-pristine ng-valid ng-touched"].sendKeys();
	             Thread.sleep(5000);
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).clear();
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).sendKeys(test);
	      	   Thread.sleep(1000);
	    	   utilityFileWriteOP.writeToLog(TC_no, "Enter value ", "PASS!!!",ResultPath);
	             // Take Screenshot                   
	         String enter_value=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(enter_value, driver);
	             
	           
	       driver.findElement(By.xpath("//a[@href='#MUC']")).click();
	       Thread.sleep(1000);
		   utilityFileWriteOP.writeToLog(TC_no, "MUC page", "PASS!!!",ResultPath);
	         // Take Screenshot                   
	     String MUC=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	     TakeScreenShot.saveScreenShot(MUC, driver);
	       gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	      if(gettext.equalsIgnoreCase(test))
	      {
	     	 System.out.println("Text match");
	     	 res=true;
	     	
	      }else
	      {
	     	 System.out.println("Text not match");
	     	 res=false;
	     	
	      }   
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]/input")).clear();
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]/input")).sendKeys("1");
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//a[@href='#StandardMarkup']")).click();
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//a[@href='#MUC']")).click();
	      Thread.sleep(1000);
		   utilityFileWriteOP.writeToLog(TC_no, "MUC page", "PASS!!!",ResultPath);
	        // Take Screenshot                   
	    String digit_validation=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	    TakeScreenShot.saveScreenShot(digit_validation, driver);
	      gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]/input")).getAttribute("value");
	      System.out.println(gettext);
	      if(gettext.equalsIgnoreCase(digit))
	      {
	     	 System.out.println("Text match");
	     	 res=true;
	     	
	      }else
	      {
	     	 System.out.println("Text not match");
	     	 res=false;
	     	
	      }   
	           
	            	 
	            
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Data team page", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    //*******************************End Commercial*******************************************
	    //******************************Disable cost unit*****************************************
	    public static boolean cost_unit(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {   String digit="1"; 
	    	String gettext="";  
	    	 String test="Tesing";
	           boolean res=false;
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'COMMERCIAL')]")).click();
	        	   utilityFileWriteOP.writeToLog(TC_no, "Commercial page", "PASS!!!",ResultPath);
	                 // Take Screenshot                   
	             String commercail_page=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(commercail_page, driver);
	             
	           //input[@class="textbox-wid-bor ng-pristine ng-valid ng-touched"].sendKeys();
	             Thread.sleep(5000);
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).clear();
	             driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).sendKeys(test);
	             
	           
	       driver.findElement(By.xpath("//a[@href='#MUC']")).click();
	       gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr/td/input")).getAttribute("value");
	      if(gettext.equalsIgnoreCase(test))
	      {
	     	 System.out.println("Text match");
	     	 res=true;
	     	
	      }else
	      {
	     	 System.out.println("Text not match");
	     	 res=false;
	     	
	      }   
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]/input")).clear();
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]/input")).sendKeys("1");
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//a[@href='#StandardMarkup']")).click();
	      Thread.sleep(1000);
	      driver.findElement(By.xpath("//a[@href='#MUC']")).click();
	      gettext=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]/input")).getAttribute("value");
	      System.out.println(gettext);
	      if(gettext.equalsIgnoreCase(digit))
	      {
	     	 System.out.println("Text match");
	     	 res=true;
	     	 
	      }else
	      {
	     	 System.out.println("Text not match");
	     	 res=false;
	     	
	      }   
	      
	      Thread.sleep(3000);
	      WebElement element =  driver.findElement(By.xpath("//*[@id='nprDataTbl']/tbody/tr[1]/td[5]"));
	     
	      
	    
	      System.out.println(element.getAttribute("value"));
	      
	      
	   
	    
	        
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Data team page", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    //*****************************End diable cost unit
	    
	    //********************************Supply_Chain_exclusion_list***********************
	    public static boolean exclusion_list(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {    String gettext="";  
	    	 String test="Tesing";
	           boolean res=false;
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'SUPPLY CHAIN')]")).click();
	        	   utilityFileWriteOP.writeToLog(TC_no, "Supply Chain page", "PASS!!!",ResultPath);
	                 // Take Screenshot                   
	             String supply_chain=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(supply_chain, driver);
	              Thread.sleep(5000);
	             
	            
	              
	              System.out.println("Validate exclusion list in screenshot");
	         
	           }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Data team page", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    
	    
	    //******************************End_Supply_Chain_exclusion_list*********************
	    
	    //*****************************Download**************************************************
	    public static boolean download(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {      String Batch_ID,	Batch_Type,	Amazon_Channel,	Upload_Date, Batch_Description ,Total_Products,Rejected_Deleted,Errored,Data_Review,Commercial_Review,Supply_Ch_Review,Awaiting_Export,Exported;
	           
	           
	           boolean res=false;
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//h5[@id='mr']")).click();
	        	  
	                 // Take Screenshot                   
	             String manage_page=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(manage_page, driver);
	              Thread.sleep(5000);
	          /*  //table[@id="nprDataTbl"]/tbody/tr[1]/td[2]
	            //table[@id="nprDataTbl"]/tbody/tr[1]/td[3]
	            //table[@id="nprDataTbl"]/tbody/tr[1]/td[4]
	            //table[@id="nprDataTbl"]/tbody/tr[1]/td[5]
	            //table[@id="nprDataTbl"]/tbody/tr[1]/td[11]
	              Batch_ID=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[2]")).getText();
	              System.out.println(Batch_ID);
	              Batch_Type=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[3]")).getText();
	              System.out.println(Batch_Type);
	              Amazon_Channel=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[4]")).getText();
	              System.out.println(Amazon_Channel);
	              Upload_Date=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[5]")).getText();
	              System.out.println(Upload_Date);
	              Batch_Description=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[6]")).getText();
	              System.out.println(Batch_Description);
	              Total_Products=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[7]")).getText();
	              System.out.println(Total_Products);
	              Rejected_Deleted=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[8]")).getText();
	              System.out.println(Rejected_Deleted);
	              Errored=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[9]")).getText();
	              System.out.println(Errored);
	              Data_Review=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[10]")).getText();
	              System.out.println(Data_Review);
	              Commercial_Review=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[11]")).getText();	
	              System.out.println(Commercial_Review);
	              Supply_Ch_Review=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[12]")).getText();
	              System.out.println(Supply_Ch_Review);
	              Awaiting_Export=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[13]")).getText();
	              System.out.println(Awaiting_Export);
	              Exported=driver.findElement(By.xpath("//table[@id='nprDataTbl']/tbody/tr[1]/td[14]")).getText();
	              System.out.println(Exported);*/
	              Thread.sleep(5000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconDownload.svg']")).click();
	              
	              
	              // Take Screenshot                   
	          String manage_page_download=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(manage_page_download, driver);
	              Thread.sleep(10000);
	             
	             //DATA TEAM DOWNLOAD
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'DATA TEAM')]")).click();
	Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconDownload.svg']")).click();
	              
	              // Take Screenshot                   
	          String data_team_page_download=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(data_team_page_download, driver);
	              Thread.sleep(10000);
	              //Commercial
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'COMMERCIAL')]")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconDownload.svg']")).click();
	              
	              // Take Screenshot                   
	          String commercial_page_download=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(commercial_page_download, driver);
	              Thread.sleep(10000);
	              //Supply Chain 
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'SUPPLY CHAIN')]")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconDownload.svg']")).click();
	             
	              // Take Screenshot                   
	          String supply_chain_page_download=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(supply_chain_page_download, driver);
	              Thread.sleep(10000);
	                //View NIS Exports
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//h5[@id='nsfe']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconDownload.svg']")).click();
	             
	              // Take Screenshot                   
	          String view_NIS_page_download=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(view_NIS_page_download, driver);
	              Thread.sleep(10000);
	              //View list of exports
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath(" //h5[@id='vloe']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconDownload.svg']")).click();
	              res=true;
	              
	              // Take Screenshot                   
	          String view_list_exports_page_download=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(view_list_exports_page_download, driver);
	              Thread.sleep(10000);
	            }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Page ", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    
	    //*******************************End Download**********************************************
	    //********************************START PRINT******************************************&*&&
	    
	    public static boolean print(WebDriver driver,WebDriverWait wait, String TC_no, String ResultPath)
	    {
	           
	           
	           boolean res=false;
	           boolean res0=false;
	           boolean res1=false;
	           boolean res2=false;
	           boolean res3=false;
	           boolean res4=false;
	           boolean res5=false;
	           
	           try
	           {
	                  Thread.sleep(3000);
	        	   driver.findElement(By.xpath("//h5[@id='mr']")).click();
	        	  
	                 // Take Screenshot                   
	             String manage_page=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(manage_page, driver);
	              Thread.sleep(5000); 
	              
	              driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[1]/div/div[2]/div/span[2]/img")).click();
	              
	             
	              // Take Screenshot                   
	          String Manage_req_print_page=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(Manage_req_print_page, driver);
	          Thread.sleep(5000);
	              String winHandleBefore = driver.getWindowHandle();

	           for(String winHandle : driver.getWindowHandles()){
	               driver.switchTo().window(winHandle);
	           }
	           String winHandleafter = driver.getWindowHandle();
	           if(!winHandleBefore.equalsIgnoreCase(winHandleafter))
	           {
	        	   res0=true;
	           }
	           
	           // Take Screenshot                   
	           String Manage_req_print_page1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	           TakeScreenShot.saveScreenShot(Manage_req_print_page1, driver);
	          
	           
	           driver.close();

	           driver.switchTo().window(winHandleBefore);


	              Thread.sleep(5000);
	              
	             
	             //DATA TEAM DOWNLOAD
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'DATA TEAM')]")).click();
	             Thread.sleep(3000);
	             
	             driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[1]/div/div[2]/div/span[1]/img")).click();
	             
	             
	             // Take Screenshot                   
	         String data_team_print=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	         TakeScreenShot.saveScreenShot(data_team_print, driver);
	         Thread.sleep(5000);
	             String winHandleBeforedata = driver.getWindowHandle();

	          for(String winHandle : driver.getWindowHandles()){
	              driver.switchTo().window(winHandle);
	          }
	          String winHandleafter1 = driver.getWindowHandle();
	          if(!winHandleBeforedata.equalsIgnoreCase(winHandleafter1))
	          {
	       	   res1=true;
	          }
	          
	          String data_team_page1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(data_team_page1, driver);
	          Thread.sleep(5000);
	          driver.close();

	          // Switch back to original browser (first window)
	          Thread.sleep(5000);
	          driver.switchTo().window(winHandleBeforedata);


	             Thread.sleep(5000);
	            
	              //Commercial
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'COMMERCIAL')]")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[1]/div/div[2]/div/span[1]/img")).click();
	              
	              
	              // Take Screenshot                   
	          String commercial=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(commercial, driver);
	          Thread.sleep(5000);
	              String winHandleBeforecommercial = driver.getWindowHandle();

	           for(String winHandle : driver.getWindowHandles()){
	               driver.switchTo().window(winHandle);
	           }
	           String winHandleafter2 = driver.getWindowHandle();
	           if(!winHandleBeforecommercial.equalsIgnoreCase(winHandleafter2))
	           {
	        	   res2=true;
	           }
	           
	           
	           
	           String commercial_page1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	           TakeScreenShot.saveScreenShot(commercial_page1, driver);
	           
	           driver.close();

	           // Switch back to original browser (first window)
	           Thread.sleep(5000);
	           driver.switchTo().window(winHandleBeforecommercial);


	              Thread.sleep(5000);
	              //Supply Chain 
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//p[@class='black text-center' and contains(text(),'SUPPLY CHAIN')]")).click();
	              Thread.sleep(3000);
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[1]/div/div[2]/div/span[1]/img")).click();
	              
	             
	              // Take Screenshot                   
	          String supply_chain=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(supply_chain, driver);
	          Thread.sleep(5000);
	              String winHandleBeforesupply = driver.getWindowHandle();

	           for(String winHandle : driver.getWindowHandles()){
	               driver.switchTo().window(winHandle);
	           }
	           String winHandleafter3 = driver.getWindowHandle();
	           if(!winHandleBeforesupply.equalsIgnoreCase(winHandleafter3))
	           {
	        	   res3=true;
	           }
	           
	           
	           
	           String supply_chain_page1=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	           TakeScreenShot.saveScreenShot(supply_chain_page1, driver);
	           driver.close();

	           // Switch back to original browser (first window)
	           Thread.sleep(5000);
	           driver.switchTo().window(winHandleBeforesupply);


	              Thread.sleep(5000);
	                //View NIS Exports
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//h5[@id='nsfe']")).click();
	              Thread.sleep(3000);
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[1]/div/div[2]/div/span[1]/img")).click();
	              
	              
	              // Take Screenshot                   
	          String nis_export=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(nis_export, driver);
	          Thread.sleep(5000);
	              String winHandleBeforenis = driver.getWindowHandle();

	           for(String winHandle : driver.getWindowHandles()){
	               driver.switchTo().window(winHandle);
	           }
	           
	           String winHandleafter4 = driver.getWindowHandle();
	           if(!winHandleBeforenis.equalsIgnoreCase(winHandleafter4))
	           {
	        	   res4=true;
	           }
	           String NIS_EXPORTS=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	           TakeScreenShot.saveScreenShot(NIS_EXPORTS, driver);
	           driver.close();

	           // Switch back to original browser (first window)
	           Thread.sleep(5000);
	           driver.switchTo().window(winHandleBeforenis);


	              Thread.sleep(5000);
	              
	              
	              //View list of exports
	              Thread.sleep(3000);
	              driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
	              Thread.sleep(3000);
	              driver.findElement(By.xpath(" //h5[@id='vloe']")).click();
	              Thread.sleep(3000);
	              
	              
	             driver.findElement(By.xpath("/html/body/my-app/div/ng-component/div[3]/div[1]/div/div/span[1]/img")).click();
	              
	              
	              // Take Screenshot                   
	          String export=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	          TakeScreenShot.saveScreenShot(export, driver);
	          Thread.sleep(5000);
	              String winHandleBeforeexport = driver.getWindowHandle();

	           for(String winHandle : driver.getWindowHandles()){
	               driver.switchTo().window(winHandle);
	           }
	           String winHandleafter5 = driver.getWindowHandle();
	           if(!winHandleBeforeexport.equalsIgnoreCase(winHandleafter5))
	           {
	        	   res5=true;
	           }
	          
	           String view_exports=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	           TakeScreenShot.saveScreenShot(view_exports, driver);
	           driver.close();

	           // Switch back to original browser (first window)
	           Thread.sleep(5000);
	           driver.switchTo().window(winHandleBeforeexport);


	              Thread.sleep(5000);
	              
	              res=res0&&res1&&res2&&res3&&res4&&res5;
	               
	              return res;
	            }
	           catch(Exception e)
	           {
	                  res=false;  
	                  System.out.println(e);
	                  utilityFileWriteOP.writeToLog(TC_no, "Page ", "Not Found!!!",ResultPath);
	                  
	                  // Take Screenshot                   
	             String datateam=ResultPath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";                      
	             TakeScreenShot.saveScreenShot(datateam, driver);
	             return res;
	           }
	           finally
	           {
	                  return res;
	           }
	    }
	    //**************************************END PRINT

	  }


