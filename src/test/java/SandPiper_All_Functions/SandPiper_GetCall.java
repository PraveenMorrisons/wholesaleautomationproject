package SandPiper_All_Functions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;




import org.apache.poi.xwpf.usermodel.XWPFRun;

import SandPiperUtilities.*;
import SandPiper_All_Functions.*;
import SandPiperUtilities.utilityFileWriteOP;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class SandPiper_GetCall {
	
	public static boolean GetCallItemStatusValidation_Invalid(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		
		System.out.println(statusCurrentValue);
		
		String [] shipStatusArrey=statusCurrentValue.split(",");
		
		statusCurrentValue=shipStatusArrey[0];
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 9) {
		        	res = true;
		        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 9");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
					
				break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
		        	
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	public static String GetCallOrderOpsFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String StoreId, String ReferenceCode, String tcid, String Resultpath,XWPFRun xwpfRun) throws IOException {
		
		String res = null;
		
		String lookupsJsonvalue = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/stores/items/" + StoreId + "?" + ApiKey+"&filtertype=maptype&filtervalue=CLIENTID");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Store ID is " + StoreId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "StoreId "+StoreId,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        
	        
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "Order OPS -> Webportal Response Body is ",jsonobject.toString(),Resultpath,xwpfRun,"");
	        
	        
	        
	        
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
			    
			    
			    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
			    	
			    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
		        	
		        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
			        
		        	if(lookupsJsonKey.equals("ORDER-OPS-"+ReferenceCode)) {
		        		
		        		
		        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
			        		
					        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

					        lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}
		        	
	 	        		
		        	}
		    	
			    }
		   
			   	     
			    res = lookupsJsonvalue;
			    
			    
			    utilityFileWriteOP.writeToLog(tcid, "Order OPS value  in Webportal for  the store is  "+StoreId, res);
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}

	
	
	
	public static String GetCallStoreAddressFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String StoreId, String tcid, String Resultpath,XWPFRun xwpfRun) throws IOException {
		
		String res = null;
		
		String storeAddress = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/stores/items/" + StoreId + "?" + ApiKey+"&filtertype=maptype&filtervalue=CLIENTID");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Store ID is " + StoreId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "StoreId "+StoreId,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ",jsonresponse,Resultpath,xwpfRun,"");
	        
	        
	        
	        
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
		        
		        storeAddress = description.split(":")[1].trim(); 
	        	
			   
			   	     
			    res = storeAddress;
			    
			    
			    
			    
			    utilityFileWriteOP.writeToLog(tcid, "StoreName in WebPortal "+res,"",Resultpath,xwpfRun,"");
		        
		        
			    
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
	
public static String GetCallItemAvailabilityFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath,XWPFRun xwpfRun) throws IOException {
		
		String res = null;
		
		String ItemAvailability = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Item ID is " + ItemId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemId "+ItemId,Resultpath,xwpfRun, "");
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath,xwpfRun, "");
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath, xwpfRun, "");
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath, xwpfRun, "");
	        
	      
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ",jsonresponse,Resultpath,xwpfRun,"");
	        
	        
	        
	        
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
	        	ItemAvailability = jsonobject.get("availability").toString().replaceAll("^\"|\"$", "");
		        
		       
	        	
			   
			   	     
			    res = ItemAvailability;
			    
			    
			    
			    
			    utilityFileWriteOP.writeToLog(tcid, "ItemAvailability in WebPortal "+res,"",Resultpath,xwpfRun,"");
		        
		        
			    
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath, xwpfRun, "");
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
	
	public static String GetCallWSCPorWSPFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemID, String quantityType, String tcid, String Resultpath) throws IOException {
		
		String res = null;
		
		String pricesJsonvalue = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemID + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("ItemID ID is " + ItemID);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemID "+ItemID,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
			    
			    
			    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
			    	
			    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
		        	
		        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
			        
		        	if(quantityType.equals("CS")) {
		        		
		        		if(pricesJsonKey.equals("WSCP")) {
		        			
		        			
		        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
				        		
						        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

						        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
						     
				        	}
		        			
		        			
		        		}
		        		
		        	}
		        	
		        	if(quantityType.equals("EA")) {
		        		
		        		if(pricesJsonKey.equals("WSP")) {
		        			
		        			
		        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
				        		
						        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

						        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
						     
				        	}
		        			
		        			
		        		}
		        		
		        	}
		        	
		        	/*if(pricesJsonKey.equals("ORDER-OPS")) {
		        		
		        		
		        		for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}
		        	
	 	        		
		        	}*/
		    	
			    }
		   
			   	     
			    res = pricesJsonvalue;
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
	
public static String GetCallVatRateFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemID, String quantityType, String tcid, String Resultpath) throws IOException {
		
		String res = null;
		
		String pricesJsonvalue = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemID + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("ItemID ID is " + ItemID);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemID "+ItemID,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
			    
			    
			    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
			    	
			    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
		        	
		        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
			        
		        
		        		
		        		if(pricesJsonKey.equals("VAT")) {
		        			
		        			
		        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
				        		
						        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

						        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
						     
				        	}
		        			
		        			
		        		}
		        		
	        	
		        	
		    	
			    }
		   
			   	     
			    res = pricesJsonvalue;
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
	
	
	
public static String GetCallSTATUSFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemID, String tcid, String Resultpath, XWPFRun xwpfrun) throws IOException {
		
		String res = null;
		
		String lookupsJsonvalue = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemID + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("ItemID is " + ItemID);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemID "+ItemID,Resultpath, xwpfrun, "");
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath, xwpfrun, "");
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath, xwpfrun, "");
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath, xwpfrun, "");
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath, xwpfrun, "");
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
			    
			    
			    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
			    	
			    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
		        	
		        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
			        
		        	
		        		
		        		if(lookupsJsonKey.equals("STATUS")) {
		        			
		        			
		        			for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
				        		
						        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

						        lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
						     
				        	}
		        			
		        			
		        		}
		        		
		        
		        	
		        	
		    	
			    }
		   
			   	     
			    res = lookupsJsonvalue;
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath, xwpfrun, "");
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}



public static String GetCallAllocationPriorityFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String StoreID, String tcid, String Resultpath, XWPFRun xwpfrun) throws IOException {
	
	String res = null;
	
	String lookupsJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/stores/items/" + StoreID + "?" + ApiKey+"&filtertype=maptype&filtervalue=CLIENTID");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("StoreID is " + StoreID);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "StoreID "+StoreID,Resultpath, xwpfrun, "");
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath, xwpfrun, "");
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath, xwpfrun, "");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath, xwpfrun, "");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath, xwpfrun, "");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	
	        
		    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
		    
		    
		    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
		    	
		    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
	        	
	        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
		        
	        	
	        		
	        		if(lookupsJsonKey.equals("ALLOCATION_PRIORITY")) {
	        			
	        			
	        			for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
			        		
					        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

					        lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}
	        			
	        			
	        		}
	        		
	        
	        	
	        	
	    	
		    }
	   
		   	     
		    res = lookupsJsonvalue;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath, xwpfrun, "");
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}
	
	
	
	public static String GetOrderQuantityType_WebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath) throws IOException {
		
		String res = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	    
	    
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Item ID is " + ItemId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
//	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	       
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
			    
			    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
			    ArrayList<String> listoflookupkeys = new ArrayList<String>();
			    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();

			    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

			    
			    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
			    	
			    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
		        	
		        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
			        
		        	if(lookupsJsonKey.equals("SINGLE-PICK")) {
		        		
		        		
		        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
			        		
					        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

					        String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
					       System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
					        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
					    	
					    	long startdatemilli = date.getTime();
					        
					        mapofquantityType.put(lookupsJsonvalue+String.valueOf(lookupsBodyindex), startdatemilli);
			        		
			        	}
		        	
	 	        		
		        	}
		    	
			    }
			    System.out.println("mapofquantityType.size() "+mapofquantityType.size());
			    for(String str: mapofquantityType.keySet()) {
			    	
			    	listoflookupkeys.add(str);
			    	listoflookupstartvalues.add(mapofquantityType.get(str));
			    }

			    long latestdatemilli = 0; 
			    
			    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
			    			    	
			    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
			    		
			    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
			    		
			    	}
			    	
			    }
			    
			    String finalSinglePickkey = null;
			    String quantityTypeFromWebPortal = null;
			    
			    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
			    	
			    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
			    		
			    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
			    	}
			    	
			    }
			    
			    System.out.println("latest date in milli "+latestdatemilli);
			    
			    if(finalSinglePickkey.contains("Y")) {
			    	
			    	quantityTypeFromWebPortal = "EA";
			    }
			    
			    if(finalSinglePickkey.contains("N")) {
			    	
			    	quantityTypeFromWebPortal = "CS";
			    }
			    
			    
			    //String itemDetailsbody = "{\"quantityType\": \"" + quantityTypeFromWebPortal + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
			    	    
			    //System.out.println(itemDetailsbody);	    
			    	     
			    res = quantityTypeFromWebPortal;
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
		
		boolean res = false;
		
		
		String [] shipStatusArrey=statusCurrentValue.split(",");
		
		statusCurrentValue=shipStatusArrey[0];

		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        
	        
	        for (int j=0;j<5;j++){
	        
	        
	       Thread.sleep(5000);
	        	
	        	
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

	        response = httpclient.execute(target, httpget);

	        
	        
	      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        
	        
	        System.out.println(jsonresponse);
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("orders");
	        
	        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
	       
	        
	     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
	      //  System.out.println(jarray.size());
	     //   for(int i=0; i<jarray.size(); i++) {
	        
	      //  System.out.println(jsonobject.toString());
	        
	        
	        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
	        
	        
	    //  System.out.println(jarray);
	       
	    
		        
	        	
	        //String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        //System.out.println("StatusValidation Result is " + statusValidationResult);

		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent, "Success",ResultPath,xwpfrun,"");
					
					
					System.out.println("Orders statusCurrent is " +statusCurrent);
					
				     break;
				
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent,"FAIL",ResultPath,xwpfrun,"");
					System.out.println("statusCurrent is " +statusCurrent);
					
					continue;
		        	
		        }
	        	
	       
	        }
	        
	      
	    	
		}  //end of try
	    
	    
	    
	    catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
			//return res;
		}
	    
	    finally{
	        
	     response.close();
	     
	     return res;
	        
	}
	    
	    
	    
		
	}
	
	
	
	
	
	
public static String  GetOrderOPS(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
	
	String OPS=null;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
 
       Thread.sleep(10000);
        	
        	
      //  System.out.println("Order ID is " + OrderID);
      //  utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
      //  System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       // utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
       String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
        
        OPS = jsonobject1.get("orderOPS").toString().replaceAll("^\"|\"$", "");
        
        
        
        utilityFileWriteOP.writeToLog(tcid, "Order OPS value  for order "+OrderID+"is ", OPS);
        
        
       
        
     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
      //  System.out.println(jarray.size());
     //   for(int i=0; i<jarray.size(); i++) {
        
      //  System.out.println(jsonobject.toString());
        
        
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        
    //  System.out.println(jarray);
       
    
	        
        	
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	       

       
      
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return OPS;
        
}
    
    
    
	
}


public static String  GetOrderStoreName(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
	
String OPS=null;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
 
       Thread.sleep(10000);
        	
        	
      //  System.out.println("Order ID is " + OrderID);
      //  utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
      //  System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       // utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
       String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
        
        OPS = jsonobject1.get("storeName").toString().replaceAll("^\"|\"$", "");
        
        
        
        utilityFileWriteOP.writeToLog(tcid, "Order StoreName value  for order "+OrderID+"is ", OPS);
        
        utilityFileWriteOP.writeToLog(tcid, "StoreName in OrderDetails is  "+OPS,"",ResultPath,xwpfrun,"");
         
       
        
     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
      //  System.out.println(jarray.size());
     //   for(int i=0; i<jarray.size(); i++) {
        
      //  System.out.println(jsonobject.toString());
        
        
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        
    //  System.out.println(jarray);
       
    
	        
        	
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	       

       
      
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return OPS;
        
}
    
    
    
    
	
}

	
	
	
	
public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        
	        int stCnt=jarray.size();
	        
	        int mtchCnt=0;
	        
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        logger.log(LogStatus.PASS, "StatusValidation Result is " + statusValidationResult);
		        
		        
		        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
		        
		        
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = true;
		        	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 10");
					logger.log(LogStatus.PASS, "Order Id :"+OrderID);
					logger.log(LogStatus.PASS, "Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        
		        }
		        else {
		        	
		        	res = false;
					
		        	
		        	utilityFileWriteOP.writeToLog(tcid, "Item StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	//break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "Item StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("Item statusCurrent is " +statusCurrent);
					
					mtchCnt=mtchCnt+1;
					
					
				//break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "Item StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					
					
					System.out.println("Item statusCurrent is " +statusCurrent);
		        	
		        }
	        	
	        }
	        	  
	       
	        if(mtchCnt==stCnt) {
	        	break;
	        
	        }
	        
	      }
	    	
} 
	    
	    
catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}
	
	

public static String GetCallAllocationPriorityFromPostMan(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	String res = null;

	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
   
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        

        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        
	        String Allocation_Priority = jsonobject.get("allocationPriority").toString().replaceAll("^\"|\"$", "");
	        
	        
       
	        utilityFileWriteOP.writeToLog(tcid, "Allocation_Priority from Postman is ",Allocation_Priority,ResultPath,xwpfrun,"");
	        
	     res = Allocation_Priority;
        	
        }
        	  
        
      }
 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e, ResultPath, xwpfrun, "");
        
		res = null;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
    }

}





public static HashMap<String, String> GetCallItemStatusValidation_PriceConfirmedAmount(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	HashMap<String, String> PriceConFirmedAmt = new HashMap<String, String>();
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
       
        
        System.out.println(jarray.size());
 for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	        
	        
	      //  String ItemId=jsonobject.get("itemID").toString().replaceAll("^\"|\"$", "");
	        
	        String PriceConfirmedAmt = jsonobject.get("priceConfirmedAmount").toString().replaceAll("^\"|\"$", "");
	        
	        
	        
	        PriceConFirmedAmt.put(ItemIDs,PriceConfirmedAmt);

	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	        

        	
        }
  
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
		
		
	}
    
    finally{
        
        response.close();
        
        return PriceConFirmedAmt;
        
    }

}



public static HashMap<String, String> GetCallItemStatusValidation_PriceConfirmedTaxRate(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	HashMap<String, String> PriceConFirmedTax = new HashMap<String, String>();
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
       
        
        System.out.println(jarray.size());
 for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	        
	        
	      //  String ItemId=jsonobject.get("itemID").toString().replaceAll("^\"|\"$", "");
	        
	        String PriceConfirmedTaxRate = jsonobject.get("priceConfirmedTaxRate").toString().replaceAll("^\"|\"$", "");
	        
	        
	        
	        PriceConFirmedTax.put(ItemIDs,PriceConfirmedTaxRate);

	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	        

        	
        }
  
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
		
		
	}
    
    finally{
        
        response.close();
        
        return PriceConFirmedTax;
        
    }

}



public static boolean GetCallItemStatusValidation_ShipToLocationWithPrefix(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String shipToLocationId, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	//HashMap<String, String> PriceConFirmedAmt = new HashMap<String, String>();
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
      
       
        
        System.out.println(jarray.size());
 
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	       // String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");

	      //  String ItemId=jsonobject.get("itemID").toString().replaceAll("^\"|\"$", "");
	        
	        String shipToLocationWithPrefix = jsonobject.get("shipToLocationWithPrefix").toString().replaceAll("^\"|\"$", "");
	        
	       if(shipToLocationWithPrefix.equals("SPR"+shipToLocationId)) {
	        	
	        	res = res && true;
	 	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+" : shipToLocationWithPrefix from Postman is "+shipToLocationWithPrefix+ ", Expected value is "+"SPR"+shipToLocationId," ....   Matched",ResultPath,xwpfrun,"");

	        	
	        }
	        else {
	        	
	        	res = res && false;
		 	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+" : shipToLocationWithPrefix from Postman is "+shipToLocationWithPrefix+ ", Expected value is "+"SPR"+shipToLocationId," ....   Did not Match. FAIL",ResultPath,xwpfrun,"");

	        }
	   
       	
        }
  
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
    }

}








public static boolean GetCallItemStatusValidation_InvalidCase_InvalidItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	 boolean res = true;
	
	
	ItemIDDs.split(",");
	
	
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
        System.out.println(jarray.size());
        
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	       if(i==0)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		        	
		
				    System.out.println("Count of 1 is 10");
		       
		           
		        }
		       
		        
		       else {
		        	
		        	
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

	               }        		

	         }
		    
		    
		    
		    
	if(i==1)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		
				 System.out.println("Count of 1 is 10");
		       
		        
		        }
		       
		        
		       else {
		       
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);

	            }        		

	 }
		 
	 

	   }

	   
	    	
	} 
	    
	    
	catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}



public static boolean GetCallItemStatusValidation_InvalidLocation_validItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	
	ItemIDDs.split(",");
	
	
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	        
	    if(i==0)  { 
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = false;
	        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
	        	
	
			    System.out.println("Count of 1 is 10");
	       
	           
	        }
	       
	        
	       else {
	        	
	        	
	    	   res=res&&true;
	    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

               }        		

         }
	    
	    
	    
	    
if(i==1)  { 
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = false;
	        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
	
			    System.out.println("Count of 1 is 10");
	       
	        
	        }
	       
	        
	       else {
	       
	    	   res=res&&true;
	    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);

            }        		

 }
	 
 

   }

   
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
    }

}

















public static boolean GetCallItemStatusValidation_InvalidDateAndValidItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	
	ItemIDDs.split(",");
	
	
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
        System.out.println(jarray.size());
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	       if(i==0)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		        	
		
				    System.out.println("Count of 1 is 10");
		       
		           
		        }
		       
		        
		       else {
		        	
		        	
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

	               }        		

	         }
		    
		    
		    
		    
	if(i==1)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		
				    System.out.println("Count of 1 is 10");
		       
		        
		        }
		       
		        
		       else {
		       
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);

	            }        		

	 }
		 
	 

	   }

	   
	    	
	} 
	    
	    
	catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}



public static boolean GetCallItemStatusValidation_InvalidRefCodeAndValidItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	
	ItemIDDs.split(",");
	
	
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
        System.out.println(jarray.size());
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	       if(i==0)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValidation is not as expected ", "for Item ID:"+ItemIDs);
		        	
		
				    System.out.println("Count of 1 is 10");
		       
		           
		        }
		       
		        
		       else {
		        	
		        	
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

	               }        		

	         }
		    
		    
		    
		    
	if(i==1)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValidation is not as expected ", "for Item ID:"+ItemIDs);
		
				    System.out.println("Count of 1 is 10");
		       
		        
		        }
		       
		        
		       else {
		       
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValidation is as expected ", "for Item ID:"+ItemIDs);

	            }        		

	 }
		 
	 

	   }

	   
	    	
	} 
	    
	    
	catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}



public static boolean GetCallItemStatusValidation_InvalidAvailabilityAndValidItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	
	ItemIDDs.split(",");
	
	
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
        System.out.println(jarray.size());
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	       if(i==0)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValidation is not as expected ", "for Item ID:"+ItemIDs);
		        	
		
				    System.out.println("Count of 1 is 10");
		       
		           
		        }
		       
		        
		       else {
		        	
		        	
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

	               }        		

	         }
		    
		    
		    
		    
	if(i==1)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValidation is not as expected ", "for Item ID:"+ItemIDs);
		
				    System.out.println("Count of 1 is 10");
		       
		        
		        }
		       
		        
		       else {
		       
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValidation is as expected ", "for Item ID:"+ItemIDs);

	            }        		

	 }
		 
	 

	   }

	   
	    	
	} 
	    
	    
	catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}



public static boolean GetCallItemStatusValidation_negativeScenarios(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	      /*  if(countOne == 9) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is 9");
	        }
	        else {
	        	
	        	res = false;
				
	        	
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//break;
	        }*/
	        System.out.println(countOne);
	        System.out.println(statusCurrent);
	        System.out.println(statusCurrentValue);
	        
	        if( (statusCurrent.equals(statusCurrentValue)) && (countOne != 9)) {
	        	res = true;
	        	
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation is "+statusValidationResult.replaceAll("^\"|\"$", ""), "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");

				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValidation is "+statusValidationResult.replaceAll("^\"|\"$", ""), "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");

				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
        	
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}


	
	
	
public static boolean GetCallItemStatusValidation_InvalidItems(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
	
	boolean res1 = false;
	
	String [] StatusValues=statusCurrentValue.split(",");
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
        
         	
        Thread.sleep(10);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
       
        
        for(int i=0; i<jarray.size(); i++) {
        	
        statusCurrentValue=StatusValues[i+1];
        
        
     
        		 jsonobject = jarray.get(i).getAsJsonObject();
        		
        		
        	//	String itemLineId= jsonobject.get("itemLineId").toString().replaceAll("^\"|\"$", "");

        		   String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");

        		   if(statusCurrent.equals(statusCurrentValue)) {
        		        	
        			   res1=true;
        					
        			   
        			   utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
        					
        					
        					
        					System.out.println("statusCurrent is " +statusCurrent);
        					
        				//continue;
        				
        				
        		        }
        		
        		        
        		        
        		       
        		        else{
        		        	
        		        	res1=false;
        		        	
        		        	
        		        	
        					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
        					System.out.println("statusCurrent is " +statusCurrent);

        		        }

        }

    	
	} catch (Exception e) {
		// TODO: handle exception
		res1=false;
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
       
	}
    
    finally{
        
        response.close();
        
        return res1;
        
}
    
  
    
	
}


	
	
	
	
	


	
public static boolean GetCallItemStatusValidationWithInvalidItems(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID,String statusCurrentValue, String ItemStatus, String tcid,String ResultPath,XWPFRun xwpfRun) throws IOException {
	
	boolean res = true;
	
	int p=0;
	
	
	 HashMap<String, String> hm = null;
	 HashMap<String, String> hm1 = null;
	 
	 HashMap<String, String> ValidStatusCnt = null;
	 HashMap<String, String> InvalidValidStatusCnt = null;
	 
	 
	 HashMap<String, String> ValidStatusCntST_StringList = null;
	 HashMap<String, String> InvalidValidStatusCnt_StringList = null;
	 
	 
	 
	 
	 
		
     hm = new HashMap<String, String>();
     
     hm1 = new HashMap<String, String>();
     
     ValidStatusCnt = new HashMap<String, String>();
     
     InvalidValidStatusCnt=new HashMap<String, String>();
     
     
	 ValidStatusCntST_StringList = new HashMap<String, String>();
	InvalidValidStatusCnt_StringList = new HashMap<String, String>();
     
     
     
     
     
	
	int validExpectedCount=0;
	int invalidExpectedCount=0;
	
	String[] ItemsStatusInput=ItemStatus.split(",");
	
	for(int u=0;u<ItemsStatusInput.length;u++){
	
	
	  if(ItemsStatusInput[u].contains("P")){
	
		
		  validExpectedCount=validExpectedCount+1;
		  
		  
	  }
	  
	  
	  else  {
		  
		  
		  invalidExpectedCount=invalidExpectedCount+1;
		  
		  
		  
	  }
		  
		  
		
	}
		
		
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfRun,"");
        
       // for(int j =1; j<= 10; j ++) {
        	
        	
        Thread.sleep(10000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
      
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfRun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfRun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
      
        
        //System.out.println(jsonobject.toString());
       
        
        
     //   utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        jarray.size();
        
        
     //   System.out.println("Json Array Size  for get call is "+stCnt);
        
        
        System.out.println(jarray.size());
       
        String statusCurrent =null;
        String CurrentItemID=null;
        
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        CurrentItemID = jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);

	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result for Item  ::  "+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "")+" IS ..", statusValidationResult,ResultPath,xwpfRun,"");
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result for Item  ::  "+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "")+" IS ..", statusValidationResult,ResultPath);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	if(countOne == 9) {
	        	res = true;
	        	//System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfRun,"");
				
	        	ValidStatusCntST_StringList.put(String.valueOf(i),statusValidationResult);
	        	
	        	//System.out.println("Count of 1 is 9");
				
				
	  hm1.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status Validation Result  is "  +statusValidationResult);
				
	 // utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result  is "  +statusValidationResult, "",ResultPath);		
				
	       
	
	}
	       
	        
	else {
	        	
	        	//res = false;
		
		InvalidValidStatusCnt_StringList.put(String.valueOf(i),statusValidationResult);
				
		        hm1.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status Validation Result  is "  +statusValidationResult);
	        	//utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfRun,"");

		      //  utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusValidationResult, "",ResultPath);		
				
		        
		        
	        	//break;
	        }
	        
	        
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	
	        	// ItemsStatus[p]=statusCurrent;
	        	 
	        	 ValidStatusCnt.put(String.valueOf(i), statusCurrent);
	        	 
	        	 
	        	 hm.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status is "  +statusCurrent);
	        	
	        	 p=p+1;
	        	
	        	// utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusCurrent, "",ResultPath);		
					
	        	 
	        	 
	        	 
	        	 
	        	
	        	//res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent+" for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),"",ResultPath,xwpfRun,"");
				
				 hm.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status is "  +statusCurrent);
				
				System.out.println("statusCurrent is " +statusCurrent+"for "+CurrentItemID);
				
				
		      //	 utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusCurrent, "",ResultPath);	
				
				//mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	
	        	
	    //ItemsStatus[p]=statusCurrent;
	    
	    InvalidValidStatusCnt.put(String.valueOf(i), statusCurrent);
	        	
	        	
      utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent+" for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),"",ResultPath,xwpfRun,"");
				
	   System.out.println("statusCurrent is " +statusCurrent+"for "+CurrentItemID);
	   
	   hm.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status is "  +statusCurrent);
	   
	 //  utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusCurrent, "",ResultPath);		
		
	        	
	
				
	//System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
        	
      }
        
        
  //  utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent+" for Item ID:"+CurrentItemID,"",ResultPath,xwpfRun,"");
		    

    
    
    
  for(int cnt=0;cnt<hm.size();cnt++){
    
    hm.get(String.valueOf(cnt));
    	
    //utilityFileWriteOP.writeToLog(tcid, status,"",ResultPath,xwpfRun,"");

    }
    
  for(int cnt1=0;cnt1<hm1.size();cnt1++){
	    
	    hm.get(String.valueOf(cnt1));
	    	
	   // utilityFileWriteOP.writeToLog(tcid, status,"",ResultPath,xwpfRun,"");

	 } 
  

  
  if(validExpectedCount==ValidStatusCnt.size()){

	// utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all valid items " ,"",ResultPath,xwpfRun,"");

	 res=res&&true;
	 
	//  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all valid items ", "",ResultPath);		
		

   } 
  
  
  else{
  
	  
	 // utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all valid items " ,"",ResultPath,xwpfRun,"");
	  res=res&&false;
	  
	 // utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all valid items ", "",ResultPath);		
  
  
  
  }
  
  
  
  if(invalidExpectedCount==InvalidValidStatusCnt.size()){

		// utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all invalid items " ,"",ResultPath,xwpfRun,"");

		 res=res&&true;
		 
		 
		// utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all invalid items ", "",ResultPath);		
			
		 
  
  } 
	  
	  
else{
	  
		  
     //  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all invalid items " ,"",ResultPath,xwpfRun,"");

       res=res&&false;
       
     //  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all invalid items ", "",ResultPath);		
		
       
       

	  }

  
/*
  ValidStatusCntST_StringList = new HashMap<String, String>();
  InvalidValidStatusCnt_StringList = new HashMap<String, String>();
  
  
  
  if(validExpectedCount==ValidStatusCntST_StringList.size()){

		 utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all valid items " ,"",ResultPath,xwpfRun,"");

		 res=res&&true;
		 
		 
		 utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation is success for all valid items ", "",ResultPath);		
			
		 
	   } 
	  
	  
	  else{
	  
		  
		  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all valid items " ,"",ResultPath,xwpfRun,"");
		  res=res&&false;
		  
		  
		  utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation not  success for all valid items ", "",ResultPath);		
			
	  
	  }
  
  
  if(invalidExpectedCount==InvalidValidStatusCnt_StringList.size()){

		 utilityFileWriteOP.writeToLog(tcid, "Item Status  validation is success for all invalid items " ,"",ResultPath,xwpfRun,"");

		 res=res&&true;
		 
		 utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation is success for all invalid items ", "",ResultPath);		
			
		 
		 

} 
	  
	  
else{
	  
		  
    utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all invalid items " ,"",ResultPath,xwpfRun,"");

    utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation not success for all invalid items ", "",ResultPath);		
	
    
    
    res=res&&false;

	  }
  
  */
  

        
 //  }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		
		
	//	utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+ItemsStatus.toString(),"",ResultPath,xwpfRun,"");
		
		
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath,xwpfRun,"");
        
	//	res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
   }

}

public static String GetCallItemDetailsFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);

		        	
		        }

	        	
	        }
	        
	       	//fetching  priceOrderedCurrency
	        
	        String PriceOrderedAmount = null;
	        
	        switch (quantityType) {
			case "CS":
				PriceOrderedAmount = mapofPrices.get("WSCP");
				break;
				
			case "EA":
				PriceOrderedAmount = mapofPrices.get("WSP");
				break;

			default:
				break;
			}
	        
	        
		    
		    System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + PriceOrderedAmount + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
			
			
        	
        }
        	  
       
        
       // }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}
////////////////////////////////////i13//////////////////////////////////////////////////

public static boolean GetAllItems_StatusValidation(String TestDataPath,String SheetName,Integer r,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue,String AllQuantitiesOrdered,String ItemID,String status, String tcid, String ResultPath, XWPFRun xwpfrun) throws IOException {
	
	
	boolean res = false;
					
	String item[] = ItemID.split(",");
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath, xwpfrun, "");
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath, xwpfrun, "");
     	Thread.sleep(10000);

        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath, xwpfrun, "");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath, xwpfrun, "");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
		utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
        utilityFileWriteOP.writeToLog(tcid,jsonobject.toString(),"",ResultPath, xwpfrun, "");

        utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
       System.out.println("Got "+AllQuantitiesOrdered);
        
        Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
		
		Sheet sheet1 = wrk1.getSheet(SheetName);
		
		
		AllQuantitiesOrdered = sheet1.getCell(33, r).getContents().trim();
			
			String[] QuantityOrdered = AllQuantitiesOrdered.split(",");
		
        System.out.println("And at last "+AllQuantitiesOrdered);
        	        	
        for(int i=0; i<jarray.size(); i++) {
        	
        
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String item_serv = jsonobject.get("itemId").toString();
        	
        	for (int j = 0; j < item.length; j++) 
        	{
				
		
        	
        	if (item_serv.substring(1, item_serv.length()-1).equals(item[j])) 
        	{
				
        		
	        		        		        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        String qtyConfirm = jsonobject.get("quantityConfirmed").toString();
	        
	       
	         String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	         System.out.println("StatusValidation Result is " + statusValidationResult);
	         
	         utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is", statusValidationResult,ResultPath, xwpfrun, "");
	         
	         	utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
				utilityFileWriteOP.writeToLog(tcid,"StatusValiadtion is: "+statusValidationResult,ResultPath);
				
	        
	        statusValidationResult.split("1");
	        
	      /*  if(countOne == 10) {
	        	res = true;
	        	System.out.println("Success for Item ID: "+item_serv);
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath, xwpfrun, "");
				System.out.println("Count of 1 is 7");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID: "+item_serv,ResultPath, xwpfrun, "");

	        	break;
	        }
	        */
	        if(statusCurrent.equals(status)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID: "+item_serv,ResultPath, xwpfrun, "");
				System.out.println("statusCurrent is " +statusCurrent);
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID: "+item_serv,ResultPath, xwpfrun, "");
				System.out.println("statusCurrent is not " +statusCurrent);
	        	//break;
	        }
	        
	        System.out.println(qtyConfirm+" "+QuantityOrdered[j].trim());
	        if(qtyConfirm.equals(QuantityOrdered[j].trim())) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Success for Item ID: "+item_serv,ResultPath, xwpfrun, "");
				System.out.println("Quantity Confirmed is " +qtyConfirm);
	        }
	        
	        else{
	        	res = false;
	        	utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Failure for Item ID: "+item_serv,ResultPath, xwpfrun, "");
				System.out.println("Quantity Confirmed is not " +qtyConfirm+","+QuantityOrdered[j]);
				//break;
	        }
	       
        	}
        	
        	
        }
        	
        
        }
        
      
    
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "FAIL",ResultPath, xwpfrun, "");
        
		res = false;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



public static boolean GetAllItems_StatusValidation_negative(String TestDataPath,String SheetName,Integer r,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue,String AllQuantitiesOrdered,String ItemID,String status, String tcid, String ResultPath, XWPFRun xwpfrun) throws IOException {

boolean res = false;
				
String item[] = ItemID.split(",");



CloseableHttpResponse response=null;

CredentialsProvider credsProvider = new BasicCredentialsProvider();

credsProvider.setCredentials(
		new AuthScope(ProxyHostName, ProxyPort),
		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

try {
	
	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
    HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
    
    RequestConfig config = RequestConfig.custom()
            .setProxy(proxy)
            .build();
    
    HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
    //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
    httpget.setConfig(config);
    
    httpget.addHeader(AuthorizationKey, AuthorizationValue);
    
    System.out.println("Order ID is " + OrderID);
    utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath, xwpfrun, "");
    
    
    System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
    utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath, xwpfrun, "");
 	Thread.sleep(10000);

    response = httpclient.execute(target, httpget);
    

    System.out.println("Response code is "+response.getStatusLine().getStatusCode());
    utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath, xwpfrun, "");
    
    String jsonresponse = EntityUtils.toString(response.getEntity());
    
    
    System.out.println(jsonresponse);
    utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath, xwpfrun, "");
    
  
    
    JsonElement jsonelement = new JsonParser().parse(jsonresponse);
    JsonObject jsonobject = jsonelement.getAsJsonObject();
    
    System.out.println(jsonobject.toString());
    //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
	utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid,jsonobject.toString(),"",ResultPath, xwpfrun, "");

    utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),ResultPath);
    
    JsonArray jarray = jsonobject.getAsJsonArray("items");
    System.out.println(jarray.size());
   System.out.println("Got "+AllQuantitiesOrdered);
    
    Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	
	Sheet sheet1 = wrk1.getSheet(SheetName);
	
	
	AllQuantitiesOrdered = sheet1.getCell(33, r).getContents().trim();
		
		//String[] QuantityOrdered = AllQuantitiesOrdered.split(",");
	
    System.out.println("And at last "+AllQuantitiesOrdered);
    	        	
    for(int i=0; i<jarray.size(); i++) {
    	
    
    	jsonobject = jarray.get(i).getAsJsonObject();
    	
    	String item_serv = jsonobject.get("itemId").toString();
    	
    	for (int j = 0; j < item.length; j++) 
    	{
			
	
    	
    	if (item_serv.substring(1, item_serv.length()-1).equals(item[j])) 
    	{
			
    		
        		        		        
        //String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
        
        //String qtyConfirm = jsonobject.get("quantityConfirmed").toString();
        
       
         String statusValidationResult = jsonobject.get("statusValidation").toString();
        
         System.out.println("StatusValidation Result is " + statusValidationResult);
         
         utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is", statusValidationResult,ResultPath, xwpfrun, "");
         
         	utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
			utilityFileWriteOP.writeToLog(tcid,"StatusValiadtion is: "+statusValidationResult,ResultPath);
			
        
        int countOne = statusValidationResult.split("1").length - 1;
        
        if(countOne != 10) {
        	res = true;
        	System.out.println("Success for Item ID: "+item_serv);
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath, xwpfrun, "");
        }
        else {
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID: "+item_serv,ResultPath, xwpfrun, "");

        	break;
        }
        
      /*  if(statusCurrent.equals(status)) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID: "+item_serv,ResultPath, xwpfrun, "");
			System.out.println("statusCurrent is " +statusCurrent);
        }
        
        else{
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID: "+item_serv,ResultPath, xwpfrun, "");
			System.out.println("statusCurrent is " +statusCurrent);
        	//break;
        }
        */
        //System.out.println(qtyConfirm+" "+QuantityOrdered[j].trim());
       /* if(qtyConfirm.equals(QuantityOrdered[j].trim())) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Success for Item ID: "+item_serv,ResultPath, xwpfrun, "");
			System.out.println("Quantity Confirmed is " +qtyConfirm);
        }
        
        else{
        	res = false;
        	utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Failure for Item ID: "+item_serv,ResultPath, xwpfrun, "");
			System.out.println("Quantity Confirmed is not " +qtyConfirm+","+QuantityOrdered[j]);
			//break;
        }*/
       
    	}
    	
    	
    }
    	
    
    }
    
  

	
} catch (Exception e) {
	// TODO: handle exception
	
	e.printStackTrace();
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "FAIL",ResultPath, xwpfrun, "");
    
	res = false;
	return res;
}

finally{
    
    response.close();
    
}

return res;


}

public static boolean GetAllOrder_StatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID,String status, String tcid, String ResultPath, XWPFRun xwpfrun) throws IOException {

boolean res = false;
				



CloseableHttpResponse response=null;

CredentialsProvider credsProvider = new BasicCredentialsProvider();

credsProvider.setCredentials(
		new AuthScope(ProxyHostName, ProxyPort),
		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

try {
	
	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
    HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
    
    RequestConfig config = RequestConfig.custom()
            .setProxy(proxy)
            .build();
    
    HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
    //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
    httpget.setConfig(config);
    
    httpget.addHeader(AuthorizationKey, AuthorizationValue);
    
    System.out.println("Order ID is " + OrderID);
    utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath, xwpfrun, "");
    
    
    System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
    utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath, xwpfrun, "");
 	Thread.sleep(10000);

    response = httpclient.execute(target, httpget);
    

    System.out.println("Response code is "+response.getStatusLine().getStatusCode());
    utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath, xwpfrun, "");
    
    String jsonresponse = EntityUtils.toString(response.getEntity());
    
    
    System.out.println(jsonresponse);
    utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath, xwpfrun, "");
    
  
    
    JsonElement jsonelement = new JsonParser().parse(jsonresponse);
    JsonObject jsonobject = jsonelement.getAsJsonObject();
    
    System.out.println(jsonobject.toString());
    //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
	utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid,jsonobject.toString(),"",ResultPath, xwpfrun, "");

    utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),ResultPath);
    
    JsonArray jarray = jsonobject.getAsJsonArray("orders");
    System.out.println(jarray.size());
    
	
		
	
    	        	
    for(int i=0; i<jarray.size(); i++) {
    	
    
    	jsonobject = jarray.get(i).getAsJsonObject();
    	
 
  		        		        
        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
          if(statusCurrent.equals(status)) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Order ID: "+OrderID,ResultPath, xwpfrun, "");
			System.out.println("statusCurrent is " +statusCurrent);
        }
        
        else{
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Order ID: "+OrderID,ResultPath, xwpfrun, "");
			System.out.println("statusCurrent is " +statusCurrent);
        	//break;
        }
        
   	
    
    }
    
  

	
} catch (Exception e) {
	// TODO: handle exception
	
	e.printStackTrace();
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "FAIL",ResultPath, xwpfrun, "");
    
	res = false;
	return res;
}

finally{
    
    response.close();
    
}

return res;


}


public static boolean GetOrder_MultiItems(String TestDataPath,String SheetName,Integer r,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue,String AllQuantitiesOrdered,String ItemID,String[] status, String tcid, String ResultPath,XWPFRun xwpfrun) throws IOException {

boolean res = false;

	
String item[] = ItemID.split(",");



CloseableHttpResponse response=null;

CredentialsProvider credsProvider = new BasicCredentialsProvider();

credsProvider.setCredentials(
		new AuthScope(ProxyHostName, ProxyPort),
		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

try {
	
	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
    HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
    
    RequestConfig config = RequestConfig.custom()
            .setProxy(proxy)
            .build();
    
    HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
    //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
    httpget.setConfig(config);
    
    httpget.addHeader(AuthorizationKey, AuthorizationValue);
    
    System.out.println("Order ID is " + OrderID);
    utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath,xwpfrun,"");
    
    
    System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
    utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
 	Thread.sleep(10000);

    response = httpclient.execute(target, httpget);
    

    System.out.println("Response code is "+response.getStatusLine().getStatusCode());
    utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
    
    String jsonresponse = EntityUtils.toString(response.getEntity());
    
    
    System.out.println(jsonresponse);
    utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
    
  
    
    JsonElement jsonelement = new JsonParser().parse(jsonresponse);
    JsonObject jsonobject = jsonelement.getAsJsonObject();
    
    System.out.println(jsonobject.toString());
    //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
	utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
	
    utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),"",ResultPath,xwpfrun,"");
    
    JsonArray jarray = jsonobject.getAsJsonArray("items");
    System.out.println(jarray.size());
    
    
    Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	
	Sheet sheet1 = wrk1.getSheet(SheetName);
	
	
	AllQuantitiesOrdered = sheet1.getCell(33, r).getContents().trim();
		
		String[] QuantityOrdered = AllQuantitiesOrdered.split(",");
	
   
    	        	
    for(int i=0; i<jarray.size(); i++) {
    	
    System.out.println("Hello");
    	jsonobject = jarray.get(i).getAsJsonObject();
    	
    	String item_serv = jsonobject.get("itemId").toString();
    	
    	for (int j = 0; j < item.length; j++) {
			
		System.out.println("j is"+j +" and i is"+i);
    	
    	if (item_serv.substring(1, item_serv.length()-1).equals(item[j])) 
    	{
				        	
        		        		        
        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
        
        String qtyConfirm = jsonobject.get("quantityConfirmed").toString();
        

         String statusValidationResult = jsonobject.get("statusValidation").toString();
        
         System.out.println("StatusValidation Result is " + statusValidationResult);
         
         utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is", statusValidationResult,ResultPath,xwpfrun,"");
         
         utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
		 utilityFileWriteOP.writeToLog(tcid,"StatusValiadtion is: "+statusValidationResult,ResultPath);
			
        
        
        
        int countOne = statusValidationResult.split("1").length - 1;
        
        System.out.println(statusCurrent+" "+status[j]+" "+j);
        
        System.out.println(status[j].equals("confirmed"));
        System.out.println(status[j].equals("shipped"));
        System.out.println(countOne == 10);
        System.out.println(countOne < 10);
        
        
        System.out.println("..." + (status[j].equals("confirmed") && (countOne == 10)));
        System.out.println("..." + (status[j].equals("shipped") && (countOne < 10)));
        
        if(status[j].equals("confirmed") && (countOne == 10)) {
        	res = true;
        	System.out.println("Success for Item ID: "+item_serv);
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Count of 1 is 10");
        }
        else
        
        	if(status[j].equals("shipped") && (countOne < 10)) {
	        	res = true;
	        	System.out.println("Success for Item ID: "+item_serv);
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
				System.out.println("Count of 1 is "+countOne);
	        }
        	else{
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");

        	break;
        }
        
        
       /* if(status[j].equals("shipped") && (countOne < 10)) {
        	res = true;
        	System.out.println("Success for Item ID: "+item_serv);
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Count of 1 is "+countOne);
        }
        else {
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");

        	break;
        }*/
        
        for(String str: status) {
        	System.out.println(str);
        }
        
       
       
        if(statusCurrent.equals(status[j])) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+status[j], "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("statusCurrent is " +statusCurrent);
        }
        
        else{
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+status[j], "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("statusCurrent is " +statusCurrent);
        	break;
        }
        if(qtyConfirm.equals(QuantityOrdered[j].trim())) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Quantity Confirmed is " +qtyConfirm);
        }
        
        else{
        	res = false;
        	utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Quantity Confirmed is " +qtyConfirm);
			break;
        }

        break;
    	}
    	
    	
    	
    	}
    }
    	  
    
  

	
} catch (Exception e) {
	// TODO: handle exception
	
	e.printStackTrace();
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "",ResultPath,xwpfrun,"");
    
	res = false;
	return res;
}

finally{
    
    response.close();
    
}

return res;


}




public static boolean GetOrder_MultiItems_PartialShip(String TestDataPath,String SheetName,Integer r,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue,String AllQuantitiesOrdered,String ItemID,String[] status, String tcid, String ResultPath,XWPFRun xwpfrun) throws IOException {

boolean res = false;

	
String item[] = ItemID.split(",");



CloseableHttpResponse response=null;

CredentialsProvider credsProvider = new BasicCredentialsProvider();

credsProvider.setCredentials(
		new AuthScope(ProxyHostName, ProxyPort),
		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

try {
	
	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
    HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
    
    RequestConfig config = RequestConfig.custom()
            .setProxy(proxy)
            .build();
    
    HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
    //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
    httpget.setConfig(config);
    
    httpget.addHeader(AuthorizationKey, AuthorizationValue);
    
    System.out.println("Order ID is " + OrderID);
    utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath,xwpfrun,"");
    
    
    System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
    utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
 	Thread.sleep(10000);

    response = httpclient.execute(target, httpget);
    

    System.out.println("Response code is "+response.getStatusLine().getStatusCode());
    utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
    
    String jsonresponse = EntityUtils.toString(response.getEntity());
    
    
    System.out.println(jsonresponse);
    utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
    
  
    
    JsonElement jsonelement = new JsonParser().parse(jsonresponse);
    JsonObject jsonobject = jsonelement.getAsJsonObject();
    
    System.out.println(jsonobject.toString());
    //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
	utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
	
    utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),"",ResultPath,xwpfrun,"");
    
    JsonArray jarray = jsonobject.getAsJsonArray("items");
    System.out.println(jarray.size());
    
    
    Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	
	Sheet sheet1 = wrk1.getSheet(SheetName);
	
	
	AllQuantitiesOrdered = sheet1.getCell(33, r).getContents().trim();
		
		String[] QuantityOrdered = AllQuantitiesOrdered.split(",");
	
   
    	        	
    for(int i=0; i<jarray.size(); i++) {
    	
    System.out.println("Hello");
    	jsonobject = jarray.get(i).getAsJsonObject();
    	
    	String item_serv = jsonobject.get("itemId").toString();
    	
    	for (int j = 0; j < item.length; j++) {
			
		System.out.println("j is"+j +" and i is"+i);
    	
    	if (item_serv.substring(1, item_serv.length()-1).equals(item[j])) 
    	{
				        	
        		        		        
        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
        
        String qtyConfirm = jsonobject.get("quantityConfirmed").toString();
        

         String statusValidationResult = jsonobject.get("statusValidation").toString();
        
         System.out.println("StatusValidation Result is " + statusValidationResult);
         
         utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is", statusValidationResult,ResultPath,xwpfrun,"");
         
         utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
		 utilityFileWriteOP.writeToLog(tcid,"StatusValiadtion is: "+statusValidationResult,ResultPath);
			
        
        
        
        int countOne = statusValidationResult.split("1").length - 1;
        
        System.out.println(statusCurrent+" "+status[j]+" "+j);
        
        System.out.println(status[j].equals("confirmed"));
        System.out.println(status[j].equals("shipped"));
        System.out.println(countOne == 10);
        System.out.println(countOne < 10);
        
        
        System.out.println("..." + (status[j].equals("confirmed") && (countOne == 10)));
        System.out.println("..." + (status[j].equals("shipped") && (countOne < 10)));
        
        if(status[j].equals("confirmed") && (countOne == 10)) {
        	res = true;
        	System.out.println("Success for Item ID: "+item_serv);
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Count of 1 is 10");
        }
        else
        
        	if(status[j].equals("shipped") && (countOne == 10)) {
	        	res = true;
	        	System.out.println("Success for Item ID: "+item_serv);
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
				System.out.println("Count of 1 is "+countOne);
	        }
        	else{
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");

        	break;
        }
        
        
       /* if(status[j].equals("shipped") && (countOne < 10)) {
        	res = true;
        	System.out.println("Success for Item ID: "+item_serv);
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Count of 1 is "+countOne);
        }
        else {
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");

        	break;
        }*/
        
        for(String str: status) {
        	System.out.println(str);
        }
        
       
       
        if(statusCurrent.equals(status[j])) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+status[j], "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("statusCurrent is " +statusCurrent);
        }
        
        else{
        	res = false;
			utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+status[j], "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("statusCurrent is " +statusCurrent);
        	break;
        }
        if(qtyConfirm.equals(QuantityOrdered[j].trim())) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Success for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Quantity Confirmed is " +qtyConfirm);
        }
        
        else{
        	res = false;
        	utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Failure for Item ID: "+item_serv,ResultPath,xwpfrun,"");
			System.out.println("Quantity Confirmed is " +qtyConfirm);
			break;
        }

        break;
    	}
    	
    	
    	
    	}
    }
    	  
    
  

	
} catch (Exception e) {
	// TODO: handle exception
	
	e.printStackTrace();
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "",ResultPath,xwpfrun,"");
    
	res = false;
	return res;
}

finally{
    
    response.close();
    
}

return res;


}



public static boolean GetOrder_Item_Price(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID,String Price,String item, String tcid, String ResultPath) throws IOException {

boolean res = false;
		
CloseableHttpResponse response=null;

CredentialsProvider credsProvider = new BasicCredentialsProvider();

credsProvider.setCredentials(
		new AuthScope(ProxyHostName, ProxyPort),
		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

try {
	
	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
    HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
    
    RequestConfig config = RequestConfig.custom()
            .setProxy(proxy)
            .build();
    
    HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
    //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
    httpget.setConfig(config);
    
    httpget.addHeader(AuthorizationKey, AuthorizationValue);
    
    System.out.println("Order ID is " + OrderID);
    utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
    
    
    System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
    utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath);
     
    response = httpclient.execute(target, httpget);
    

    System.out.println("Response code is "+response.getStatusLine().getStatusCode());
    utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath);
    
    String jsonresponse = EntityUtils.toString(response.getEntity());
    
    
    System.out.println(jsonresponse);
    utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath);
    
  
    
    JsonElement jsonelement = new JsonParser().parse(jsonresponse);
    JsonObject jsonobject = jsonelement.getAsJsonObject();
    
    System.out.println(jsonobject.toString());
    //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
    utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
    utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),ResultPath);
    
    JsonArray jarray = jsonobject.getAsJsonArray("items");
    System.out.println(jarray.size());
   
    	        	
    for(int i=0; i<jarray.size(); i++) {
    	
    
    	jsonobject = jarray.get(i).getAsJsonObject();
    	
    	String item_serv = jsonobject.get("itemId").toString();
    	
    	if (item_serv.substring(1, item_serv.length()-1).equals(item)) 
    	{
		
        String rs = jsonobject.get("quantityConfirmed").toString();
        
        if(rs.equals(Price)) {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "Price Confirmed is "+Price, "Success for Item ID: "+item_serv,ResultPath);
			System.out.println("Price Confirmed is " +Price);
        }
        
        else{
        	res = false;
        	utilityFileWriteOP.writeToLog(tcid, "Price Confirmed is "+Price, "Failure for Item ID: "+item_serv,ResultPath);
			System.out.println("Price Confirmed is " +rs);
			break;
        }
       
    	}
    	
    	else{
    		System.out.println("Item not Found");
    	}
    }
    	  
    
  

	
} catch (Exception e) {
	// TODO: handle exception
	
	e.printStackTrace();
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
    
	res = false;
	return res;
}

finally{
    
    response.close();
    
}

return res;


}


public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue,String AllQuantitiesOrdered, String tcid, String ResultPath) throws IOException {
	
	boolean res = false;
	
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	int MaxwaitingTime = 60000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath);
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(waitingTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
		utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
		
        utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        String qtyConfirm = jsonobject.get("quantityConfirmed").toString();
	        
	        //System.out.println(qtyConfirm);
	        //System.out.println(QuantityOrdered[i]);
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 7) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("Count of 1 is 7");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);

	        	break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("statusCurrent is " +statusCurrentValue);
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("statusCurrent is " +statusCurrentValue);
	        	break;
	        }
	        if(qtyConfirm.equals(QuantityOrdered[i])) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("Quantity Confirmed is " +qtyConfirm);
	        }
	        
	        else{
	        	res = false;
	        	utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("Quantity Confirmed is " +qtyConfirm);
				break;
	        }
	       
        }
        	  
        
        if(res==true) {
        	break;
        }
        
        }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}

public static boolean GetCallItem_InvalidStatus(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue,String ItemID , String AllQuantitiesOrdered,String tcid, String ResultPath) throws IOException {
	
	boolean res = false;
	
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String item[] = ItemID.split(",");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath);
        
        
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        utilityFileWriteOP.writeToLog(tcid," ",ResultPath);
		utilityFileWriteOP.writeToLog(tcid,getCurrentDate.getISTTime(),ResultPath);
		
        utilityFileWriteOP.writeToLog(tcid, jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        		        		        	
        	String item_serv = jsonobject.get("itemId").toString();
        		        	
        	if (item_serv.substring(1, item_serv.length()-1).equals(item[i])) 
        	{
				
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        String qtyConfirm = jsonobject.get("quantityConfirmed").toString();
	        		        		        		        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(!(countOne == 7)) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("Count of 1 is not 7");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);

	        	break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("statusCurrent is " +statusCurrentValue);
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				System.out.println("statusCurrent is " +statusCurrentValue);
	        	break;
	        }
	        System.out.println(qtyConfirm);
	        System.out.println(QuantityOrdered[i]);
	        if (qtyConfirm.equals(QuantityOrdered[i])) {
				System.out.println(123);
			}
	        if(qtyConfirm.equals(QuantityOrdered[i])) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Success for Item ID: "+item_serv,ResultPath);
				System.out.println("Quantity Confirmed is " +qtyConfirm);
	        }
	        
	        else{
	        	res = false;
	        	utilityFileWriteOP.writeToLog(tcid, "Quantity Confirmed is "+qtyConfirm, "Failure for Item ID: "+item_serv,ResultPath);
				System.out.println("Quantity Confirmed is " +qtyConfirm);
				break;
	        }
        }
        	  
            
        if(res==true) {
        	break;
        }
       }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}

public static boolean GetCallItemStatusValidationOnlyConfirmed(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(waitingTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne != 7) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is not 7");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrentValue);
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrentValue);
	        	break;
	        }
        	
        }
        	  
        
        if(res==true) {
        	break;
        }
        
        }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}

public static String GetCallCaseSizeFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemID, String tcid, String Resultpath) throws IOException {

String res = null;

String pricesJsonvalue = null;

CloseableHttpResponse response=null;

CredentialsProvider credsProvider = new BasicCredentialsProvider();

credsProvider.setCredentials(
		new AuthScope(ProxyHostName, ProxyPort),
		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

try {
	
	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
    HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
    
    RequestConfig config = RequestConfig.custom()
            .setProxy(proxy)
            .build();
    
    //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
    
    HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemID + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
    //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
    httpget.setConfig(config);
    
    httpget.addHeader(AuthorizationKey, AuthorizationValue);
    
    System.out.println("Item ID is " + ItemID);
    utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemID "+ItemID,Resultpath);
    
    
    System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
    utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
    
 /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
    	
    	Thread.sleep(regularWaitingTime);*/
    
    response = httpclient.execute(target, httpget);
    

    System.out.println("Response code is "+response.getStatusLine().getStatusCode());
    utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
    
    String jsonresponse = EntityUtils.toString(response.getEntity());
    System.out.println(jsonresponse);
    utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
    
  
    
    JsonElement jsonelement = new JsonParser().parse(jsonresponse);
    JsonObject jsonobject = jsonelement.getAsJsonObject();
    
    System.out.println(jsonobject.toString());
    utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
    
    JsonArray jarray = jsonobject.getAsJsonArray("items");
    System.out.println(jarray.size());
    for(int i=0; i<jarray.size(); i++) {
    	
    	jsonobject = jarray.get(i).getAsJsonObject();

        
	    JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
	    
	    
	    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
	    	
	    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
        	
        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
        	
        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
	      
        	if(pricesJsonKey.equals("CS")) {
        		
        		
        		for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
	        		
			        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

			        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
			     
	        	}
        	
	        		
        	}
    	
	    }
   
	    pricesJsonvalue = String.valueOf( new Double(Double.parseDouble(pricesJsonvalue)).intValue() );
	    
	    res = pricesJsonvalue;
		
		
    	
    }
    	  
   
} catch (Exception e) {
	// TODO: handle exception
	
	e.printStackTrace();
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
    
	res = null;
	return res;
}

finally{
    
    response.close();
    
}

return res;


}
public static ArrayList<String> skuMinExtract(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	String skuMin = null;
	
	ArrayList<String> MinList = new ArrayList<String>();
	

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int k =0; k<= 10; k++) {
        	
        	Thread.sleep(20000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body for Extracting SkuMin", "Displayed as "+jsonobject.toString(),ResultPath,xwpfrun,"");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());

		for(int lineid=0;lineid<jarray.size(); lineid++) {
           	
	           System.out.println("hello");
	           
	           for(int i=0; i<jarray.size(); i++) {
	        	   
		        	jsonobject = jarray.get(i).getAsJsonObject();

	        	   String itemLineIDforValidation = jsonobject.get("itemLineId").toString().replaceAll("^\"|\"$", "");
	        	   
	        	   System.out.println("itemLineIDforValidation "+itemLineIDforValidation);
	        	   System.out.println(itemLineIDforValidation +" "+ (lineid+1) );
	        	   if(Integer.parseInt(itemLineIDforValidation) != (lineid+1) ) {
	        		   
	        		   continue;
	        	   }
	        	   
	        	   
	        	   
	        	   
	        	   jsonobject.get("statusValidation").toString();
	   	        
	   	        String itemAlternateId = jsonobject.get("itemAlternateId").toString();
	   	        
	   	        JsonElement jsonelementitemAlternateId = new JsonParser().parse(itemAlternateId);
	   	        JsonObject jsonobjectitemAlternateId = jsonelementitemAlternateId.getAsJsonObject();
	   	        
	   	        skuMin = jsonobjectitemAlternateId.get("skuMin").toString().replaceAll("^\"|\"$", "");
	        	   
	        	   //skuMin = jsonobject.get("skuMin").toString().replaceAll("^\"|\"$", "");
			        
			        MinList.add(skuMin);
			        
			        System.out.println(".........................skuMin Result is " + skuMin);
			       
	        	   
	        	   break;
	        	   
	           }
	           
	        }
        

        if(skuMin!=null) {
        	break;
        }
        
        }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "",ResultPath,xwpfrun,"");
        
		skuMin = null;
		return MinList;
	}
    
    finally{
        
        response.close();
        
}
    
    return MinList;
    
	
}
public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid, String Resultpath,XWPFRun xwpfrun, int l, ExtentTest logger) throws IOException {
	
	boolean res = true;
	
	int MaxwaitingTime = 120000;
	
	int regularWaitingTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        //System.out.println("Order ID is " + OrderID);
        
        utilityFileWriteOP.writeToLog(tcid, "Searching the status for ", "Order ID "+OrderID,Resultpath);
        logger.log(LogStatus.PASS, "Order ID "+OrderID);
        
        //System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
        outerloop:for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);
        
        response = httpclient.execute(target, httpget);
        

        //System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        //utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",Resultpath,xwpfrun,String.valueOf(response.getStatusLine().getStatusCode()));
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        //System.out.println(jsonresponse);
        //utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as ",Resultpath,xwpfrun,jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        //System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        //System.out.println(jarray.size());
        innerloop : for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
        	if(!statusCurrent.equals(statusCurrentValue))
        	{
        		res=false;
        		System.out.println(statusCurrent);
        		System.out.println(statusCurrentValue);
        		
        		break innerloop;
        		
        	}
        	else
        	{
        		res=true;
        	}
        	
        	
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        
	        logger.log(LogStatus.PASS, "Current Status of "+jarray.get(i)+" is "+statusValidationResult);
	        //System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = true;
	        	//System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
	        	System.out.println(l+". Item details validation for Order id: "+OrderID);
		        RDS_validiation.startValidation();
		        //System.out.println("Response code for Order details validation is "+response.getStatusLine().getStatusCode());
		        System.out.println("Item StatusValidation Result is " + statusValidationResult);
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation is successful: " +statusValidationResult , "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),Resultpath,xwpfrun,"");
				System.out.println("Item StatusValiadtion is successful for Item ID: "+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Item status for " +OrderID+ " validated successfully");
		        
				//System.out.println("Count of 1 is 10");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),Resultpath,xwpfrun,"");

	        	break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),Resultpath,xwpfrun,"");
				System.out.println("Order Status is " +statusCurrentValue);
				RDS_validiation.EndValidation();
				break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),Resultpath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrentValue);
	        	
	        }
        	
        }
        	  
        
        if(res==true) {
        	break outerloop;
        }
        
        }
        
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "",Resultpath,xwpfrun,"");
        
		res = false;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}
public static ArrayList<String> priceConfirmedEachesAmountList(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String tcid) throws IOException {
	//Need
ArrayList<String> priceConfirmedEachesAmounts = new ArrayList<String>();

String priceConfirmedEachesAmount = null;
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
      
        for(int k = 0; k<=10; k++) {
        	
        	Thread.sleep(20000);
        
        response = httpclient.execute(target, httpget);
        

        //System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        //utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        //System.out.println(jsonresponse);
        //utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
       // System.out.println(jsonobject.toString());
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");

        for(int lineid=0;lineid<jarray.size(); lineid++) {
           	
	           //System.out.println("hello");
	           
	           for(int i=0; i<jarray.size(); i++) {
	        	   
		        	jsonobject = jarray.get(i).getAsJsonObject();

	        	   String itemLineIDforValidation = jsonobject.get("itemLineId").toString().replaceAll("^\"|\"$", "");
	        	   
	        	   //System.out.println("itemLineIDforValidation "+itemLineIDforValidation);
	        	   //System.out.println(itemLineIDforValidation +" "+ (lineid+1) );
	        	   if(Integer.parseInt(itemLineIDforValidation) != (lineid+1) ) {
	        		   
	        		   continue;
	        	   }
	        	   
	        	   priceConfirmedEachesAmount = jsonobject.get("priceConfirmedEachesAmount").toString().replaceAll("^\"|\"$", "");
			        
			        priceConfirmedEachesAmounts.add(priceConfirmedEachesAmount);
			        
			        //System.out.println("priceConfirmedEachesAmount Result is " + priceConfirmedEachesAmount);
			       
	        	   
	        	   break;
	        	   
	           }
	           
	        }
 
        
        if(priceConfirmedEachesAmount!=null) {
        	break;
        }
        
        }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		priceConfirmedEachesAmounts = null;
		return priceConfirmedEachesAmounts;
	}
    
    finally{
        
        response.close();
        
}
    
    return priceConfirmedEachesAmounts;
    
	
}

public static ArrayList<String> priceConfirmedtaxrateList(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String tcid) throws IOException {
	
	ArrayList<String> priceConfirmedTaxRates = new ArrayList<String>();
	
	String priceConfirmedTaxRate = null;
		
		
	CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	       
	        for(int k =0; k<= 5; k++) {
	        	
	         Thread.sleep(20000);
	        
	        response = httpclient.execute(target, httpget);
	        

	        //System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        //utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        //System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        //System.out.println(jsonobject.toString());
	        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        //System.out.println(jarray.size());

	        
	        for(int lineid=0;lineid<jarray.size(); lineid++) {
	           	
		           //System.out.println("hello");
		           
		           for(int i=0; i<jarray.size(); i++) {
		        	   
			        	jsonobject = jarray.get(i).getAsJsonObject();

		        	   String itemLineIDforValidation = jsonobject.get("itemLineId").toString().replaceAll("^\"|\"$", "");
		        	   
		        	   //System.out.println("itemLineIDforValidation "+itemLineIDforValidation);
		        	   //System.out.println(itemLineIDforValidation +" "+ (lineid+1) );
		        	   if(Integer.parseInt(itemLineIDforValidation) != (lineid+1) ) {
		        		   
		        		   continue;
		        	   }
		        	   
		        	   priceConfirmedTaxRate = jsonobject.get("priceConfirmedTaxRate").toString().replaceAll("^\"|\"$", "");
				        
		        	   priceConfirmedTaxRates.add(priceConfirmedTaxRate);
				        
				        //System.out.println("priceConfirmedTaxRate Result is " + priceConfirmedTaxRate);
				       
		        	   
		        	   break;
		        	   
		           }
		           
		        }
	        
	        
	       	  
	        
	        if(priceConfirmedTaxRate!=null) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			priceConfirmedTaxRates = null;
			return priceConfirmedTaxRates;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	  return priceConfirmedTaxRates;
	    
		
	}
public static String GetCallquantityTypeFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
    
    SYSUserName="SYSTCS19";
    SYSPassWord="Argha1234567890";
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
//        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
       
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	
	        
		    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
		    
		    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
		    ArrayList<String> listoflookupkeys = new ArrayList<String>();
		    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();

		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

		    
		    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
		    	
		    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
	        	
	        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
		        
	        	if(lookupsJsonKey.equals("SINGLE-PICK")) {
	        		
	        		
	        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
		        		
				        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

				        String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
				        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
				       System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
				        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
				    	
				    	long startdatemilli = date.getTime();
				        
				        mapofquantityType.put(lookupsJsonvalue+String.valueOf(lookupsBodyindex), startdatemilli);
		        		
		        	}
	        	
 	        		
	        	}
	    	
		    }
		    System.out.println("mapofquantityType.size() "+mapofquantityType.size());
		    for(String str: mapofquantityType.keySet()) {
		    	
		    	listoflookupkeys.add(str);
		    	listoflookupstartvalues.add(mapofquantityType.get(str));
		    }

		    long latestdatemilli = 0; 
		    
		    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
		    			    	
		    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
		    		
		    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
		    		
		    	}
		    	
		    }
		    
		    String finalSinglePickkey = null;
		    String quantityTypeFromWebPortal = null;
		    
		    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
		    	
		    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
		    		
		    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
		    	}
		    	
		    }
		    
		    System.out.println("latest date in milli "+latestdatemilli);
		    
		    if(finalSinglePickkey.contains("Y")) {
		    	
		    	quantityTypeFromWebPortal = "EA";
		    }
		    
		    if(finalSinglePickkey.contains("N")) {
		    	
		    	quantityTypeFromWebPortal = "CS";
		    }
		    
		    
		    //String itemDetailsbody = "{\"quantityType\": \"" + quantityTypeFromWebPortal + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    //System.out.println(itemDetailsbody);	    
		    	     
		    res = quantityTypeFromWebPortal;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}
}
