package SandPiper_All_Functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import SandPiperUtilities.excelCellValueWrite;
import SandPiperUtilities.utilityFileWriteOP;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;

public class RDS_validiation {
	
public static ArrayList<HashMap<String, String>> TemporaryOrderIDExtract(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String NasPath, String LocalPath, String OrderID, String tcid) throws IOException {
		
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		
		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;
		
		//ArrayList<ArrayList<Map.Entry<String, String>>> ListofMaps = new ArrayList<ArrayList<Map.Entry<String,String>>>();
		
		ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> hm = null;
		
		ArrayList<String> HeaderList = null;
		
		//String Result = null;
		//int flag = 0;
		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftpchannel = (ChannelSftp) channel;
			
		/*	sftpchannel.cd("cd..;");
			sftpchannel.cd("cd..;");*/
			
			
			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				
				//Date currentfilelastmodifieddate = new Date(new File(filename).lastModified());
				//System.out.println(entry.getAttrs().getMTime());
				long currentfilelastmodifiedmilliseconds = (long) entry.getAttrs().getMTime();
				
				if(currentfilelastmodifiedmilliseconds > lastmodifiedfiletime) {
					
					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;

				}
					
				
			}
			System.out.println(lastmodifiedfilename +"  "+ LocalPath + lastmodifiedfilename);
			sftpchannel.get(lastmodifiedfilename, LocalPath + lastmodifiedfilename);//Copy file from WinSCP to local

				//Read from local
				br = new BufferedReader(new FileReader(LocalPath + lastmodifiedfilename));
				System.out.println(lastmodifiedfilename +" "+ lastmodifiedfiletime);
				
				String CurrentLine = null;
				int linecount = 0;
				
				while((CurrentLine = br.readLine()) != null) {
					
					//ArrayList<Map.Entry<String, String>> FileKeyValues = new ArrayList<Map.Entry<String,String>>();
					hm = new HashMap<String, String>();
					//Split the Current line in comma seperated values
					String[] strarr = CurrentLine.split(",");
					
					if(linecount==0) {
						
						HeaderList = new ArrayList<String>();
						
						for(int i=0; i<strarr.length; i++){
							HeaderList.add(strarr[i]);
						}
						
						
					}
				
					if(linecount > 0) {
						
						
						
						for(int j=0; j<strarr.length; j++) {
							
							//FileKeyValues.add(new AbstractMap.SimpleEntry(HeaderList.get(j), strarr[j]));
							//System.out.println("About to put "+HeaderList.get(j)+ " and" +strarr[j]);
							hm.put(HeaderList.get(j), strarr[j]);
						}
						
						ListofMaps.add(hm);
						System.out.println("added hashmap to arraylist "+hm.size() + hm.get("messageCreatedAt"));

					}
					
					

					linecount++;
					
					//TempOrderIDList.add(strarr[0]);
					

					
				}
				
			
			br.close();
			FileUtils.cleanDirectory(new File(LocalPath)); // Delete all downloaded files from local directory
			
			
			sftpchannel.exit();
			
			session.disconnect();
			
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);
			return ListofMaps;
		}
		
		
		
	
		
		return ListofMaps;
	}

public static long LastModifiedFileTime(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String NasPath, String TemporaryFilePath, String tcid) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	//BufferedReader br = null;

	long lastmodifiedfiletime = 0;
	//String Result = null;
	//int flag = 0;
	
	try {
		
		session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(SFTPPassword);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
				
		sftpchannel.cd(NasPath);
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			
			
			
			sftpchannel.get(filename, TemporaryFilePath + filename);//Copy file from WinSCP to local
			
			Date currentfilelastmodifieddate = new Date(new File(TemporaryFilePath + filename).lastModified());

			long currentfilelastmodifiedmilliseconds = currentfilelastmodifieddate.getTime();

			if(currentfilelastmodifiedmilliseconds > lastmodifiedfiletime) {

				lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;

			}
			
		
			
		}
		
		FileUtils.cleanDirectory(new File(TemporaryFilePath)); // Delete all downloaded files from local directory
		
		//br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);
		return lastmodifiedfiletime;
	}
	
	
	

	
	return lastmodifiedfiletime;
}


	public static ArrayList<String> OrderIDListExtract(String DriverSheetPath, String SheetName) {
		
		ArrayList<String> OrderIDList = new ArrayList<String>();
		
		String OrderID = null;
		
		int r = 0;
		
		try {
			
			int rows = 0;
			
			Workbook wrk1 = Workbook.getWorkbook(new File(DriverSheetPath));
			
			Sheet sheet1 = wrk1.getSheet(SheetName);
			
			rows = sheet1.getRows();
			
			for(r=1; r<rows; r++) {
				
				OrderID = sheet1.getCell(16, r).getContents().trim();
				
				OrderIDList.add(OrderID);
			}
			
			
			return OrderIDList;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			OrderIDList = null;
			return OrderIDList;
		}
		
		
	}
	
	
	
	public static boolean validateInRDS_MultipleOrder(String orderid,String[] columnname, String[] itemid, HashMap<String, String> Payload_map, ArrayList<String> ItemDetailsList, ArrayList<String> priceConfirmedEachesAmountList, ArrayList<String> MinList, String ResultPath, String tcid, int r, String testdatapath ,  String sheetname , ArrayList<String> vatrate, String[] ship, XWPFRun xwpfRun,String ItemDetailsSheetName,int row) throws JSchException, BiffException, IOException{
		//********************Initialising Variables*******************************
		Channel channel=null;
		Session session=null;
		String order_type_source = "SALE";
		String channel_reference = "WHOLESALE";
		String sales_process_status = "pending";
		boolean res=true;
		int itemcaseSize = 0;
		String QResult="";
		ItemDetailsList = new ArrayList<String>();
	 	Workbook wrk1 = Workbook.getWorkbook(new File(testdatapath));
		Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
		String ItemIDs = sheet2.getCell(1, row).getContents().trim();
		String[] AllItemsInItemDetails = ItemIDs.split(",");
		for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
			String ItemDetailsJson = sheet2.getCell(c, row).getContents().trim();
			ItemDetailsList.add(ItemDetailsJson);
		}
		try{ 
			System.out.println(" Order details validation in RDS database");
			//********************Start Validation***********************************
			RDS_validiation.startValidation();
			JSch jsch = new JSch();
			java.util.Properties config = new java.util.Properties(); 
			String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user api_space_v1_owner_sit";
			session = jsch.getSession("mwadmin","10.244.39.83",22);
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("2Bchanged");
			session.connect();
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			InputStream in = channel.getInputStream();
			((ChannelExec)channel).setPty(true);
			OutputStream out=channel.getOutputStream();
			channel.connect();
			Thread.sleep(1000);
	        out.write(("api_space_v1_owner_sit999"+"\n").getBytes());
	        out.flush();
	        for(int col=0;col<columnname.length;col++){
	        	Thread.sleep(1000);
	        	QResult="";
	        	out.flush();
	        	out.write(("select distinct "+columnname[col]+" from storepick_sales_service.SALES_DETAILS where order_id ='"+orderid+"';\n").getBytes());
	        	out.flush();
	        	Thread.sleep(5000);
	        	byte[] tmp = new byte[4096];
        		int i=0;  
        		while (in.available() > 0) {
        			i = in.read(tmp, 0, 4096);
        			if (i < 0) {
        				break;
        			}
        		}
    			QResult=new String(tmp, 0, i); 
    			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid,"Query result screenshot is: " ,QResult , ResultPath);
    			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Query result", "Displayed in RDS as -", ResultPath, xwpfRun, QResult);
    			String[] rest= QResult.split(columnname[col]);
    			String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
    			String[] rest2=justfinalres.split("[\\r\\n]+");
    			String finalres = "";
    			for(int j=0; j<rest2.length; j++) {
    				if(rest2[j].contains("(1 row)")) {
    					break;
    				}
    				finalres = finalres + rest2[j].trim();
    			}
    			finalres = finalres.replaceAll("\\.\\.", "");
    			String nasres = null;
    			if(columnname[col].equalsIgnoreCase("order_id")) {
    				nasres = orderid;
    			}
    			if(columnname[col].equalsIgnoreCase("order_type")) {
    				nasres = order_type_source;
    			}
    			if(columnname[col].equals("channel")) 
    			{
    				nasres = channel_reference;
    			}
    			if(columnname[col].equals("sales_process_status")) 
    			{
    				nasres = sales_process_status;
    			}
    			if(columnname[col].equals("sales_payload")) 
    			{
    				JsonElement jsonelement = new JsonParser().parse(finalres);
    				JsonObject jsonobject = jsonelement.getAsJsonObject();
    				jsonobject.get("locationId").toString().replaceAll("^\"|\"$", "");
    				jsonobject.get("transactions").toString();
    				JsonArray transactionsJsonArray = jsonobject.getAsJsonArray("transactions");
    				JsonObject jsonobjectTransactions = transactionsJsonArray.get(0).getAsJsonObject();
    				//getting transaction attributes
    				String typeTransactions = jsonobjectTransactions.get("type").toString().replaceAll("^\"|\"$", "");
    				String sourceTransactions = jsonobjectTransactions.get("source").toString().replaceAll("^\"|\"$", "");
    				String referenceTransactions = jsonobjectTransactions.get("reference").toString().replaceAll("^\"|\"$", "");
    				jsonobjectTransactions.get("transactionId").toString().replaceAll("^\"|\"$", "");
    				jsonobjectTransactions.get("items").toString();
    				JsonArray itemsJsonArray = jsonobjectTransactions.getAsJsonArray("items");
    				//this function will arrange the items in sequence
    				for(int item=0;item<itemid.length; item++) {
	 	           		for(int p=0; p<itemid.length; p++) {
	 	           			try{
			  		        	jsonobject = itemsJsonArray.get(p).getAsJsonObject();
	 	           			}catch (IndexOutOfBoundsException e) {
	 	           				SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The  item "+MinList.get(item), "Did not found in RDS DB ", ResultPath,xwpfRun,"");
	 	           				break;
	 	           			}
 	           				String productIdRDS = jsonobject.get("productId").toString().replaceAll("^\"|\"$", "");
 	           				JsonObject jsonobjectItems1 = itemsJsonArray.get(p).getAsJsonObject();
 	           				String quantityItems2 = jsonobjectItems1.get("quantity").toString().replaceAll("^\"|\"$", "");
 	           				excelCellValueWrite.appendValueToCell(productIdRDS+",", r, 44, testdatapath, sheetname);
 	           				excelCellValueWrite.appendValueToCell(quantityItems2+",", r, 45, testdatapath, sheetname);
 	           				if( !(productIdRDS.equals(MinList.get(item))) ) {
 	           					continue;
 	           				}
 	           				String uomItems = null;
 	           				String statusItems = null;
 	           				String quantityItems = null;
 	           				String productId = null;
 	           				String amountretailPrice = null;
 	           				String currencyretailPrice = null;
 	           				JsonElement jsonelementItemDetailsList = new JsonParser().parse(ItemDetailsList.get(item));
 	           				JsonObject jsonobjectItemDetailsList = jsonelementItemDetailsList.getAsJsonObject();
 	           				//getting itemdetailslist attributes
 	           				String ItemDetailsListuom = jsonobjectItemDetailsList.get("quantityType").toString().replaceAll("^\"|\"$", "");
 	           				jsonobjectItemDetailsList.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");	
 	           				jsonobjectItemDetailsList.get("itemId").toString().replaceAll("^\"|\"$", "");
 	           				jsonobjectItemDetailsList.get("priceOrderedCurrency").toString().replaceAll("^\"|\"$", "");
 	           				String ItemDetailsitemcaseSize = jsonobjectItemDetailsList.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
 	           				itemcaseSize = Integer.parseInt(ItemDetailsitemcaseSize);
           					JsonObject jsonobjectItems = null;
           					jsonobjectItems = itemsJsonArray.get(p).getAsJsonObject();
           					//getting item attributes
           					uomItems = jsonobjectItems.get("uom").toString().replaceAll("^\"|\"$", "");
           					statusItems = jsonobjectItems.get("status").toString().replaceAll("^\"|\"$", "");
           					quantityItems = jsonobjectItems.get("quantity").toString().replaceAll("^\"|\"$", "");
           					productId = jsonobjectItems.get("productId").toString().replaceAll("^\"|\"$", "");
           					jsonobjectItems.get("retailPrice").toString();	
           					JsonArray retailPriceJsonArray = jsonobjectItems.getAsJsonArray("retailPrice");
           					JsonObject jsonobjectretailPrice = retailPriceJsonArray.get(0).getAsJsonObject();
           					//getting retailPrice attributes
           					amountretailPrice = jsonobjectretailPrice.get("amount").toString().replaceAll("^\"|\"$", "");
           					currencyretailPrice = jsonobjectretailPrice.get("currency").toString().replaceAll("^\"|\"$", "");
           					if(uomItems.equals("EA") ) {
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of uom", "Matched with the value found in RDS DB: "+uomItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
           					}else {
	  			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of uom", "Did not match with the value found in RDS DB "+uomItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  				    		 res=res&&false;
	  			             }
           					if( statusItems.equals(order_type_source)) {
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+"", "Matched with the value found in RDS DB "+statusItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  			            	System.out.println("The value of order_type_source in webportal: "+order_type_source+ " matched with the value found in RDS DB: "+uomItems+ " for item "+itemid[item]);
           					}else{
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+"", "Did not match with the value found in RDS DB "+statusItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  			            	res=res&&false;
           					}
           					int lastquantity;
           					if(ItemDetailsListuom.contains("CS")){
           						lastquantity= itemcaseSize * Integer.parseInt(ship[item]);// only for case... For eaches remove the itemcaseSize
           					}else{
           						lastquantity= Integer.parseInt(ship[item]);
           					}
           					if( Integer.parseInt(quantityItems) == lastquantity ) {
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+lastquantity+"", "Matched with the value found in RDS DB "+quantityItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  			            	System.out.println("The value of ItemDetailsListquantity in webportal: "+lastquantity+ " matched with the value found in RDS DB: "+quantityItems+ " for item "+itemid[item]);
           					}else {
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+lastquantity+"", "Did not match with the value found in RDS DB "+quantityItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  			            	res=res&&false;
           					}
           					if( productId.equals(MinList.get(item)) ) {
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+MinList.get(item)+"", "Matched with the value found in RDS DB "+productId+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  			            	System.out.println("The value of ItemDetailsListItemId in webportal: "+MinList.get(item)+ " matched with the value found in RDS DB: "+productId+ " for item "+itemid[item]);
           					}else {
           						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+MinList.get(item)+"", "Did not match with the value found in RDS DB "+productId+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  			            	res=res&&false;
           					}	
	  		         //---------------------------priceConfirmedEachesAmountExtract validation --------------------------
           					//This change is finally implemented on 26/03/2018 as discussed with thiru
       					double initialprice = Double.parseDouble(priceConfirmedEachesAmountList.get(item));
	  		    		BigDecimal bigDecimal = new BigDecimal(initialprice);
	  		            BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
	  		            String finalamount = roundedWithScale.toString();
	  		            if( amountretailPrice.equals(finalamount) ) {
	  		            	SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+finalamount+"", "Matched with the value found in RDS DB "+amountretailPrice+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  		            	System.out.println("The value of priceConfirmedEachesAmountExtract in webportal: "+finalamount+ " matched with the value found in RDS DB: "+amountretailPrice+ " for item "+itemid[item]);
	  		            }else {
	  		            	SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+finalamount+"", "Did not match with the value found in RDS DB "+amountretailPrice+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  		            	res=res&&false;
	  		            }
	  		            //----------------------------------------------------------------------- 
	  		            if(currencyretailPrice.equals("GBP")) {
	  		            	SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice ", "Matched with the value found in RDS DB "+currencyretailPrice+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  		            	System.out.println("The value of currencyretailPrice in webportal: GBP matched with the value found in RDS DB: "+currencyretailPrice+ " for item "+itemid[item]);
	  		            }else {
	  		            	SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice ", "Did not match with the value found in RDS DB "+currencyretailPrice+ " for item "+itemid[item], ResultPath,xwpfRun,"");
	  		            	res=res&&false;
	  		            }
	  		            JsonArray tendersJsonArray = jsonobjectTransactions.getAsJsonArray("tenders");
	  		            JsonObject jsonobjectTenders = tendersJsonArray.get(0).getAsJsonObject();
	  		            jsonobjectTenders.get("tenderamount").toString();
	  		            JsonArray tenderamountJsonArray = jsonobjectTenders.getAsJsonArray("tenderamount");
	  		            JsonObject jsonobjectTendersAmount = tenderamountJsonArray.get(0).getAsJsonObject();
	  		            String amountTenderAmount = jsonobjectTendersAmount.get("amount").toString().replaceAll("^\"|\"$", "");
	  		            String date = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
	  		            excelCellValueWrite.writeValueToCell(date, r, 46, testdatapath, sheetname);
	  		            excelCellValueWrite.writeValueToCell(amountTenderAmount, r, 43, testdatapath, sheetname);
	  		            break;
	 	           		}     
    				}
    				if( typeTransactions.equals(order_type_source)) {
    					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" ", "Matched with the value found in RDS DB "+typeTransactions, ResultPath,xwpfRun,"");
    					System.out.println("The value of order_type_source in webportal: " +order_type_source+ " matched with the value found in RDS DB: "+typeTransactions);
    				}else {
    					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" ", "Did not match with the value found in RDS DB "+typeTransactions, ResultPath,xwpfRun,"");
    					res=res&&false;
    				}
    				if( sourceTransactions.equals("WEB")) {
    					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions ", "Matched with the value found in RDS DB "+sourceTransactions, ResultPath,xwpfRun,"");
    					System.out.println("The value of sourceTransactions in webportal: WEB matched with the value found in RDS DB: "+sourceTransactions);
    				}else {
    					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions ", "Did not match with the value found in RDS DB "+sourceTransactions, ResultPath,xwpfRun,"");
    					res=res&&false;
    				}
    				if( referenceTransactions.equals(channel_reference)) {
    					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+"", "Matched with the value found in RDS DB "+referenceTransactions, ResultPath,xwpfRun,"");
    					System.out.println("The value of channel_reference in webportal: " +channel_reference+ " matched with the value found in RDS DB: "+referenceTransactions);
    				}else {
    					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+"", "Did not match with the value found in RDS DB "+referenceTransactions, ResultPath,xwpfRun,"");
    					res=res&&false;
    				}
    			}
				if(!columnname[col].equals("sales_payload")) {
					if(nasres.equalsIgnoreCase(finalres))
					{
						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" "+nasres, "Matched with the value found in RDS DB "+finalres, ResultPath,xwpfRun,"");
	  		    		System.out.println("The value of "+columnname[col]+"from webportal: "+nasres+ " matched with the value found in RDS DB: "+finalres);
					}
					else
					{		
						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" "+nasres, "Did not match with the value found in RDS DB "+finalres, ResultPath,xwpfRun,"");
						res=res&&false;
					}
    			}
	        }
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        out.flush();
	        Thread.sleep(5000);
	        channel.disconnect();
	        session.disconnect();
	        Thread.sleep(5000);
	        out.close();
	        in.close();
	        System.out.println("Order details validated successfully in RDS DB");
	        RDS_validiation.EndValidation();
		}catch(Exception e){
			e.printStackTrace();
			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Error occurred while", "Validating in RDS DB", ResultPath,xwpfRun,"");
			System.out.println("inside catch block"+res);
			res = false;
		}finally{        
			if (channel != null) {
				session = channel.getSession();
				channel.disconnect();
				session.disconnect();
			}
	    	wrk1.close();
	    	return res;
		}
	}
	
	

	
	public static boolean validateInRDS_Multiple_Negative(String orderid,String[] columnname, String[] itemid, HashMap<String, String> Payload_map, ArrayList<String> ItemDetailsList, ArrayList<String> priceConfirmedEachesAmountList, ArrayList<String> MinList, String ResultPath, String tcid, int r, String testdatapath ,  String sheetname,String ItemDetailsSheetName,int row,XWPFRun xwpfRun) throws JSchException, BiffException, IOException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=true;
		 
		 String QResult="";
	     
	     
	     ItemDetailsList = new ArrayList<String>();
	 	Workbook wrk1 = Workbook.getWorkbook(new File(testdatapath));
		
		Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
		
		
	
			String ItemIDs = sheet2.getCell(1, row).getContents().trim();
				  
				 
				
				String[] AllItemsInItemDetails = ItemIDs.split(",");
				
				//System.out.println("Number of ItemIDs " + ItemID.length);
				System.out.println("Items with details " + AllItemsInItemDetails.length);
				
				for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
					
					String ItemDetailsJson = sheet2.getCell(c, row).getContents().trim();
					System.out.println("Adding " + ItemDetailsJson);
					ItemDetailsList.add(ItemDetailsJson);
					
					
				}


	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              //String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user api_space_v1_owner_sit";

	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(true);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        //out.write(("user124676r4"+"\n").getBytes());
	        out.write(("api_space_v1_owner_sit999"+"\n").getBytes());

	        out.flush();
	        
	        

	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     //out.write(("select distinct "+columnname[col]+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     out.write(("select distinct sales_process_status from storepick_sales_service.SALES_DETAILS where order_id ='"+orderid+"';\n").getBytes());

	     //select * from storepick_sales_service.SALES_DETAILS;	

	        out.flush();
	        
	        //Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[4096];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 4096);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	              System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
			 System.out.println("QResult is "+QResult+" is this!!!");
			 
			 
			 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid,"Query result screenshot is: " ,QResult , ResultPath);
			 
			 
			 
			 

	        String[] rest= QResult.split("sales_process_status");

			String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 
			System.out.println("Testing "+justfinalres.replaceAll("[\\r\\n]+", "").replaceAll("[\\.\\.]+", ""));
			
			//System.out.println(justfinalres);
			String[] rest2=justfinalres.split("[\\r\\n]+");
			//System.out.println(rest2.length);
			 
			String finalres = "";
			for(int j=0; j<rest2.length; j++) {
				 
				 if(rest2[j].contains("(1 row)")) {
					 break;
				 }
				 
				 finalres = finalres + rest2[j].trim();
			 }
			 
			 finalres = finalres.replaceAll("\\.\\.", "");
			 
			 System.out.println("finalres is "+finalres);
	    

	    	 
	    	 if(finalres.contains("0 rows")){
	    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Order id: "+orderid, "Not found in RDS DB "+finalres, ResultPath,xwpfRun,""); 
	    		 res =true;
	    	 }
	    	 
	    	 else{
	    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Order id: "+orderid, "Found in RDS DB "+finalres, ResultPath,xwpfRun,""); 
	    		 res =false;
	    	 }
	    	 
	
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
	        catch(Exception e){
	        	e.printStackTrace();
	        	System.out.println(e);

	        	res = false;
	        	return res;

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		wrk1.close();
	      return res;
	}

	}
	
	
	
	
	
	
	
	
	public static boolean validateInRDS_Multiple_Shipped_quantity(String orderid,String[] columnname, String[] itemid, HashMap<String, String> Payload_map, ArrayList<String> ItemDetailsList, ArrayList<String> priceConfirmedEachesAmountList, ArrayList<String> MinList, String shippedquantity, String ResultPath, String tcid, int r, String testdatapath ,  String sheetname, ArrayList<String> vatrate,String[] ship,XWPFRun xwpfRun,String ItemDetailsSheetName,int row) throws JSchException, BiffException, IOException{
		 Channel channel=null;
		 Session session=null;
		 
		 shippedquantity.split(",");
		 
		 String order_type_source = "SALE";
		 String channel_reference = "WHOLESALE";
		 String sales_process_status = "pending";
		 
		 boolean res=true;
		 
		 //ArrayList<Map> itemlist=list;
		 int itemcaseSize = 0;
	     
	     String QResult="";
	     
	     
	     ItemDetailsList = new ArrayList<String>();
	 	Workbook wrk1 = Workbook.getWorkbook(new File(testdatapath));
		
		Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
		
		
	
			String ItemIDs = sheet2.getCell(1, row).getContents().trim();
				  
				 
				
				String[] AllItemsInItemDetails = ItemIDs.split(",");
				
				//System.out.println("Number of ItemIDs " + ItemID.length);
				System.out.println("Items with details " + AllItemsInItemDetails.length);
				
				for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
					
					String ItemDetailsJson = sheet2.getCell(c, row).getContents().trim();
					System.out.println("Adding " + ItemDetailsJson);
					ItemDetailsList.add(ItemDetailsJson);
					
					
				}


	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              //String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user api_space_v1_owner_sit";

	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(true);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        //out.write(("user124676r4"+"\n").getBytes());
	        out.write(("api_space_v1_owner_sit999"+"\n").getBytes());

	        out.flush();
	        
	        
	        for(int item=0;item<itemid.length;item++)
	        {
	        for(int col=0;col<columnname.length;col++)
	        {
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     //out.write(("select distinct "+columnname[col]+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     out.write(("select distinct "+columnname[col]+" from storepick_sales_service.SALES_DETAILS where order_id ='"+orderid+"';\n").getBytes());

	     //select * from storepick_sales_service.SALES_DETAILS;	

	        out.flush();
	        
	        //Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[4096];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 4096);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	              System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
			 System.out.println("QResult is "+QResult+" is this!!!");
			 
			 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid,"Query result screenshot is: " ,QResult , ResultPath,xwpfRun,"");
			 
			 
			 
			 
			 

	        String[] rest= QResult.split(columnname[col]);

			String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 
			System.out.println("Testing "+justfinalres.replaceAll("[\\r\\n]+", "").replaceAll("[\\.\\.]+", ""));
			
			//System.out.println(justfinalres);
			String[] rest2=justfinalres.split("[\\r\\n]+");
			//System.out.println(rest2.length);
			 
			String finalres = "";
			for(int j=0; j<rest2.length; j++) {
				 
				 if(rest2[j].contains("(1 row)")) {
					 break;
				 }
				 
				 finalres = finalres + rest2[j].trim();
			 }
			 
			 finalres = finalres.replaceAll("\\.\\.", "");
			 
			 System.out.println("finalres is "+finalres);
	    
	    	 //String finalres=rest2[1].trim();
	    	 System.out.println(columnname[col]);
	    	 System.out.println(finalres);
	    	 
	    	//System.out.println(itemlist.get(item).toString());
	    	System.out.println(columnname[col]);
	    	 //String nasres=itemlist.get(item).get(columnname[col]).toString().replaceAll("[-+|]", "");
	    	 
	    	/*JsonElement jsonelementItemDetailsList = new JsonParser().parse(ItemDetailsList.get(item));
	    	//System.out.println(ItemDetailsList.get(item));
          JsonObject jsonobjectItemDetailsList = jsonelementItemDetailsList.getAsJsonObject();
          
          //getting itemdetailslist attributes
          String ItemDetailsListuom = jsonobjectItemDetailsList.get("quantityType").toString().replaceAll("^\"|\"$", "");
          String ItemDetailsListquantity = jsonobjectItemDetailsList.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");	
	    	String ItemDetailsListItemId = jsonobjectItemDetailsList.get("itemId").toString().replaceAll("^\"|\"$", "");
	    	String ItemDetailspriceOrderedCurrency = jsonobjectItemDetailsList.get("priceOrderedCurrency").toString().replaceAll("^\"|\"$", "");
	    	String ItemDetailsitemcaseSize = jsonobjectItemDetailsList.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
	    	
	    	itemcaseSize = Integer.parseInt(ItemDetailsitemcaseSize);*/
	    			
	    	 String nasres = null;
	    	 
	    	 if(columnname[col].equalsIgnoreCase("order_id")) {
	    		 nasres = orderid;
	    	 }

	    	 
	    	 if(columnname[col].equalsIgnoreCase("order_type")) {
	    		 nasres = order_type_source;
	    	 }
	    	 
	    	 if(columnname[col].equals("channel")) 
	    	 {
	    		 nasres = channel_reference;
	    	 }
	    	 
	    	 if(columnname[col].equals("sales_process_status")) 
	    	 {
	    		 nasres = sales_process_status;
	    	 }
	    	 
	    	 if(columnname[col].equals("sales_payload")) 
	    	 {
	    		 System.out.println("finalres for sales_payload " + finalres);
	    		 
	    		 
	    		 JsonElement jsonelement = new JsonParser().parse(finalres);
	             JsonObject jsonobject = jsonelement.getAsJsonObject();
	                
	             String resultlocationId = jsonobject.get("locationId").toString().replaceAll("^\"|\"$", "");
	             
	             jsonobject.get("transactions").toString();
	             
	             
	             //JsonElement jsonelementTransactions = new JsonParser().parse(transactionsJsonBody);
	             
	             JsonArray transactionsJsonArray = jsonobject.getAsJsonArray("transactions");

	             JsonObject jsonobjectTransactions = transactionsJsonArray.get(0).getAsJsonObject();
	             
	             //getting transaction attributes
	             String typeTransactions = jsonobjectTransactions.get("type").toString().replaceAll("^\"|\"$", "");
	             String sourceTransactions = jsonobjectTransactions.get("source").toString().replaceAll("^\"|\"$", "");
	             String referenceTransactions = jsonobjectTransactions.get("reference").toString().replaceAll("^\"|\"$", "");
	             jsonobjectTransactions.get("transactionId").toString().replaceAll("^\"|\"$", "");
	             
	             jsonobjectTransactions.get("items").toString();
	             JsonArray itemsJsonArray = jsonobjectTransactions.getAsJsonArray("items");
	             
	             String uomItems = null;
	             String statusItems = null;
	             String quantityItems = null;
	             String productId = null;
	             
	             String amountretailPrice = null;
	             String currencyretailPrice = null;

	             
	             
	            	 
	            	 JsonElement jsonelementItemDetailsList = new JsonParser().parse(ItemDetailsList.get(item));
	     	    	//System.out.println(ItemDetailsList.get(item));
	                 JsonObject jsonobjectItemDetailsList = jsonelementItemDetailsList.getAsJsonObject();
	                 
	                 //getting itemdetailslist attributes
	                 String ItemDetailsListuom = jsonobjectItemDetailsList.get("quantityType").toString().replaceAll("^\"|\"$", "");
	                 jsonobjectItemDetailsList.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");	
	     	    	jsonobjectItemDetailsList.get("itemId").toString().replaceAll("^\"|\"$", "");
	     	    	jsonobjectItemDetailsList.get("priceOrderedCurrency").toString().replaceAll("^\"|\"$", "");
	     	    	String ItemDetailsitemcaseSize = jsonobjectItemDetailsList.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
	     	    	
	     	    	itemcaseSize = Integer.parseInt(ItemDetailsitemcaseSize);
	            	 
	            	//JsonElement jsonelementItems = new JsonParser().parse(itemsjsonBody);
		             JsonObject jsonobjectItems = itemsJsonArray.get(item).getAsJsonObject();
		             
		           //getting item attributes
		              uomItems = jsonobjectItems.get("uom").toString().replaceAll("^\"|\"$", "");
		              statusItems = jsonobjectItems.get("status").toString().replaceAll("^\"|\"$", "");
		              quantityItems = jsonobjectItems.get("quantity").toString().replaceAll("^\"|\"$", "");
		              productId = jsonobjectItems.get("productId").toString().replaceAll("^\"|\"$", "");
		             
		             jsonobjectItems.get("retailPrice").toString();
		             
		             JsonArray retailPriceJsonArray = jsonobjectItems.getAsJsonArray("retailPrice");
		             
		          // JsonElement jsonelementretailPrice = new JsonParser().parse(retailPricejsonBody);
		             JsonObject jsonobjectretailPrice = retailPriceJsonArray.get(0).getAsJsonObject();
		             
		             //getting retailPrice attributes
		              amountretailPrice = jsonobjectretailPrice.get("amount").toString().replaceAll("^\"|\"$", "");
		              currencyretailPrice = jsonobjectretailPrice.get("currency").toString().replaceAll("^\"|\"$", "");
		              
		              
		              
		              if(uomItems.equals("EA") ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
				    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of uom:  from NAS file ", "Matched with the value found in RDS DB "+uomItems+ "for item "+itemid[item], ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of uom: "+ItemDetailsListuom+" from NAS file ", "Did not match with the value found in RDS DB "+uomItems+ "for item "+itemid[item], ResultPath,xwpfRun,"");
				    		 res=false;
			             }
		              
		              if( statusItems.equals(order_type_source)) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+statusItems+ "for item "+itemid[item], ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+statusItems+ "for item "+itemid[item], ResultPath,xwpfRun,"");
				    		 res=false;
			             }
		           
		             /* if( quantityItems.equals(allquantityshipped[item]) ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+allquantityshipped[item]+" from NAS file ", "Matched with the value found in RDS DB "+quantityItems+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+allquantityshipped[item]+" from NAS file ", "Did not match with the value found in RDS DB "+quantityItems+ "for item "+itemid[item], ResultPath);
				    		 res=false;
			             }
		              */
		              
		              int lastquantity;
		              if(ItemDetailsListuom.contains("CS")){
		               lastquantity= itemcaseSize * Integer.parseInt(ship[item]);// only for case... For eaches remove the itemcaseSize
	    	        }
		              
		              
		              else{
		               lastquantity= Integer.parseInt(ship[item]);
	    	        }
		              if( Integer.parseInt(quantityItems) == lastquantity ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+lastquantity+"", "Matched with the value found in RDS DB "+quantityItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
			            	 
			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+lastquantity+"", "Did not match with the value found in RDS DB "+quantityItems+ " for item "+itemid[item], ResultPath,xwpfRun,"");
				    		 res=false;
			             }
		              
		              
		              if( productId.equals(MinList.get(item)) ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+MinList.get(item)+" from NAS file ", "Matched with the value found in RDS DB "+productId+ "for item "+itemid[item], ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+MinList.get(item)+" from NAS file ", "Did not match with the value found in RDS DB "+productId+ "for item "+itemid[item], ResultPath,xwpfRun,"");
				    		 res=false;
			             }
		              
		              /*
		              	 int initialprice = Integer.parseInt(priceConfirmedEachesAmountList.get(item));
			             int vatamount = Integer.parseInt(priceConfirmedEachesAmountList.get(item)) * vat;
			             int finalprice = initialprice+vatamount;
			             String finalvalue = Integer.toString(finalprice);
			             BigDecimal bigDecimal = new BigDecimal(finalvalue);
			             BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		              
		              
		              if( amountretailPrice.equals(roundedWithScale) ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+priceConfirmedEachesAmountList.get(item)+" from NAS file ", "Matched with the value found in RDS DB "+amountretailPrice+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+priceConfirmedEachesAmountList.get(item)+" from NAS file ", "Did not match with the value found in RDS DB "+amountretailPrice+ "for item "+itemid[item], ResultPath);
				    		 res=false;
			             }
		              */
		              /*
		              double initialprice = Double.parseDouble(priceConfirmedEachesAmountList.get(item));
			             
			             String variant=String.valueOf(initialprice);
			             
			             String[] splitter = variant.split("\\.");

			            // d.toString().split("\\.");
			            
			             splitter[0].length();   // Before Decimal Count
			             
			           String decPoint=  String.valueOf(splitter[1].length())     ;   // After  Decimal Count
			           
			           
			           String finalamount=null;
			             
			             
			            	 
			            	    
			            	 double vatint = Double.parseDouble(vatrate.get(item));
				             System.out.println("price calculation starts: ");
				             System.out.println("initialprice: "+initialprice);
				             double vatamount = initialprice * vatint;
				             vatamount = vatamount/100;
				             BigDecimal vatbig = new BigDecimal(vatamount);
				             BigDecimal roundedWithScale2 = vatbig.setScale(2, BigDecimal.ROUND_HALF_UP);
				             String vatamount2 = roundedWithScale2.toString();
				             double finalvatamount = Double.parseDouble(vatamount2);
				             System.out.println("vatamount: "+vatamount);
				             double finalprice = initialprice+finalvatamount;
				             System.out.println("finalprice: "+finalprice);
				             String finalvalue = Double.toString(finalprice);
				             BigDecimal bigDecimal = new BigDecimal(finalvalue);
				             //BigDecimal roundedWithScale = bigDecimal.setScale(decpo, BigDecimal.ROUND_HALF_UP); //Round of by number of digits after decimal
				             BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
				             finalamount = roundedWithScale.toString();
			             
			             
			             */
			             
			             
			             /*
			             if(decPoint.contentEquals("3")){
				             
			            	 double vatint = Double.parseDouble(vatrate.get(item));
					             System.out.println("price calculation starts: ");
					             System.out.println("initialprice: "+initialprice);
					             double vatamount = initialprice * vatint;
					             System.out.println("vatamount: "+vatamount);
					             double finalprice = initialprice+vatamount;
					             System.out.println("finalprice: "+finalprice);
					             String finalvalue = Double.toString(finalprice);
					             BigDecimal bigDecimal = new BigDecimal(finalvalue);
					             BigDecimal roundedWithScale = bigDecimal.setScale(3, BigDecimal.ROUND_HALF_UP);
					            finalamount = roundedWithScale.toString();
			             
			             
			             }
			             
			             
			             
			             if(decPoint.contentEquals("2")){
				             
			            	 double vatint = Double.parseDouble(vatrate.get(item));
					             System.out.println("price calculation starts: ");
					             System.out.println("initialprice: "+initialprice);
					             double vatamount = initialprice * vatint;
					             System.out.println("vatamount: "+vatamount);
					             double finalprice = initialprice+vatamount;
					             System.out.println("finalprice: "+finalprice);
					             String finalvalue = Double.toString(finalprice);
					             BigDecimal bigDecimal = new BigDecimal(finalvalue);
					             BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
					              finalamount = roundedWithScale.toString();
			             
			             
			             }
			             
			            */
			         
			      //----------------------------priceConfirmedEachesAmountExtract validation-------------------        
		              double initialprice = Double.parseDouble(priceConfirmedEachesAmountList.get(item));
			    		BigDecimal bigDecimal = new BigDecimal(initialprice);
			            //BigDecimal roundedWithScale = bigDecimal.setScale(decpo, BigDecimal.ROUND_HALF_UP); //Round of by number of digits after decimal
			            BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
			            String finalamount = roundedWithScale.toString();
			            System.out.println(finalamount);
			              
			              
			             if( amountretailPrice.equals(finalamount) ) {
				            	 
				            	 //true;
				            	 //ItemDetailsList
				            	 System.out.println("positive checking");
				            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+finalamount+"", "Matched with the value found in RDS DB "+amountretailPrice+ " for item "+itemid[item], ResultPath,xwpfRun,"");

				             }
				             else {
				            	 System.out.println("negative checking");
				            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+finalamount+"", "Did not match with the value found in RDS DB "+amountretailPrice+ " for item "+itemid[item], ResultPath,xwpfRun,"");
					    		 res=false;
				             }
			             
			    //---------------------------------------------------------------------------------------------          
		              
		              if(currencyretailPrice.equals("GBP")) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice from NAS file ", "Matched with the value found in RDS DB "+currencyretailPrice+ "for item "+itemid[item], ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+currencyretailPrice+ "for item "+itemid[item], ResultPath,xwpfRun,"");
				    		 res=false;
			             }
		              
		              
		            //getting tenders attributes
			             JsonArray tendersJsonArray = jsonobjectTransactions.getAsJsonArray("tenders");
			             //System.out.println("tendersJsonArray" + tendersJsonArray.size());
			             JsonObject jsonobjectTenders = tendersJsonArray.get(0).getAsJsonObject();
			             String tendersjsonBody = jsonobjectTenders.get("tenderamount").toString();
			             
			             System.out.println(tendersjsonBody);

			             JsonArray tenderamountJsonArray = jsonobjectTenders.getAsJsonArray("tenderamount");
			             System.out.println(tenderamountJsonArray.size());
			             JsonObject jsonobjectTendersAmount = tenderamountJsonArray.get(0).getAsJsonObject();
			             
			             String amountTenderAmount = jsonobjectTendersAmount.get("amount").toString().replaceAll("^\"|\"$", "");
			         
			             amountTenderAmount = amountTenderAmount.replaceAll(".", "");

			             excelCellValueWrite.writeValueToCell(amountTenderAmount, r, 43, testdatapath, sheetname);
			             
			             jsonobjectTransactions.get("totalSalesAmt").toString();
			             
			             JsonArray totalSalesAmtJsonArray = jsonobjectTransactions.getAsJsonArray("totalSalesAmt");

			             
			             //JsonElement jsonelementtotalSalesAmt = new JsonParser().parse(totalSalesAmtjsonBody);
			             JsonObject jsonobjecttotalSalesAmt = totalSalesAmtJsonArray.get(0).getAsJsonObject();
			             
			             //getting totalSalesAmt attributes
			             String amounttotalSalesAmt = jsonobjecttotalSalesAmt.get("amount").toString().replaceAll("^\"|\"$", "");
			             jsonobjecttotalSalesAmt.get("currency").toString().replaceAll("^\"|\"$", "");
			             
			             System.out.println(amounttotalSalesAmt);
			             amounttotalSalesAmt = amounttotalSalesAmt.replaceAll("\\.", "");
				           System.out.println("Amount tender amount: "+amounttotalSalesAmt);
				           
				           System.out.println("Amount tender amount: "+amountTenderAmount);
				           String date = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
				           System.out.println(date);
			             
			             if( resultlocationId.equals("29369")) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: 9376 from NAS file ", "Matched with the value found in RDS DB "+resultlocationId, ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: 9376 from NAS file ", "Did not match with the value found in RDS DB "+resultlocationId, ResultPath,xwpfRun,"");
				    		 res=false;
			             }
			             
			             
			             
			             if( typeTransactions.equals(order_type_source)) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+typeTransactions, ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+typeTransactions, ResultPath,xwpfRun,"");
				    		 res=false;
			             }
			             
			             if( sourceTransactions.equals("WEB")) {
		 	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Matched with the value found in RDS DB "+sourceTransactions, ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Did not match with the value found in RDS DB "+sourceTransactions, ResultPath,xwpfRun,"");
			            	 res=false;
			             }
			             
		  
			             if( referenceTransactions.equals(channel_reference)) {
		 	 
			             //true;
			             //ItemDetailsList
			             System.out.println("positive checking");
			             SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Matched with the value found in RDS DB "+referenceTransactions, ResultPath,xwpfRun,"");

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Did not match with the value found in RDS DB "+referenceTransactions, ResultPath,xwpfRun,"");
			            	 res=false;
			             }
		              
		              
		              
			             
			             excelCellValueWrite.writeValueToCell(date, r, 46, testdatapath, sheetname);
			          excelCellValueWrite.writeValueToCell(amounttotalSalesAmt, r, 43, testdatapath, sheetname);
		              excelCellValueWrite.writeValueToCell(productId+",", r, 44, testdatapath, sheetname);
		              excelCellValueWrite.writeValueToCell(quantityItems+",", r, 45, testdatapath, sheetname);
	             
	             
	             /*//getting tenders attributes
	             JsonArray tendersJsonArray = jsonobjectTransactions.getAsJsonArray("tenders");
	             //System.out.println("tendersJsonArray" + tendersJsonArray.size());
	             JsonObject jsonobjectTenders = tendersJsonArray.get(0).getAsJsonObject();
	             String tendersjsonBody = jsonobjectTenders.get("tenderamount").toString();
	             
	             System.out.println(tendersjsonBody);

	             JsonArray tenderamountJsonArray = jsonobjectTenders.getAsJsonArray("tenderamount");
	             System.out.println(tenderamountJsonArray.size());
	             JsonObject jsonobjectTendersAmount = tenderamountJsonArray.get(0).getAsJsonObject();
	             
	             String amountTenderAmount = jsonobjectTendersAmount.get("amount").toString().replaceAll("^\"|\"$", "");
	         
	             amountTenderAmount = amountTenderAmount.replaceAll(".", "");

	             excelCellValueWrite.writeValueToCell(amountTenderAmount, r, 43, testdatapath, sheetname);
	             
	             String totalSalesAmtjsonBody = jsonobjectTransactions.get("totalSalesAmt").toString();
	             
	             JsonArray totalSalesAmtJsonArray = jsonobjectTransactions.getAsJsonArray("totalSalesAmt");

	             
	             //JsonElement jsonelementtotalSalesAmt = new JsonParser().parse(totalSalesAmtjsonBody);
	             JsonObject jsonobjecttotalSalesAmt = totalSalesAmtJsonArray.get(0).getAsJsonObject();
	             
	             //getting totalSalesAmt attributes
	             String amounttotalSalesAmt = jsonobjecttotalSalesAmt.get("amount").toString().replaceAll("^\"|\"$", "");
	             String currencytotalSalesAmt = jsonobjecttotalSalesAmt.get("currency").toString().replaceAll("^\"|\"$", "");
	             

	             
	             if( resultlocationId.equals(Payload_map.get("locationId"))) {
	            	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: "+Payload_map.get("locationId")+" from NAS file ", "Matched with the value found in RDS DB "+resultlocationId, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: "+Payload_map.get("locationId")+" from NAS file ", "Did not match with the value found in RDS DB "+resultlocationId, ResultPath);
		    		 res=false;
	             }
	             
	             
	             
	             if( typeTransactions.equals(order_type_source)) {
	            	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+typeTransactions, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+typeTransactions, ResultPath);
		    		 res=false;
	             }
	             
	             if( sourceTransactions.equals("WEB")) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Matched with the value found in RDS DB "+sourceTransactions, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Did not match with the value found in RDS DB "+sourceTransactions, ResultPath);
	            	 res=false;
	             }
	             

	             if( referenceTransactions.equals(channel_reference)) {
	 
	             //true;
	             //ItemDetailsList
	             System.out.println("positive checking");
	             Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Matched with the value found in RDS DB "+referenceTransactions, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Did not match with the value found in RDS DB "+referenceTransactions, ResultPath);
	            	 res=false;
	             }*/
	             
	             /*if( uomItems.equals(ItemDetailsListuom)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListuom: "+ItemDetailsListuom+" from NAS file ", "Matched with the value found in RDS DB "+uomItems, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListuom: "+ItemDetailsListuom+" from NAS file ", "Did not match with the value found in RDS DB "+uomItems, ResultPath);
	            	 res=false;
	             }
	             
	             if( statusItems.equals(order_type_source)) {
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+statusItems, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+statusItems, ResultPath);
	            	 res=false;
	             }
	             
	             if( quantityItems.equals(ItemDetailsListquantity)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+ItemDetailsListquantity+" from NAS file ", "Matched with the value found in RDS DB "+quantityItems, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+ItemDetailsListquantity+" from NAS file ", "Did not match with the value found in RDS DB "+quantityItems, ResultPath);
	            	 res=false;
	             }
	             
	             if( productId.equals(ItemDetailsListItemId)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+ItemDetailsListItemId+" from NAS file ", "Matched with the value found in RDS DB "+productId, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+ItemDetailsListItemId+" from NAS file ", "Did not match with the value found in RDS DB "+productId, ResultPath);
	            	 res=false;
	             }
	             
	             if( currencyretailPrice.equals(ItemDetailspriceOrderedCurrency)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Matched with the value found in RDS DB "+currencyretailPrice, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Did not match with the value found in RDS DB "+currencyretailPrice, ResultPath);
	            	 res=false;
	             }
	             
	             if( currencytotalSalesAmt.equals(ItemDetailspriceOrderedCurrency)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencytotalSalesAmt: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Matched with the value found in RDS DB "+currencytotalSalesAmt, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencytotalSalesAmt: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Did not match with the value found in RDS DB "+currencytotalSalesAmt, ResultPath);
	            	 res=false;
	             }*/

	    	 }
	    	 
	    	 if(!columnname[col].equals("sales_payload")) {
	    		 
	    		 if(nasres.equalsIgnoreCase(finalres))
		    	 {
		    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Matched with the value found in RDS DB "+finalres, ResultPath,xwpfRun,"");
		    		 
		    		 
		    	 }
		    	 else
		    	 {		
		    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Did not match with the value found in RDS DB "+finalres, ResultPath,xwpfRun,"");
		    		 res=false;
		    	 }
	    	 }
	    	 
	    	 
	    	 
	    	 
	        }
	        }
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);
SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "Error occurred ", "Validation in RDS DB ", ResultPath,xwpfRun,"");

res = false;
return res;

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		wrk1.close();
	      return res;
	}

	}
	
	
	
	
	
	
	
	public static boolean validateInRDS_Multiple_Negative_zero_ship(String orderid,String[] columnname, String[] itemid, HashMap<String, String> Payload_map, ArrayList<String> ItemDetailsList, ArrayList<String> priceConfirmedEachesAmountList, ArrayList<String> MinList, String ResultPath, String tcid, int r, String testdatapath ,  String sheetname, ArrayList<String> vatrate,String[] ship,String ItemDetailsSheetName,int row) throws JSchException, BiffException, IOException{
		 
		 Channel channel=null;
		 Session session=null;
		 
		 String order_type_source = "SALE";
		 String channel_reference = "WHOLESALE";
		 String sales_process_status = "pending";
		 
		 boolean res=true;
		 
		 //ArrayList<Map> itemlist=list;
		 int itemcaseSize = 0;
	     
	     String QResult="";
	     
	     
	     
	     ItemDetailsList = new ArrayList<String>();
	 	Workbook wrk1 = Workbook.getWorkbook(new File(testdatapath));
		
		Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
		
		
	
			String ItemIDs = sheet2.getCell(1, row).getContents().trim();
				  
				 
				
				String[] AllItemsInItemDetails = ItemIDs.split(",");
				
				//System.out.println("Number of ItemIDs " + ItemID.length);
				System.out.println("Items with details " + AllItemsInItemDetails.length);
				
				for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
					
					String ItemDetailsJson = sheet2.getCell(c, row).getContents().trim();
					System.out.println("Adding " + ItemDetailsJson);
					ItemDetailsList.add(ItemDetailsJson);
					
					
				}

		


	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              //String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user api_space_v1_owner_sit";

	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(true);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        //out.write(("user124676r4"+"\n").getBytes());
	        out.write(("api_space_v1_owner_sit999"+"\n").getBytes());

	        out.flush();
	        
	        

	        for(int col=0;col<columnname.length;col++)
	        {
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     //out.write(("select distinct "+columnname[col]+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     out.write(("select distinct "+columnname[col]+" from storepick_sales_service.SALES_DETAILS where order_id ='"+orderid+"';\n").getBytes());

	     //select * from storepick_sales_service.SALES_DETAILS;	
	     	
	        out.flush();
	        
	        //Thread.sleep(1000); 
	        
	        
	        
	        Thread.sleep(5000);
	        
	     // channel.setOutputStream(System.out);
	        
	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[4096];
	           
	        int i=0; 
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 4096);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	              System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
			 System.out.println("QResult is "+QResult+" is this!!!");
			 
			 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid,"Query result screenshot is: " ,QResult , ResultPath);
			 
			 
			 
			 
			 

	        String[] rest= QResult.split(columnname[col]);

			String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 
			System.out.println("Testing "+justfinalres.replaceAll("[\\r\\n]+", "").replaceAll("[\\.\\.]+", ""));
			
			//System.out.println(justfinalres);
			String[] rest2=justfinalres.split("[\\r\\n]+");
			//System.out.println(rest2.length);
			 
			String finalres = "";
			for(int j=0; j<rest2.length; j++) {
				 
				 if(rest2[j].contains("(1 row)")) {
					 break;
				 }
				 
				 finalres = finalres + rest2[j].trim();
			 }
			 
			 finalres = finalres.replaceAll("\\.\\.", "");
			 
			 System.out.println("finalres is "+finalres);
	    
	    	 //String finalres=rest2[1].trim();
	    	 System.out.println(columnname[col]);
	    	 System.out.println(finalres);
	    	 
	    	//System.out.println(itemlist.get(item).toString());
	    	 System.out.println(columnname[col]);
	    	 //String nasres=itemlist.get(item).get(columnname[col]).toString().replaceAll("[-+|]", "");
	    	 
	    	/*JsonElement jsonelementItemDetailsList = new JsonParser().parse(ItemDetailsList.get(item));
	    	//System.out.println(ItemDetailsList.get(item));
          JsonObject jsonobjectItemDetailsList = jsonelementItemDetailsList.getAsJsonObject();
          
          //getting itemdetailslist attributes
          String ItemDetailsListuom = jsonobjectItemDetailsList.get("quantityType").toString().replaceAll("^\"|\"$", "");
          String ItemDetailsListquantity = jsonobjectItemDetailsList.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");	
	    	String ItemDetailsListItemId = jsonobjectItemDetailsList.get("itemId").toString().replaceAll("^\"|\"$", "");
	    	String ItemDetailspriceOrderedCurrency = jsonobjectItemDetailsList.get("priceOrderedCurrency").toString().replaceAll("^\"|\"$", "");
	    	String ItemDetailsitemcaseSize = jsonobjectItemDetailsList.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
	    	
	    	itemcaseSize = Integer.parseInt(ItemDetailsitemcaseSize);*/
	    			
	    	 String nasres = null;
	    	 
	    	 if(columnname[col].equalsIgnoreCase("order_id")) {
	    		 nasres = orderid;
	    	 }

	    	 
	    	 if(columnname[col].equalsIgnoreCase("order_type")) {
	    		 nasres = order_type_source;
	    	 }
	    	 
	    	 if(columnname[col].equals("channel")) 
	    	 {
	    		 nasres = channel_reference;
	    	 }
	    	 
	    	 if(columnname[col].equals("sales_process_status")) 
	    	 {
	    		 nasres = sales_process_status;
	    	 }
	    	 
	    	 if(columnname[col].equals("sales_payload")) 
	    	 {
	    		 System.out.println("finalres for sales_payload " + finalres);
	    		 
	    		 
	    		 JsonElement jsonelement = new JsonParser().parse(finalres);
	             JsonObject jsonobject = jsonelement.getAsJsonObject();
	                
	             String resultlocationId = jsonobject.get("locationId").toString().replaceAll("^\"|\"$", "");
	             
	             jsonobject.get("transactions").toString();
	             
	             
	             //JsonElement jsonelementTransactions = new JsonParser().parse(transactionsJsonBody);
	             
	             JsonArray transactionsJsonArray = jsonobject.getAsJsonArray("transactions");
	             
	             JsonObject jsonobjectTransactions = transactionsJsonArray.get(0).getAsJsonObject();
	             
	             //getting transaction attributes
	             String typeTransactions = jsonobjectTransactions.get("type").toString().replaceAll("^\"|\"$", "");
	             String sourceTransactions = jsonobjectTransactions.get("source").toString().replaceAll("^\"|\"$", "");
	             String referenceTransactions = jsonobjectTransactions.get("reference").toString().replaceAll("^\"|\"$", "");
	             jsonobjectTransactions.get("transactionId").toString().replaceAll("^\"|\"$", "");
	             
	             jsonobjectTransactions.get("items").toString();
	             JsonArray itemsJsonArray = jsonobjectTransactions.getAsJsonArray("items");
	             
	             String uomItems = null;
	             String statusItems = null;
	             String quantityItems = null;
	             String productId = null;
	             
	             String currencyretailPrice = null;

	             
	 	        for(int item=0;item<itemid.length;item++)
		        {
	            	 
	            	 JsonElement jsonelementItemDetailsList = new JsonParser().parse(ItemDetailsList.get(item));
	     	    	//System.out.println(ItemDetailsList.get(item));
	                 JsonObject jsonobjectItemDetailsList = jsonelementItemDetailsList.getAsJsonObject();
	                 
	                 //getting itemdetailslist attributes
	                 String ItemDetailsListuom = jsonobjectItemDetailsList.get("quantityType").toString().replaceAll("^\"|\"$", "");
	                 jsonobjectItemDetailsList.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");	
	     	    	jsonobjectItemDetailsList.get("itemId").toString().replaceAll("^\"|\"$", "");
	     	    	jsonobjectItemDetailsList.get("priceOrderedCurrency").toString().replaceAll("^\"|\"$", "");
	     	    	String ItemDetailsitemcaseSize = jsonobjectItemDetailsList.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
	     	    	
	     	    	itemcaseSize = Integer.parseInt(ItemDetailsitemcaseSize);
	            	 
	            	//JsonElement jsonelementItems = new JsonParser().parse(itemsjsonBody);
	     	    	JsonObject jsonobjectItems = null;
	     	    	try {
			              jsonobjectItems = itemsJsonArray.get(item).getAsJsonObject();

					} catch (IndexOutOfBoundsException e) {
						// TODO: handle exception
						SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The  item "+MinList.get(item), "Did not found in RDS DB ", ResultPath);
						
						break;
					}
		             
		           //getting item attributes
		              uomItems = jsonobjectItems.get("uom").toString().replaceAll("^\"|\"$", "");
		              statusItems = jsonobjectItems.get("status").toString().replaceAll("^\"|\"$", "");
		              quantityItems = jsonobjectItems.get("quantity").toString().replaceAll("^\"|\"$", "");
		              productId = jsonobjectItems.get("productId").toString().replaceAll("^\"|\"$", "");
		             
		             jsonobjectItems.get("retailPrice").toString();
		             
		             JsonArray retailPriceJsonArray = jsonobjectItems.getAsJsonArray("retailPrice");
		             
		          // JsonElement jsonelementretailPrice = new JsonParser().parse(retailPricejsonBody);
		             JsonObject jsonobjectretailPrice = retailPriceJsonArray.get(0).getAsJsonObject();
		             
		             jsonobjectretailPrice.get("amount").toString().replaceAll("^\"|\"$", "");
		              currencyretailPrice = jsonobjectretailPrice.get("currency").toString().replaceAll("^\"|\"$", "");
		              
		              
		              
		              if(uomItems.equals("EA") ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
				    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of uom:  from NAS file ", "Matched with the value found in RDS DB "+uomItems+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of uom: "+ItemDetailsListuom+" from NAS file ", "Did not match with the value found in RDS DB "+uomItems+ "for item "+itemid[item], ResultPath);
				    		 
			             }
		              
		              if( statusItems.equals(order_type_source)) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+statusItems+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+statusItems+ "for item "+itemid[item], ResultPath);
				    		 
			             }
		              //Logic1
		              int lastquantity;
		              if(ItemDetailsListuom.contains("CS")){
		               lastquantity= itemcaseSize * Integer.parseInt(ship[item]);// only for case... For eaches remove the itemcaseSize
	    	        }
		              
		              
		              else{
		               lastquantity= Integer.parseInt(ship[item]);
	    	        }
		              if( Integer.parseInt(quantityItems) == lastquantity ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+lastquantity+"", "Matched with the value found in RDS DB "+quantityItems+ " for item "+itemid[item], ResultPath);
			            	 
			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+lastquantity+"", "Did not match with the value found in RDS DB "+quantityItems+ " for item "+itemid[item], ResultPath);
				    		 res=false;
			             }
		              
		              /*if( Integer.parseInt(quantityItems) == Integer.parseInt(ItemDetailsListquantity) ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+Integer.parseInt(ItemDetailsListquantity)+" from NAS file ", "Matched with the value found in RDS DB "+quantityItems+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+Integer.parseInt(ItemDetailsListquantity)+" from NAS file ", "Did not match with the value found in RDS DB "+quantityItems+ "for item "+itemid[item], ResultPath);
				    		 
			             }
		              */
		              
		              if( productId.equals(MinList.get(item)) ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+MinList.get(item)+" from NAS file ", "Matched with the value found in RDS DB "+productId+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+MinList.get(item)+" from NAS file ", "Did not match with the value found in RDS DB "+productId+ "for item "+itemid[item], ResultPath);
				    		 
			             }
		              //Logic2
		             /* 
		              int initialprice = Integer.parseInt(priceConfirmedEachesAmountList.get(item));
			             int vatamount = Integer.parseInt(priceConfirmedEachesAmountList.get(item)) * vat;
			             int finalprice = initialprice+vatamount;
			             String finalvalue = Integer.toString(finalprice);
			             BigDecimal bigDecimal = new BigDecimal(finalvalue);
			             BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		              
		             
		              if( amountretailPrice.equals(roundedWithScale) ) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+priceConfirmedEachesAmountList.get(item)+" from NAS file ", "Matched with the value found in RDS DB "+amountretailPrice+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+priceConfirmedEachesAmountList.get(item)+" from NAS file ", "Did not match with the value found in RDS DB "+amountretailPrice+ "for item "+itemid[item], ResultPath);
				    		 
			             }*/
		              
		              
		              double initialprice = Double.parseDouble(priceConfirmedEachesAmountList.get(item));
			             
			             String variant=String.valueOf(initialprice);
			             
			             String[] splitter = variant.split("\\.");

			            // d.toString().split("\\.");
			            
			             splitter[0].length();   // Before Decimal Count
			             
			           String decPoint=  String.valueOf(splitter[1].length())     ;   // After  Decimal Count
			           
			           
			           if(decPoint.contentEquals("4")){
			            	 double vatint = Double.parseDouble(vatrate.get(item));
			            	    
					             System.out.println("price calculation starts: ");
					             System.out.println("initialprice: "+initialprice);
					             double vatamount = initialprice * vatint;
					             System.out.println("vatamount: "+vatamount);
					             double finalprice = initialprice+vatamount;
					             System.out.println("finalprice: "+finalprice);
					             String finalvalue = Double.toString(finalprice);
					             BigDecimal bigDecimal = new BigDecimal(finalvalue);
					             BigDecimal roundedWithScale = bigDecimal.setScale(4, BigDecimal.ROUND_HALF_UP);
					            roundedWithScale.toString();
			             
			             
			             }
			             
			             
			             
			             if(decPoint.contentEquals("3")){
				             
			            	 double vatint = Double.parseDouble(vatrate.get(item));
					             System.out.println("price calculation starts: ");
					             System.out.println("initialprice: "+initialprice);
					             double vatamount = initialprice * vatint;
					             System.out.println("vatamount: "+vatamount);
					             double finalprice = initialprice+vatamount;
					             System.out.println("finalprice: "+finalprice);
					             String finalvalue = Double.toString(finalprice);
					             BigDecimal bigDecimal = new BigDecimal(finalvalue);
					             BigDecimal roundedWithScale = bigDecimal.setScale(3, BigDecimal.ROUND_HALF_UP);
					            roundedWithScale.toString();
			             
			             
			             }
			             
			             
			             
			             if(decPoint.contentEquals("2")){
				             
			            	 double vatint = Double.parseDouble(vatrate.get(item));
					             System.out.println("price calculation starts: ");
					             System.out.println("initialprice: "+initialprice);
					             double vatamount = initialprice * vatint;
					             System.out.println("vatamount: "+vatamount);
					             double finalprice = initialprice+vatamount;
					             System.out.println("finalprice: "+finalprice);
					             String finalvalue = Double.toString(finalprice);
					             BigDecimal bigDecimal = new BigDecimal(finalvalue);
					             BigDecimal roundedWithScale = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
					              roundedWithScale.toString();
			             
			             
			             }
			             
			            
			         
			        //-----------------------------------priceConfirmedEachesAmountExtract validation--------------------      
			             
			            /* if( amountretailPrice.equals(finalamount) ) {
				            	 
				            	 //true;
				            	 //ItemDetailsList
				            	 System.out.println("positive checking");
				            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+finalamount+"", "Matched with the value found in RDS DB "+amountretailPrice+ " for item "+itemid[item], ResultPath);

				             }
				             else {
				            	 System.out.println("negative checking");
				            	 Utilities_I32a.utilityFileWriteOP.writeToLog(tcid, "The value of priceConfirmedEachesAmountExtract: "+finalamount+"", "Did not match with the value found in RDS DB "+amountretailPrice+ " for item "+itemid[item], ResultPath);
					    		 res=false;
				             }*/
			         //-----------------------------------------------------------------------------------------------------------     
		              
		              if(currencyretailPrice.equals("GBP")) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice from NAS file ", "Matched with the value found in RDS DB "+currencyretailPrice+ "for item "+itemid[item], ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+currencyretailPrice+ "for item "+itemid[item], ResultPath);
				    		 
			             }
		              
		              
		            //getting tenders attributes
			             JsonArray tendersJsonArray = jsonobjectTransactions.getAsJsonArray("tenders");
			             //System.out.println("tendersJsonArray" + tendersJsonArray.size());
			             JsonObject jsonobjectTenders = tendersJsonArray.get(0).getAsJsonObject();
			             String tendersjsonBody = jsonobjectTenders.get("tenderamount").toString();
			             
			             System.out.println(tendersjsonBody);

			             JsonArray tenderamountJsonArray = jsonobjectTenders.getAsJsonArray("tenderamount");
			             System.out.println(tenderamountJsonArray.size());
			             JsonObject jsonobjectTendersAmount = tenderamountJsonArray.get(0).getAsJsonObject();
			             
			             String amountTenderAmount = jsonobjectTendersAmount.get("amount").toString().replaceAll("^\"|\"$", "");
			         
			             amountTenderAmount = amountTenderAmount.replaceAll(".", "");

			             excelCellValueWrite.writeValueToCell(amountTenderAmount, r, 43, testdatapath, sheetname);
			             
			             jsonobjectTransactions.get("totalSalesAmt").toString();
			             
			             JsonArray totalSalesAmtJsonArray = jsonobjectTransactions.getAsJsonArray("totalSalesAmt");

			             
			             //JsonElement jsonelementtotalSalesAmt = new JsonParser().parse(totalSalesAmtjsonBody);
			             JsonObject jsonobjecttotalSalesAmt = totalSalesAmtJsonArray.get(0).getAsJsonObject();
			             
			             //getting totalSalesAmt attributes
			             String amounttotalSalesAmt = jsonobjecttotalSalesAmt.get("amount").toString().replaceAll("^\"|\"$", "");
			             jsonobjecttotalSalesAmt.get("currency").toString().replaceAll("^\"|\"$", "");
			             

			             
			             if( resultlocationId.equals("9376")) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: 9376 from NAS file ", "Matched with the value found in RDS DB "+resultlocationId, ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: 9376 from NAS file ", "Did not match with the value found in RDS DB "+resultlocationId, ResultPath);
				    		 
			             }
			             
			             
			             
			             if( typeTransactions.equals(order_type_source)) {
			            	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+typeTransactions, ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+typeTransactions, ResultPath);
				    		 
			             }
			             
			             if( sourceTransactions.equals("WEB")) {
		 	 
			            	 //true;
			            	 //ItemDetailsList
			            	 System.out.println("positive checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Matched with the value found in RDS DB "+sourceTransactions, ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Did not match with the value found in RDS DB "+sourceTransactions, ResultPath);
			            	 
			             }
			             
		  
			             if( referenceTransactions.equals(channel_reference)) {
		 	 
			             //true;
			             //ItemDetailsList
			             System.out.println("positive checking");
			             SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Matched with the value found in RDS DB "+referenceTransactions, ResultPath);

			             }
			             else {
			            	 System.out.println("negative checking");
			            	 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Did not match with the value found in RDS DB "+referenceTransactions, ResultPath);
			            	 
			             }
		              
		              
		              
			             
			          excelCellValueWrite.writeValueToCell(amounttotalSalesAmt, r, 43, testdatapath, sheetname);
		              excelCellValueWrite.writeValueToCell(productId+",", r, 44, testdatapath, sheetname);
		              excelCellValueWrite.writeValueToCell(quantityItems+",", r, 45, testdatapath, sheetname);
		        }
	             
	             /*//getting tenders attributes
	             JsonArray tendersJsonArray = jsonobjectTransactions.getAsJsonArray("tenders");
	             //System.out.println("tendersJsonArray" + tendersJsonArray.size());
	             JsonObject jsonobjectTenders = tendersJsonArray.get(0).getAsJsonObject();
	             String tendersjsonBody = jsonobjectTenders.get("tenderamount").toString();
	             
	             System.out.println(tendersjsonBody);

	             JsonArray tenderamountJsonArray = jsonobjectTenders.getAsJsonArray("tenderamount");
	             System.out.println(tenderamountJsonArray.size());
	             JsonObject jsonobjectTendersAmount = tenderamountJsonArray.get(0).getAsJsonObject();
	             
	             String amountTenderAmount = jsonobjectTendersAmount.get("amount").toString().replaceAll("^\"|\"$", "");
	         
	             amountTenderAmount = amountTenderAmount.replaceAll(".", "");

	             excelCellValueWrite.writeValueToCell(amountTenderAmount, r, 43, testdatapath, sheetname);
	             
	             String totalSalesAmtjsonBody = jsonobjectTransactions.get("totalSalesAmt").toString();
	             
	             JsonArray totalSalesAmtJsonArray = jsonobjectTransactions.getAsJsonArray("totalSalesAmt");

	             
	             //JsonElement jsonelementtotalSalesAmt = new JsonParser().parse(totalSalesAmtjsonBody);
	             JsonObject jsonobjecttotalSalesAmt = totalSalesAmtJsonArray.get(0).getAsJsonObject();
	             
	             //getting totalSalesAmt attributes
	             String amounttotalSalesAmt = jsonobjecttotalSalesAmt.get("amount").toString().replaceAll("^\"|\"$", "");
	             String currencytotalSalesAmt = jsonobjecttotalSalesAmt.get("currency").toString().replaceAll("^\"|\"$", "");
	             

	             
	             if( resultlocationId.equals(Payload_map.get("locationId"))) {
	            	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: "+Payload_map.get("locationId")+" from NAS file ", "Matched with the value found in RDS DB "+resultlocationId, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of resultlocationId: "+Payload_map.get("locationId")+" from NAS file ", "Did not match with the value found in RDS DB "+resultlocationId, ResultPath);
		    		 res=false;
	             }
	             
	             
	             
	             if( typeTransactions.equals(order_type_source)) {
	            	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+typeTransactions, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+typeTransactions, ResultPath);
		    		 res=false;
	             }
	             
	             if( sourceTransactions.equals("WEB")) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Matched with the value found in RDS DB "+sourceTransactions, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of sourceTransactions:  from NAS file ", "Did not match with the value found in RDS DB "+sourceTransactions, ResultPath);
	            	 res=false;
	             }
	             

	             if( referenceTransactions.equals(channel_reference)) {
	 
	             //true;
	             //ItemDetailsList
	             System.out.println("positive checking");
	             Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Matched with the value found in RDS DB "+referenceTransactions, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of channel_reference: "+channel_reference+" from NAS file ", "Did not match with the value found in RDS DB "+referenceTransactions, ResultPath);
	            	 res=false;
	             }*/
	             
	             /*if( uomItems.equals(ItemDetailsListuom)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListuom: "+ItemDetailsListuom+" from NAS file ", "Matched with the value found in RDS DB "+uomItems, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListuom: "+ItemDetailsListuom+" from NAS file ", "Did not match with the value found in RDS DB "+uomItems, ResultPath);
	            	 res=false;
	             }
	             
	             if( statusItems.equals(order_type_source)) {
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Matched with the value found in RDS DB "+statusItems, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of order_type_source: "+order_type_source+" from NAS file ", "Did not match with the value found in RDS DB "+statusItems, ResultPath);
	            	 res=false;
	             }
	             
	             if( quantityItems.equals(ItemDetailsListquantity)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+ItemDetailsListquantity+" from NAS file ", "Matched with the value found in RDS DB "+quantityItems, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListquantity: "+ItemDetailsListquantity+" from NAS file ", "Did not match with the value found in RDS DB "+quantityItems, ResultPath);
	            	 res=false;
	             }
	             
	             if( productId.equals(ItemDetailsListItemId)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+ItemDetailsListItemId+" from NAS file ", "Matched with the value found in RDS DB "+productId, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of ItemDetailsListItemId: "+ItemDetailsListItemId+" from NAS file ", "Did not match with the value found in RDS DB "+productId, ResultPath);
	            	 res=false;
	             }
	             
	             if( currencyretailPrice.equals(ItemDetailspriceOrderedCurrency)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Matched with the value found in RDS DB "+currencyretailPrice, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencyretailPrice: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Did not match with the value found in RDS DB "+currencyretailPrice, ResultPath);
	            	 res=false;
	             }
	             
	             if( currencytotalSalesAmt.equals(ItemDetailspriceOrderedCurrency)) {
	 
	            	 //true;
	            	 //ItemDetailsList
	            	 System.out.println("positive checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencytotalSalesAmt: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Matched with the value found in RDS DB "+currencytotalSalesAmt, ResultPath);

	             }
	             else {
	            	 System.out.println("negative checking");
	            	 Utilities.utilityFileWriteOP.writeToLog(tcid, "The value of currencytotalSalesAmt: "+ItemDetailspriceOrderedCurrency+" from NAS file ", "Did not match with the value found in RDS DB "+currencytotalSalesAmt, ResultPath);
	            	 res=false;
	             }*/

	    	 }
	    	 
	    	 if(!columnname[col].equals("sales_payload")) {
	    		 
	    		 if(nasres.equalsIgnoreCase(finalres))
		    	 {
		    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Matched with the value found in RDS DB "+finalres, ResultPath);
		    		 
		    		 
		    	 }
		    	 else
		    	 {		
		    		 SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Did not match with the value found in RDS DB "+finalres, ResultPath);
		    		 
		    	 }
	    	 }
	    	 
	    	 
	    	 
	    	 
	        }
	        
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);

res = false;
return res;

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		wrk1.close();
	      return res;
	}

	}

	public static String TestData_OrderIdGeneration(String TestDataPath, String SheetName, String tcid, String Resultpath, int r) throws IOException {
		
		boolean res = false;
		String OrderId=null;
		try {
	    	
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	    	Sheet sheet1 = wrk1.getSheet(SheetName); 
	    	
			
			excelCellValueWrite.writeValueToCell("", r, 16, TestDataPath, SheetName);
			excelCellValueWrite.writeValueToCell("", r, 41, TestDataPath, SheetName);
			excelCellValueWrite.writeValueToCell("", r, 43, TestDataPath, SheetName);
			excelCellValueWrite.writeValueToCell("", r, 44, TestDataPath, SheetName);
			excelCellValueWrite.writeValueToCell("", r, 45, TestDataPath, SheetName);
			excelCellValueWrite.writeValueToCell("", r, 46, TestDataPath, SheetName);
			long currentDateSec = (System.currentTimeMillis() / 1000);
			
			excelCellValueWrite.writeValueToCell(String.valueOf(currentDateSec), r, 16, TestDataPath, SheetName);
			OrderId=String.valueOf(currentDateSec);
			System.out.println("New Order is "+OrderId);
			wrk1.close();
			res = true;
	        } catch (Exception e) {
	        	e.printStackTrace();
	        	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID generation ", "Due to :"+e,Resultpath);
	        	res = false;
	        	return OrderId;
	        }
	    
	    return OrderId;
	    
		
	}

	public static boolean OrderIdGenerationI32a(String TestDataPath, String SheetName, String tcid, String Resultpath, String OrderColumn, String Datecolumn) throws IOException {
		
		boolean res = false;
	
		
		int r = 0;
		
		
		
	    try {
	    	
	    	int rows = 0;
			
	    	Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	    	
			Sheet sheet1 = wrk1.getSheet(SheetName); 
	         
			rows = sheet1.getRows();
			
			//long currentDateSec = (System.currentTimeMillis() / 1000);
			
			
			for(r=1; r<rows; r++) {
	
				String date=SandPiperUtilities.getCurrentDate.getISTDate();
				
				
				
				long currentDateSec = (System.currentTimeMillis() / 1000);
				 excelCellValueWrite.writeValueToCell(String.valueOf(currentDateSec), r, Integer.parseInt(OrderColumn), TestDataPath, SheetName);
				 excelCellValueWrite.writeValueToCell(date, r, Integer.parseInt(Datecolumn), TestDataPath, SheetName);
				 Thread.sleep(1200);
				
			}
			
			wrk1.close();
			
			res = true;
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID generation ", "Due to :"+e,Resultpath);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	    	
	        
	}
	    
	    return res;
	    
		
	}
	//******************************************************
	//Method Name: statusvalidateordersInRDS
	//Method Description: Method to validate different column values of a record
	//Created By: Morrissons TCS Automation Team
	//*******************************************************
	public static boolean statusvalidateordersInRDS(String[] columnname, String[] orderid, String[] columnvalue, String tcid,String DriverSheetPath,XWPFRun xwpfRun) throws JSchException{
		//***************************Initialise Variable***************************
		Channel channel=null;
		Session session=null;
		boolean res=true;
		String QResult="";
		try{      
			
			
			//******************************Setup connection with RDS**************************
			JSch jsch = new JSch();
			java.util.Properties config = new java.util.Properties(); 
			String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user master";
			session = jsch.getSession("mwadmin","10.244.39.83",22);
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("2Bchanged");
			session.connect();
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			InputStream in = channel.getInputStream();
			((ChannelExec)channel).setPty(false);
			OutputStream out=channel.getOutputStream();
			channel.connect();
			Thread.sleep(1000);
	        out.write(("FS4T3fdQCYCym48h"+"\n").getBytes());
	        out.flush();
	        int k=0;
	        int resultflag=0;
	        //*******************Column Data validation*******************************
	        for(int item=0;item<orderid.length;item++)
	        {
	        	for(int attribute=0; attribute<columnname.length; attribute++){
	        		Thread.sleep(1000);
	        		QResult="";
	        		out.flush();
	        		out.write(("select distinct "+columnname[attribute]+" from storepick_sales_service.SALES_DETAILS where order_id='"+orderid[item]+"';\n").getBytes());
	        		out.flush();
	        		Thread.sleep(1000);
	        		byte[] tmp = new byte[2048];
	        		int i=0;  
	        		while (in.available() > 0) {
	        			i = in.read(tmp, 0, 1024);
	        			if (i < 0) {
	        				break;
	        			}
                	}
	         
	        		QResult=new String(tmp, 0, i); 
	        		String[] rest= QResult.split(columnname[attribute]);
	        		String justfinalres=rest[rest.length-1].replaceAll("[+|]", "");
	        		utilityFileWriteOP.writeToLog(tcid, "Query Result", "", DriverSheetPath, xwpfRun, QResult);
	        		String[] rest2=justfinalres.split("[\\r\\n]+");
	        		String finalres="";
	        		for(int restcount=0;restcount<rest2.length;restcount++)
	        		{
	        			if(rest2[restcount].contains("("))
	        			{
	        				if(!rest2[restcount-1].equals(columnname))
	        				{
	        					finalres=rest2[restcount-1].trim();
	        					break;
	        				}
	        			}
	   	 
	        		}
	        		finalres = rest2[2].trim();
	        		for(k=0; k<8;k++){
	        			if(attribute>0 && resultflag==1) {
	        				break;
	        			}
	        			if(finalres.equalsIgnoreCase(columnvalue[attribute].trim()))
	        			{
	        				SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[attribute]+" from POSTMAN "+columnvalue[attribute], "Matched with the value found in RDS DB "+finalres,DriverSheetPath,xwpfRun,"");
	        				System.out.println("The value of "+columnname[attribute]+" from webportal "+columnvalue[attribute]+ "matched with the value found in RDS DB "+finalres);
	        				resultflag=0;
	        				res=true;
	        				break;
	        			}
	        			else
	        			{		
	        				if(finalres.equals("(0 rows)")||finalres.equals(""))
	        				{
	        					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[attribute]+" for messageid "+orderid[item], "Did not match because NO value was found in RDS DB ",DriverSheetPath,xwpfRun,"");
	        					resultflag=1;
	        				}
	        				else
	        				{
	        					SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[attribute]+" for messageid "+orderid[item], "Did not match with the value found in RDS DB "+finalres,DriverSheetPath,xwpfRun,"");
	        					res=false;
	        					break;
	        				}
	   		 
	        				res=false;
	        			}
	        			Thread.sleep(45000);
	        		}
	        	}    
	        }
	       out.flush();
	       out.write(("\\q"+"\n").getBytes());
	       out.flush();
	       Thread.sleep(5000);
	       channel.disconnect();
	       session.disconnect();
	       Thread.sleep(5000);
	       out.close();
	       in.close();
	       
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e);
		}finally{        
			if (channel != null) {
				session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
			}
		return res;
		}
	}

	//******************************************************
	//Method Name: ExtractRTLOGnamefromRDS
	//Method Description: Method to extract RTLOG name from RDS
	//Created By: Morrissons TCS Automation Team
	//*******************************************************
	public static ArrayList<String> ExtractRTLOGnamefromRDS(String[] columnname, String[] orderid, String[] columnvalue, String tcid,String DriverSheetPath) throws JSchException{
		//**************************************Initialise Variable****************************
		Channel channel=null;
		Session session=null;
		ArrayList<String> RTLOGS = new ArrayList<String>();
		boolean res=true;
		String QResult="";
		try{      
			//*************************Establish JSch connection*****************************
			JSch jsch = new JSch();
			java.util.Properties config = new java.util.Properties(); 
			String command = "psql --host=ocadosp-spaceadaptor-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com ocadosp_salesservice_sit --user master";
			session = jsch.getSession("mwadmin","10.244.39.83",22);
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("2Bchanged");
			session.connect();
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			InputStream in = channel.getInputStream();
			((ChannelExec)channel).setPty(false);
			OutputStream out=channel.getOutputStream();
			channel.connect();
			Thread.sleep(1000);
        	out.write(("FS4T3fdQCYCym48h"+"\n").getBytes());
        	out.flush();
        	for(int item=0;item<orderid.length;item++)
        	{
        		//**********************************Execute Query to extract name************************	
        		Thread.sleep(1000);
        		QResult="";
        		out.flush();
        		out.write(("select distinct rtlog_file_name from storepick_sales_service.SALES_DETAILS where order_id='"+orderid[item]+"';\n").getBytes());
        		out.flush();
        		Thread.sleep(1000);
        		Thread.sleep(5000);
        		byte[] tmp = new byte[2048];
        		int i=0;  
        		while (in.available() > 0) {
        			i = in.read(tmp, 0, 1024);
        			if (i < 0) {
        				break;
    				}
        		}
        		QResult=new String(tmp, 0, i); 
        		String[] rest= QResult.split(orderid[item]);
        		String justfinalres=rest[rest.length-1].replaceAll("[+|]", "");
        		String[] rest2=justfinalres.split("[\\r\\n]+");
        		String finalres="";
        		for(int restcount=0;restcount<rest2.length;restcount++)
        		{
        			if(rest2[restcount].contains("("))
        			{
        				if(!rest2[restcount-1].equals(columnname))
        				{
        					finalres=rest2[restcount-1].trim();
        					break;
        				}
        			}
	   	 
        		}
        		finalres = rest2[7].trim();
        		if(finalres.equals("(0 rows)")||finalres.equals(""))
	   		 	{
        			SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of RTLOG for orderid "+orderid[item], "Did not found because NO value was found in RDS DB ",DriverSheetPath);
	   		 	}else{
	   		 		RTLOGS.add(finalres) ;
	   		 		SandPiperUtilities.utilityFileWriteOP.writeToLog(tcid, "The value of RTLOG for messageid "+orderid[item], "is "+finalres,DriverSheetPath);
	   		 	}
	   		 	res=false;
        	}
	   	 	out.flush();
	   	 	out.write(("\\q"+"\n").getBytes());
	   	 	out.flush();
	   	 	channel.disconnect();
	   	 	session.disconnect();
	   	 	out.close();
	   	 	in.close();
   	 	}catch(Exception e){
			e.printStackTrace();
			System.out.println(e);
   	 	}finally{        
   	 		if (channel != null) {
   	 			session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
   	 		}
		return RTLOGS;
   	 	}
	}
	
	//******************************************************
	//Method Name: ValidateItemDetailsInRTLOG
	//Method Description: Validate Item DetailsInRTLOG
	//Created By: Morrissons TCS Automation Team
	//*******************************************************

public static boolean ValidateItemDetailsInRTLOG (String[] RTLOGnames , String downloadfilepath ,String rtlogtrandate, String[] items, String[] quantity, String totalamount,String orderid,String TC_no , String resultpath,XWPFRun xwpfRun){
		
	boolean res = true;	
	BufferedReader br = null;
	String[] starr1 = null;
	try{
		int itemcount = 0 ;
		int flag=0;
		System.out.println("10. RTLOG validation");
		RDS_validiation.startValidation();
		for (int rtlog=0; rtlog<RTLOGnames.length;rtlog++){
			br = new BufferedReader(new java.io.FileReader(downloadfilepath+"/"+RTLOGnames[rtlog]+".DAT"));
			String currentline = null;
			String currentline1 = null;
			while((currentline=br.readLine())!=null){
				String starr[] = currentline.split("\\s+");
				//System.out.println("first string: "+starr[0]);
				
				
				
				
				
				if(starr[0].contains("FHEAD")){
					String businessdate = starr[0].substring(19, 27);
					//System.out.println("business date : "+businessdate);
					String rtlogdate = starr[0].substring(33, 41);
					//System.out.println("rtlogdate: "+rtlogdate);
					//System.out.println("rtlogtrandate: "+rtlogtrandate.trim());
					if((businessdate.equals(rtlogdate)) && (rtlogdate.equals(rtlogtrandate.trim()))){
						
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Business date matched with Order creation date: ","PASS" ,resultpath,xwpfRun,"");	
						System.out.println("Business date "+businessdate+" matched with Order creation date: "+rtlogdate);
					}
					else{
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Business date haven't matched with Order creation date: ","FAIL" ,resultpath,xwpfRun,"");
						res=false;
						
					}
				}
				
				
				
				
				
				else if(starr[0].contains("THEAD")&&(starr[6].equals(orderid))){
					//System.out.println("rtlogtrandate array : "+starr[1]);
					if(starr[1].contains(rtlogtrandate)){
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Tran date matched with Order creation date: ","PASS" ,resultpath,xwpfRun,"");
						System.out.println("Tran date in RTLOG matched with Order creation date");
					}
					else
					{
						SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Tran date haven't matched with Order creation date: ","FAIL" ,resultpath,xwpfRun,"");
					res=false;
					}
				}
				
					
				
				
				
				
				
				 if(starr[0].contains("THEAD")){
					// System.out.println("inside item validation thead: "+starr[6]);
					 //System.out.println(orderid);
						if(starr[6].equals(orderid)) {
							
							//System.out.println("inside item validation");
							//validate if order exists
							//Utilities_I32a.utilityFileWriteOP.writeToLog(TC_no, "Orderid found in RTLOG: ",starr[6] ,resultpath);
							SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Orderid found in RTLOG: ","",resultpath,xwpfRun,starr[6]);
							System.out.println("Orderid found in RTLOG: "+starr[6]);
							while( ((currentline1=br.readLine())!=null)) {
							 
								starr1 = currentline1.split("\\s+");
								if((starr1[0].contains("TTEND")))
								{
								break;
								}
								else{
								//System.out.println("starr[0]"+starr1[0]);
								if(starr1[0].contains("TITEM")){
									//System.out.println("item: "+starr1[3]+"/n quantity: "+starr1[5]);
									//System.out.println("item: "+items[itemcount]+" quantity: "+quantity[itemcount]);
									if(starr1[3].equals(items[itemcount]) && starr1[5].contains(quantity[itemcount])){
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Item and quantity matched in RTLOG: ",starr1[3]+ " "+starr1[5],resultpath);
										System.out.println("Item and quantity matched in RTLOG with orderdetails");
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Item and quantity matched in RTLOG: ","",resultpath,xwpfRun,starr1[3]+ " "+starr1[5]);
									}
									else{
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Item and quantity not matched in RTLOG: ",starr1[3]+ " "+starr1[5] ,resultpath);
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Item and quantity not matched in RTLOG: ","",resultpath,xwpfRun,starr1[3]+ " "+starr1[5]);
										res=false;
									}
									itemcount++;
								}
								
								}
								flag = 1;
								
								
								
							}
							
							 if(starr1[0].contains("TTEND") && flag==1){
									//System.out.println("total amount: "+starr1[2]);
									if(starr1[2].contains(totalamount.replace(".", ""))){
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount matched in RTLOG: ",starr1[2] ,resultpath);
										System.out.println("total amount matched in RTLOG with order details");
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount matched in RTLOG: ","" ,resultpath,xwpfRun,starr1[2]);
									}
									else
										{
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount haven't matched in RTLOG: ",starr1[2] ,resultpath);
										System.out.println("Total amount displayed as "+starr1[2]+" instead of "+totalamount);
										SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount which should be "+totalamount+" not matched in RTLOG displayed as "+starr1[2],"" ,resultpath,xwpfRun,starr1[2]);
										res=false;
										}
								}				
								if(flag==1){
									break;
								} 

								 
						}
				 }
						
				 
				
				 
				 //System.out.println(flag);
				/* if(starr1[0].contains("TTEND") && flag==1){
						System.out.println("total amount: "+starr[2]);
						if(starr[2].contains(totalamount)){
							Utilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount matched in RTLOG: ",starr[2] ,resultpath);
						}
						else
							Utilities.utilityFileWriteOP.writeToLog(TC_no, "Total amount haven't matched in RTLOG: ",starr[2] ,resultpath);
					}				
					if(flag==1){
						break;
					}*/
				 
				
			}
			
			
			}
			
			br.close();
	        System.out.println("RTLOG details validated successfully");
	        RDS_validiation.EndValidation();
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			res=false;
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "A problem occurred while validating RTLOG file", "Due to "+e,resultpath);
			SandPiperUtilities.utilityFileWriteOP.writeToLog(TC_no, "A problem occurred while validating RTLOG file", "",resultpath,xwpfRun,"");
		}
		
		finally{
			return res;
		}
		
	}
	
	
	public static void startValidation(){
		System.out.println("\n\n");
		 System.out.println("--------------------------------------------------------------");
		 System.out.println("                      Validation                              ");
		 System.out.println("--------------------------------------------------------------");
	}
	
	public static void EndValidation(){
		
		 System.out.println("--------------------------------------------------------------");
		 System.out.println("                          End                                 ");
		 System.out.println("--------------------------------------------------------------");
		 System.out.println("\n\n");
	}
	
	
	        
}
